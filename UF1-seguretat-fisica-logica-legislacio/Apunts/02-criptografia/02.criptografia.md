# CRIPTOGRAFIA

<p align="center">
<img src="imgs/criptografia-5dd5e730.png" width="400" align="center" />
</p>

**INS Carles Vallbona**

**Pau Tomé**

(Act oct-2023)

---

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [CRIPTOGRAFIA](#criptografia)
	- [CONCEPTES DE CRIPTOGRAFIA](#conceptes-de-criptografia)
	- [XIFRAT SIMETRIC (CLAU SECRETA O COMPARTIDA)](#xifrat-simetric-clau-secreta-o-compartida)
		- [ALGORISMES CLASSICS DE XIFRAT SIMETRIC](#algorismes-classics-de-xifrat-simetric)
			- [Cèsar i ROT-13](#csar-i-rot-13)
		- [AVANTATGES I INCONVENIENTS DEL XIFRAT SIMETRIC (SECRET O SHARED KEY)](#avantatges-i-inconvenients-del-xifrat-simetric-secret-o-shared-key)
			- [Avantatges](#avantatges)
			- [Inconvenients](#inconvenients)
		- [ALGORISMES ACTUALS DE XIFRAT SIMETRIC](#algorismes-actuals-de-xifrat-simetric)
	- [XIFRAT ASIMETRIC O DE CLAU PUBLICA](#xifrat-asimetric-o-de-clau-publica)
		- [CONFIDENCIALITAT AMB XIFRAT ASIMETRIC](#confidencialitat-amb-xifrat-asimetric)
		- [AUTENTICACIO AMB XIFRAT ASIMETRIC](#autenticacio-amb-xifrat-asimetric)
		- [AVANTATGES I INCONVENIENTS DEL XIFRAT ASIMETRIC](#avantatges-i-inconvenients-del-xifrat-asimetric)
			- [Avantatges](#avantatges)
			- [Inconvenients](#inconvenients)
		- [ALGORISMES ACTUALS DE XIFRAT ASIMETRIC](#algorismes-actuals-de-xifrat-asimetric)
		- [PROTOCOLS QUE FAN SERVIR XIFRAT ASIMETRIC](#protocols-que-fan-servir-xifrat-asimetric)
	- [XIFRAT ASIMETRIC vs XIFRAT SIMETRIC](#xifrat-asimetric-vs-xifrat-simetric)
	- [LES FUNCIONS DE HASH](#les-funcions-de-hash)
		- [CARACTERISTIQUES DELS HASH](#caracteristiques-dels-hash)
		- [EXEMPLES D’US PRACTICS I REALS DE LES FUNCIONS DE HASH](#exemples-dus-practics-i-reals-de-les-funcions-de-hash)
		- [FUNCIONS DE HASH ACTUALS](#funcions-de-hash-actuals)
		- [EXEMPLE D'US DEL HASH PER A FIRMA DIGITAL](#exemple-dus-del-hash-per-a-firma-digital)
		- [FUNCIONS DE HASH: EXEMPLE AL FIREFOX](#funcions-de-hash-exemple-al-firefox)
	- [XIFRAT HIBRID](#xifrat-hibrid)
		- [XIFRAT HIBRID SENSE FIRMA](#xifrat-hibrid-sense-firma)
		- [XIFRAT HIBRID AMB FIRMA](#xifrat-hibrid-amb-firma)
		- [XIFRAT HIBRID EN NAVEGADORS WEB](#xifrat-hibrid-en-navegadors-web)
	- [Recursos](#recursos)

<!-- /TOC -->
---

## CONCEPTES DE CRIPTOGRAFIA

Històricament, l'home ha intentat que els missatges que envia només siguin visibles pel seu destinatari. Per a aconseguir aquesta confidencialitat, la solució ha estat xifrar els missatges.

Alguns conceptes importants sobre la criptografia són:

- **Xifrar**: és agafar el missatge i fer-li alguna transformació que el faci inintel·ligible (amb algorismes matemàtics no reversibles).

- **Transformació o algorisme de xifrat**: procediment matemàtic que sol ser públic, de manera que tothom sap com es fa i, per tant, tothom sap com es fa per xifrar i desxifrar (algorisme de xifrat). Llavors, el secret del sistema i la seva força està en la clau. Ex: Tothom sap com s'obre i tanca una porta i sap com funciona el pany, però sense la clau no el podrà obrir.

- **Clau**: és un valor numèric que fa que els resultats del xifrat amb el mateix mètode siguin diferents per a claus diferents. Per ex: Per a cada pany hi ha una clau diferent a la de qualsevol altre.

Tenim dos models de xifrat:

- **Xifrat simètric** també anomenat "preshared key cypher" o "secret key cypher" (xifrat de clau compartida o clau secreta). És el mètode utilitzat des dels començaments de la criptografia.
- **Xifrat asimètric** també anomenat "public key cypher" (xifrat de clau pública). El mètode que va aparèixer després de la II guerra mundial, per a les comunicacions digitals.

---

## XIFRAT SIMETRIC (CLAU SECRETA O COMPARTIDA)


- **Xifrat simètric:** Consisteix en convertir el text usant una sola clau que permet xifrar i desxifrar el missatge.

<p align="center">
<img src="imgs/20180912-131431.png" width="600" align="center" />
</p>

- **Símil**: Posar el missatge en una caixa i tancar amb un cadenat que té una clau per obrir i tancar.

<p align="center">
<img src="imgs/04.criptografia-curt-af13fde2.png" width="300" align="center" />
</p>

---
- Explicació - https://www.practicalnetworking.net/series/cryptography/symmetric-encryption/

---

### ALGORISMES CLASSICS DE XIFRAT SIMETRIC

Els **sistemes clàssics** de xifrat no són prou robustos pels
ordinadors actuals ...

- Scytala
- Cèsar / ROT-13
- Substitució
- XOR
- Vigenère

#### Cèsar i ROT-13

El xifrat Cèsar consisteix en canviar cada lletra de l'alfabet per la lletra que ocupa n posicions més a la dreta, tornant a començar des del principi si arribem a la darrera lletra.
- Aquí la clau eś n
- La transformació és la substitució de cada caràcter.

A la figura es pot veure el cas en que n=13 (ROT13 o rotar 13 posicions). Aquest cas en l'alfabet anglès és un cas especial ja que hi ha 26 lletres.

<img src="imgs/20180912-132449.png" width="600" align="center" />

Traductor implementat en bash: echo “hola que tal” | tr A-Za-z N-ZA-Mn-za-m

~~~
// Xifra
$ echo “hola que tal” | tr A-Za-z N-ZA-Mn-za-m
“ubyn dhr gny”
// Desxifra
$ echo “ubyn dhr gny” | tr  N-ZA-Mn-za-m A-Za-z
“hola que tal”
~~~

- ROT 13 web (totes les claus) - https://rot13.com/
- Més info: LECCIÓN 8: ALGORITMOS DE CIFRA POR SUSTITUCIÓN MONOALFABÉTICA - https://www.criptored.es/crypt4you/temas/criptografiaclasica/leccion8.html (curs Introducción a la seguridad informática y criptografía clásica - https://www.criptored.es/crypt4you/temas/criptografiaclasica/leccion1.html)

---

### AVANTATGES I INCONVENIENTS DEL XIFRAT SIMETRIC (SECRET O SHARED KEY)

#### Avantatges

- Permet assegurar la **Confidencialitat** (secret).
- Els algorismes són **relativament ràpids per a xifrar i desxifrar** (comparats amb els asimètrics).
- S'utilitzen en **comunicació interactiva**, com per exemple ssh (consola remota segura) o https (web segura).

#### Inconvenients

- **El més important**: El problema de l’**intercanvi de claus** (shared key). S'ha de tenir la mateixa clau per xifrar i desxifrar i això és un problema per exemple a Internet, on la distància física és molt gran i cal enviar la contrasenya de manera que algú la podria interceptar.

- **No asseguren Autenticació**: En el cas de Cèsar, havia de fer servir un segell propi, per exemple al seu anell, per poder demostrar que el missatge era d'ell.

- **No asseguren Integritat** En el cas de Cèsar, feia servir el segell de cera amb la seva imatge per tancar el sobre. Si algú trencava el segell, el missatge s'havia espiat.

- **No asseguren No Rebuig**.

- Es necessita **una clau per a la comunicació entre cada parell d'entitats**.

<p align="center">
<img src="imgs/04.criptografia-curt-21dc5357.png" width="400" align="center" />
</p>

https://linuxdevices.org/primer-on-elliptical-curve-cryptography/

---

### ALGORISMES ACTUALS DE XIFRAT SIMETRIC

- **DES**: Data Encription Stàndar. Insegur.
- **3DES**: Fa servir 3 passades de DES.
- **RC5**: Rivest Cipher 5.
- **AES**: Andvanced Encription Stàndar. Basat en RC6.
- **Blowfish**: Algorisme sense patent, per substituir DES.
- **IDEA**: International Data Encryption Algorithm (primera versió insegura).

---

## XIFRAT ASIMETRIC O DE CLAU PUBLICA

Per a **superar els problemes de la criptografia de clau simètrica**, van aparèixer mètodes nous de xifrat que no es basen en una sola clau compartida.

**Xifrat asimètric (o de clau pública):** Aquest mètode criptogràfic utilitza dues claus:
- **Clau pública:** Clau que **s’ha de difondre i publicar**. Normalment, és el propi individu qui la dona al seu interlocutor quan es vol comunicar amb ell.
- **Clau privada:** Clau que **s’ha de mantenir en secret**. Normalment, **es protegeix en un fitxer xifrat amb una altra clau secreta** que s'ha de saber abans de fer servir la clau pública.

---

<p align="center">
<img src="imgs/04.criptografia-curt-07519c2f.png" width="500" align="center" />
</p>

---

Les dues claus són **complementàries i reversibles**, o sigui, el que es xifra amb una, es desxifra amb l’altra.

- **Cada entitat** (persona, servidor, programari, etc) **ha de tenir la seva parella de claus pròpia**, que es generen juntes al mateix procés.

- **És impossible xifrar i desxifrar amb la mateixa clau**.

- **Símil**: Si es vol enviar un missatge a Bob, cal posar el missatge en una caixa i tancar-ho amb un cadenat (obert) que t’ha donat Bob. Es podrà tancar però només Bob tindrà la clau per obrir-ho.

<p align="center">
<img src="imgs/3.2.criptografia-42875907.png" width="100" align="center" />
</p>

---

Explicació xifrat asimètric - https://www.practicalnetworking.net/series/cryptography/using-asymmetric-keys/

Exemple RSA - https://www.practicalnetworking.net/series/cryptography/rsa-example/

Exemple Diffie Hellman https://www.practicalnetworking.net/series/cryptography/diffie-hellman/

---

### CONFIDENCIALITAT AMB XIFRAT ASIMETRIC

Si volem **confidencialitat (secret)**, l’emissor ha de xifrar el missatge. Per això, obté la clau pública del receptor i xifra el missatge. El receptor el desxifra amb la seva clau privada.

**Missatge Confidencial:**
- Anna xifra el missatge en clar (1) usant la clau pública de David i crea el missatge xifrat (2).
- S'envia el missatge (3).
- David usa la seva pròpia clau privada per desxifrar el missatge xifrat (4) i obtenir el missatge en clar (5).

<p align="center">
<img src="imgs/20180912-132740.png" width="600" align="center" />
</p>

https://es.wikipedia.org/wiki/Criptograf%C3%ADa_asim%C3%A9trica

---

### AUTENTICACIO AMB XIFRAT ASIMETRIC

Per a poder autenticar un missatge, cal que l’emissor faci **alguna cosa que només ell pot fer** per assegurar que és ell. Per tant, l'emissor xifrarà les dades amb la seva clau privada que només ell te.

**FIRMA DIGITAL:** És l'equivalent en el mon digital de la firma clàssica per a poder autenticar el missatge enviat. La firma clàssica és molt fàcil de duplicar digitalment per exemple escannejant-la.

**ATENCIO:** En aquest cas NO hi ha confidencialitat (cal fer un procès més). A més, l'autenticació no és completa si la clau pública no és fiable. Cal ssegurar-se que la clau pública que usem és la de l'entitat que volem autenticar.

**Missatge Firmat:**
- David usa la seva pròpia clau **privada** per a xifrar el missatge en clar (1) i d'aquesta manera genera un missatge "firmat" (2).
- S'envia el missatge (3).
- Anna obté i usa la clau **pública** de David per validar la firma (4) i obtenir el missatge en clar (5).

<p align="center">
<img src="imgs/20180912-133401.png" width="600" align="center" />
</p>

https://es.wikipedia.org/wiki/Criptograf%C3%ADa_asim%C3%A9trica

---

En realitat, com que xifrar amb criptografia de clau pública grans volums d'informació és molt costós, s'extreu un hash del document en clar i és aquest hash el que en realitat s'encripta amb la clau privada de l'emissor per generar la firma.

<p align="center">
<img src="imgs/firma-digital-55b4e6a7.png" width="600" align="center" />
</p>

Frame del vídeo: https://www.youtube.com/watch?v=vaGTDS8UTs0&list=PLr5GsywSn9d_hd7MTimziiM8yZH1d-Igz&index=13

---

Quan el missatge arriba al receptor, aquest comprova la firma generant el hash del document en clar, desxifrant la firma amb la clau pública de l'emissor i finalment comparant els dos valors. En cas que siguin diferents, el missatge es descartaria.

<p align="center">
<img src="imgs/firma-digital-aaecd321.png" width="600" align="center" />
</p>

Frame del vídeo: https://www.youtube.com/watch?v=vaGTDS8UTs0&list=PLr5GsywSn9d_hd7MTimziiM8yZH1d-Igz&index=13

---

- Vídeo explicació: Firma digital de documents (INCIBE) https://www.youtube.com/watch?v=vaGTDS8UTs0&list=PLr5GsywSn9d_hd7MTimziiM8yZH1d-Igz&index=13

---

Si es vol que el **missatge estigui firmat i sigui secret**, cal xifrar el missatge firmat amb la clau pública d'Ana abans d'enviar-li.

---

### AVANTATGES I INCONVENIENTS DEL XIFRAT ASIMETRIC

#### Avantatges
- Permet assegurar Confidencialitat, Autenticació i No Rebuig (S’han de combinar xifrats).
- S'utilitzen per:
	- **Autenticació mútua** (o unilateral. Per ex. les pàgines web tenen certificat però els usuaris moltes vegades no.)
	- **No rebuig**
	- Per intercanviar claus d’algorismes Simètrics de forma secreta (Xifrat Híbrid).
- Només es requereix **un parell de claus per a cada entitat** per establir totes les comunicacions.

<p align="center">
<img src="imgs/04.criptografia-curt-b9d14721.png" width="400" align="center" />
</p>

https://linuxdevices.org/primer-on-elliptical-curve-cryptography/

---

#### Inconvenients

- **El més important**: Davant la mateixa quantitat de dades, són **molt més lents que els simètrics**.
- Les **claus són molt més llargues**.
- El **missatge xifrat ocupa més espai que el missatge en clar**. Els nous sistemes basats en corbes el·líptiques són menys costosos, però.

---

### ALGORISMES ACTUALS DE XIFRAT ASIMETRIC

- **Diffie-Hellman**: Basat en càlcul de logaritmes discrets.
- **RSA**: Rivest, Shamir y Adleman. Per xifrar i firmar. Basat en factorització de nos. sencers
- **DSA**: Digital Signature Algorithm. Només per a firmar. Més lent que RSA.
- **ElGamal**: Basat en càlcul de logaritmes discrets.
- **Criptografia de corba el·líptica**: Fan servir equacions cúbiques (de tercer grau).

### PROTOCOLS QUE FAN SERVIR XIFRAT ASIMETRIC

- **DSS** ("Digital Signature Standard") amb l’algorisme DSA ("Digital Signature Algorithm")
- **PGP** (Prety Good Privacy) Híbrid.
- **GPG** (GNU Privacy Guard), una implementació d’OpenPGP
- **SSH** (Secure Shell)
- **SSL** (Secure Socket Layer)
- **TLS** (Transport Layer Security) Succesor de SSL

---

## XIFRAT ASIMETRIC vs XIFRAT SIMETRIC

- Vídeo: Encryption - Symmetric Encryption vs Asymmetric Encryption - Cryptography - Practical TLS - https://www.youtube.com/watch?v=o_g-M7UBqI8

---

## LES FUNCIONS DE HASH

**FUNCIO DE HASH:** És un algorisme que serveix per a obtenir a partir d'un bloc de dades de qualsevol mida, un valor de mida fixa. Aquest valor s'anomena "resum" (digest) i actua com a empremta digital ("fingerprint") del fitxer original.

<p align="center">
<img src="imgs/04.criptografia-curt2223-d231b015.png" width="400" align="center" />
</p>

---

**NO és un algorisme de xifrat** ja que la informació original NO es pot recuperar.

---

### CARACTERISTIQUES DELS HASH

- Els valors retornats per una funció de hash es diuen **resum o hash**.
- Tots els HASHes generats amb una funció de hash tenen la **mateixa mida**, independentment de la mida del missatge.
- Donat un missatge, és **fàcil i ràpid** calcular el HASH.
- És **impossible obtenir el missatge original** a partir del HASH (irreversible).
- És **impossible generar un missatge concret** amb un HASH determinat.
- S’utilitza per assegurar la **integritat de dades** transmeses i per a fer **autenticació de missatges** (entre d'altres).

<p align="center">
<img src="imgs/04.criptografia-curt2223-f945f137.png" width="300" align="center" />
</p>

**Col·lissió:** Pot passar que dos blocs diferents de dades obtinguin el mateix hash (en la figura John Smith i Sandra Dee).
- En aquest cas **tenim una col·lisió**. Però és impossible trobar aquests dos blocs que donen el mateix hash. En això es basa aquesta funció.
- En el cas que es pugui fer, l'algorisme es considera trencat ja que es pot canviar el bloc binari i generar el mateix hash per enganyar-nos.

- Referències:
	- Hashing algorithm - https://www.practicalnetworking.net/series/cryptography/hashing-algorithm/
	- Hashing, Hashing Algorithms, and Collisions - Practical TLS - https://www.youtube.com/watch?v=HHQ2QP_upGM

- Video: Data Integrity - How Hashing is used to ensure data isn't modified - Cryptography - Practical TLS - https://www.youtube.com/watch?v=doN3lzzNEIM&list=PLIFyRwBY_4bTwRX__Zn4-letrtpSj1mzY&index=8

- Vídeo: Hashing, Hashing Algorithms, and Collisions - Cryptography - Practical TLS - https://www.youtube.com/watch?v=HHQ2QP_upGM&list=PLIFyRwBY_4bTwRX__Zn4-letrtpSj1mzY&index=7

- Funció de hash - https://hmong.es/wiki/Hash_sum

---

### EXEMPLES D’US PRACTICS I REALS DE LES FUNCIONS DE HASH

- Validar que un missatge o fitxer **no ha estat modificat** per un atacant o malware (comparem amb el hash calculat abans de l’atac).

- **Comprovar integritat** d'una imatge ISO o qualsevol fitxer baixat d'Internet (comparem amb el valor hash que hi ha a la web amb el generat a partir del fitxer).

- **Guardar contrasenyes** a un fitxer. Es guarda el hash, no la contrasenya i en autenticació es compara el valor calculat de hash amb el que hi ha emmagatzemat.

- **Firma digital**: Si volem **integritat, autenticació i no rebuig**, l’emissor firmarà només un hash del missatge i l’afegirà al missatge xifrat. Això permet també comprovar integritat del missatge.

- **Descartar fitxers "bons"** en una imatge forense per no haver de tractar-los.

- **Detectar fitxers "dolents"** coneguts precisament per analitzar-los per que poden ser o contenir malware.

---

### FUNCIONS DE HASH ACTUALS

**Exemples** de funcions de HASH:

- **MD5** (Message-Digest Algorithm): Funció hash amb valor de sortida de 128 bits. No serveix per xifrar un missatge, i no es pot recuperar el missatge a partir del HASH. Aquesta funció hash no és segura. A linux: md5sum

- **SHA-1** (Secure Hash Algorithm): Semblant a MD5, però amb un bloc de 160 bits. La funció es més complexa que MD5, i per tant més lent. Més robust i segur que MD5, però ja s’han trobat col·lisions i no és segur utilitzar-la. A linux: sha1sum.

- **SHA-2:** Les principals diferències amb SHA-1 són que te un disseny diferent i que els rangs de sortida han sigut incrementats. Dintre de SHA-2 trobem:
  - **SHA-224, SHA-256, SHA-384, SHA-512** amb blocs de 224, 256, 384 i 512 bits respectivament (SHA-512 és el més segur i lent de calcular).

Es pot calcular amb les comandes **sha224sum**, i per a totes les funcions SHA en general, existeix la comanda “**shaXXXsum**”, amb XXX=núm bits.

~~~
$ sha256sum VASSAL-3.5.8-linux.tar.bz2
e54b76c55a69170764ae9d88527a981c22d6aad164a71d83cfd2f64e4cbe9b0f  VASSAL-3.5.8-linux.tar.bz2
~~~

---

### EXEMPLE D'US DEL HASH PER A FIRMA DIGITAL

Bob pot comprovar que és Anna qui li envia el missatge (autenticació) i que aquest no s'ha modificat (integritat)

1. Anna calcula el hash del missatge en clar.
2. Anna xifra el hash amb la seva pròpia clau privada. Així obté la **firma digital**.
3. Anna afegeix la firma digital al missatge (si vol que sigui secret, després el xifrarà amb la clau pública de Bob). Anna envia el bloc resultant al Bob.
4. Bob rep el bloc i separa la firma del missatge (si el bloc està xifrat el desxifra amb la seva pròpia clau privada).
5. Bob desxifra la firma digital usant la clau pública de Anna.
6. Ara Bob calcula el hash del missatge rebut i el compara amb la firma desxifrada.
7. Si són iguals, Bob haurà comprovat la integritat i haurà comprovat que el missatge l'ha enviat Anna (autenticat).     

<img src="imgs/20180912-133818.png" width="700" align="center" />

---

### FUNCIONS DE HASH: EXEMPLE AL FIREFOX

Firefox usa el hash per identificar cada certificat. Manualment es pot comprovar si aquest hash és el que ens havia donat una font de confiança.

<img src="imgs/20180912-133939.png" width="600" align="center" />

---

## XIFRAT HIBRID

Com que el xifrat simètric té el problema principal de l'intercanvi de claus i el xifrat asimètric el problema de ser lent, es faran servir els dos tipus de criptografia per obtenir el millor dels dos mètodes:

- **Asimètrica**: S'utilitza per a autenticació mútua usant els certificats. Un cop autenticats, per enviar la clau secreta o simètrica.
- **Simètrica**: Per xifrar les dades durant la sessió. Aquesta clau es crea de forma aleatòria i es fa servir per una sessió només. Se li diu **CLAU DE SESSIO**. Si algú la intercepta, no li servirà per a una altra sessió.

### XIFRAT HIBRID SENSE FIRMA

Xifrat híbrid sense firma en el cas que vulguem enviar un missatge per correu electrònic.

![](imgs/02.criptografia-77503284.png)

https://es.slideshare.net/JuanPabloSarubbi/cifrado-17171950

---

### XIFRAT HIBRID AMB FIRMA

Xifrat híbrid Firmat, que s'utilitza en el cas que vulguem enviar un missatge secret i firmat.

![](imgs/20180912-134340.png)

http://mikelgarcialarragan.blogspot.com/2016/07/criptografia-xxi-criptologia-para-todos.html

---

### XIFRAT HIBRID EN NAVEGADORS WEB

Xifrat híbrid per a fer una comunicació interactiva amb un servidor web (HTTPS).

1. Inici de connexió segura des del client web
2. El servidor envia el seu certificat. El client web comprova que sigui vàlid i que està signat per una autoritat de confiança. Es negocia el protocol de xifrat simètric a usar.
3. El client envia al servidor una clau de xifrat simètric generada per aquella sessió. Va encriptada amb la clau pública del servidor.
4. El servidor desxifra la clau de sessió amb la seva clau privada.
5. A partir d'ara, el tràfic es xifra amb un protocol de xifrat simètric (negociat a l'inici) amb la clau de sessió que s'ha compartit.

![](imgs/3.2.criptografia-179ad935.png)

https://panel.rootway.com/knowledgebase.php?action=displayarticle&id=2&language=french

---

## Recursos

- Breve historia de la criptografia - https://www.eldiario.es/turing/criptografia/breve-historia-criptografia_1_4878763.html

- Conceptes criptografia - https://www.practicalnetworking.net/series/cryptography/cryptography/)

- Generador claus GPG - https://wp2pgpmail.com/pgp-key-generator/

- Calculadores de xifrat i més - http://es.planetcalc.com/search/?tag=2027

- Exemple RSA - https://www.practicalnetworking.net/series/cryptography/rsa-example/)

- Exemple Diffie Hellman - https://www.practicalnetworking.net/series/cryptography/diffie-hellman/)

- Explicació Hashing - https://www.practicalnetworking.net/series/cryptography/hashing-algorithm/)

---

- Thoth, el dios de la sabiduría y de la escritura en la mitología egipcia, es un proyecto educativo de Criptored, que cuenta con el patrocinio de Talentum Startups y que publica píldoras formativas en seguridad de la información con una duración media en torno a los 5 minutos. Dirigido por el Dr. Jorge Ramió y el Dr. Alfonso Muñoz, editores también de Criptored, este nuevo formato de enseñanza tiene como objetivo ser la vía más fácil, rápida, amena y eficaz para aprender y reforzar conceptos relacionados con la seguridad de la información y especialmente la criptografía.
  - http://www.criptored.upm.es/thoth/index.php
	- https://www.youtube.com/playlist?list=PL8bSwVy8_IcNNS5QDLjV7gUg8dIeMFSER

---
