# SEGURETAT LOGICA

**INS Carles Vallbona**

**Pau Tomé**

![](imgs/3.seg-logica-23bab58d.png)

(Act oct-2023)

---

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [SEGURETAT LOGICA](#seguretat-logica)
	- [1. CONTROL D'ACCES](#1-control-dacces)
		- [CONTROL D'ACCES: AUTENTICACIO](#control-dacces-autenticacio)
			- [SISTEMES BIOMETRICS](#sistemes-biometrics)
				- [TIPUS DE SISTEMES BIOMÈTRICS:](#tipus-de-sistemes-biomtrics)
		- [CONTROL D'ACCES: AUTORITZACIO AMB ACL’S (ACCESS CONTROL LIST)](#control-dacces-autoritzacio-amb-acls-access-control-list)
			- [Conceptes importants de les ACL](#conceptes-importants-de-les-acl)
		- [APLICACIO DE LES ACL'S](#aplicacio-de-les-acls)
			- [EXEMPLE D'APLICACIO PRACTICA DE LES ACL'S AL SIST. ARXIUS](#exemple-daplicacio-practica-de-les-acls-al-sist-arxius)
				- [Sistema d'arxius](#sistema-darxius)
	- [2. POLITIQUES DE CONTRASENYES](#2-politiques-de-contrasenyes)
		- [CREACIO DE CONTRASENYES SEGURES](#creacio-de-contrasenyes-segures)
		- [MESURES DE PROTECCIO DE CONTRASENYES (USUARIS)](#mesures-de-proteccio-de-contrasenyes-usuaris)
		- [MESURES DE PROTECCIO DE CONTRASENYES (ADMINISTRADORS)](#mesures-de-proteccio-de-contrasenyes-administradors)
		- [ATACS A LES CONTRASENYES](#atacs-a-les-contrasenyes)
			- [EINES PER INTERCEPTAR CONTRASENYES EN XARXA](#eines-per-interceptar-contrasenyes-en-xarxa)
			- [EINES PER ATACAR CONTRASENYES OFF-LINE](#eines-per-atacar-contrasenyes-off-line)
			- [EINES PER ATACAR CONTRASENYES ON-LINE](#eines-per-atacar-contrasenyes-on-line)
	- [3. REGISTRES D'USUARIS, INCIDENCIES I ALARMES (LOGS)](#3-registres-dusuaris-incidencies-i-alarmes-logs)
		- [REGISTRES DEL SISTEMA OPERATIU](#registres-del-sistema-operatiu)
		- [REGISTRES DE PROGRAMARI DE SEG.](#registres-de-programari-de-seg)
		- [GESTIO DE REGISTRES](#gestio-de-registres)
		- [PROTECCIO DELS REGISTRES](#proteccio-dels-registres)
	- [MES INFORMACIO](#mes-informacio)
	- [ACL’s al sistema d’arxius NTFS de Windows:](#acls-al-sistema-darxius-ntfs-de-windows)

<!-- /TOC -->

---

Les mesures de seguretat lògiques pretenen protegir contra atacs al programari, les dades i accés d'usuaris del sistema. Veurem com es gestiona el control d'accés, les polítiques de contrasenyes i la gestió dels registres o logs del sistema per a detectar atacs o mal funcionament del sistema.

---

## 1. CONTROL D'ACCES

El control d'accés serveix per especificar qui (persona) o què (un programa) pot accedir a cadascun dels recursos del sistema, i de quina manera (tipus d'accés). Engloba 2 aspectes:
- **Autenticació**: Cal verificar la identitat de l'entitat per permetre-li entrar al sistema o no.
- **Autorització**: Un cop autenticada l'entitat, cal concedir permís o dret a aquesta entitat per accedir a un recurs d'una manera determinada.


### CONTROL D'ACCES: AUTENTICACIO

L'autenticació permet assegurar que un subjecte (usuari o procés) és l'entitat que diu que és.

Per poder autenticar normalment existeix un **identificador** d'usuari i algunes dades addicionals per assegurar que el subjecte és qui diu ser. Exemple: nom usuari+contrasenya.

**Identificador**: És un element que distingeix un individu d'un altre. Ex: El nom d'usuari o DNI.

Un identificador ha de tenir tres característiques clau:
- **Unicitat**: Cada individu ha de tenir un identificador únic. Ex: nom d'usuari, DNI, empremta digital, etc.
- **No descriptiu**: No ha d'indicar la finalitat del compte. Ex: no es pot posar "gerent" en un compte d'una empresa.
- **Expedida per una autoritat reconeguda**. Ex: el DNI el fa la policia.

Opcionalment, per facilitar la gestió d'identificadors:
- **Uniformitat en el format**: És recomanable que tinguin el mateix format. Exemples:
	- nom.cognom sense caràcters extesos (ñ, é, ó, etc.),
	- inicial nom+cognom+inicial segon cognom, etc.


Cal recordar que tenim **3 mètodes per autenticar** (**Saps-tens-ets**):
- Una cosa que **SAPS**: Per exemple una contrasenya o un PIN.
- Una cosa que **TENS**: Per exemple una targeta magnètica, una pulsera o un mòbil amb NFC.
- Una cosa que **ETS**: Per exemple l'empremta dactilar, la forma de caminar o com fas una firma.

---

#### SISTEMES BIOMETRICS

Són sistemes d'autenticació per a requeriments de seguretat molt elevats.
- Són molt més cars.
- Verifiquen la identitat d'un usuari analitzant algun dels seus atributs físics:
  - **Coses que l'usuari és**:  Per exemple lectors d'empremtes dactilars.
  - **Coses que l'usuari sap fer**: Per exemple usar una tauleta digitalitzadora on es firma.

**Funcionament**: Es fa un escaneig d'un patró físic o comportament de l'individu (en el moment d'autenticar) i es compara amb una mostra model que es te enregistrada i que va crear l'individu amb anterioritat.

**Problemes**:
- Problemes del **patró de comportament**:  El patró pot canviar amb el temps o ser falsificat.
- Problemes de **fals positiu**: Això passa quan s'accepta un impostor.
- Problemes de **fals negatiu**: Passa quan es denega l'accés a un individu legítim.

- **Problemes ètics**, amb governs autoritaris. Si les dades biomètriques cauen en males mans, es poden usar per a perseguir les persones i aquestes poc poden fer per no se identificades ja que no poden canviar l'emprempta dactilar o el seu iris o la seva retina
  - Afganistan: https://theconversation.com/afganistan-un-nuevo-ejemplo-de-los-riesgos-que-supone-la-identificacion-biometrica-167562
	- Guia de Human Rights First per evadir control biomètric - https://humanrightsfirst.org/library/steps-to-protect-your-online-identity-from-the-taliban-digital-history-and-evading-biometrics-abuses/

---

##### TIPUS DE SISTEMES BIOMÈTRICS:

Tots es basen en característiques úniques per cada individu.
- **Lectors d'empremtes dactilars**: Analitzen la disposició de les línies als dits.

	<img src="imgs/6.seg-logica-16af50a7.png" width="400" align="center" />

	---

- **Lectors de palmell de la ma**: Analitzen la disposició de les línies del palmell de la mà.

	<img src="imgs/6.seg-logica-623a39b1.png" width="400" align="center" />

	---

- **Lectors de retina**: Comproven la disposició dels vasos sanguinis a la retina, la part blanca de l'ull.

- **Lectors d'Iris**: Es comprova la disposició dels anells i colors de l'iris, la part colorida de l'ull. Al mig està la pupil·la, el cercle "negre" al mig de l'ull.

	<img src="imgs/6.seg-logica-2db42b11.png" width="400" align="center" />

---

- **Lectors facials**: Analitzen molts factors de la cara, com ara l'estructura òssia, la distància entre ulls, la forma de la barbeta... En principi es distingeixen encara que engreixem.

	<img src="imgs/6.seg-logica-26fe9aec.png" width="400" align="center" />

	---

- **Reconeixement de veu**: Analitzen la veu de l'individu per a identificar-lo.

	<img src="imgs/6.seg-logica-47352b7e.png" width="400" align="center" />

---

### CONTROL D'ACCES: AUTORITZACIO AMB ACL’S (ACCESS CONTROL LIST)

Un cop hem autenticat l'entitat que interacciona amb el sistema, podrem assignar-li el nivell d'accés als recursos. O sigui, què pot fer amb cada recurs del sistema. Això ho farem amb ACL=Access control List.

**ACL:** És una llista de drets que pot realitzar un **SUBJECTE** (entitat que fa una acció, normalment un usuari) associats a un **OBJECTE** (recurs que rep l’acció).

- L'ACL especifica quins subjectes (usuaris o processos del sistema) tenen accés permès als objectes.
- També especifica quines operacions estan permeses sobre un determinat objecte.

<img src="imgs/6.seg-logica-e4975474.png" width="700" align="center" />

---

#### Conceptes importants de les ACL

- **Objecte**: Recurs que té l'accés controlat (objecte per emmagatzemar o rebre informació, com ara un fitxer).
- **Subjecte**: Entitat capaç d'accedir als objectes (normalment un procés, que representa un usuari).

- **Dret d'accés**: Manera en que un subjecte accedeix a un objecte. Exemple (sistema d'arxius):
	- **Lectura**: visualitzar la informació continguda.
	- **Escriptura**: Modificar, afegir, treure contingut.
	- **Execució**: Executar programes.
	- **Esborrar**: Eliminar recurs (fitxer, registre...)
	- **Crear**: Fer objectes nous (fitxers, directoris, etc.)

---

### APLICACIO DE LES ACL'S

Es pot aplicar a diferents àmbits:

- Sistemes d’arxius.
- Altres objectes del sistema (impressores, registre, etc.)
- Xarxes.
- Bases de dades.

---

#### EXEMPLE D'APLICACIO PRACTICA DE LES ACL'S AL SIST. ARXIUS

##### Sistema d'arxius

En control d'accés bàsic al sistema d'arxius de Unix, tenim 2 classes d'**objectes**:
- Fitxers
- Directoris o carpetes.

En control d'accés bàsic al sistema d'arxius de Unix, tenim 3 classes de **subjectes**:
- **Propietari (owner**): Per defecte el creador del recurs. Pot canviar l'ACL.
- **Grup (group)**: Diversos usuaris organitzats en un compte especial anomenat grup. Es pot pertànyer a múltiples grups per tenir els drets de cada grup.
- **Altres (other) o Tots**: Resta d'usuaris que han entrat al sistema.

	<img src="imgs/6.seg-logica-8bcf7d3f.png" width="500" align="center" />

En control d'accés bàsic (Unix) tenim 3 classes de **drets**:
- **Read**:
  - Sobre fitxers permet llegir el seu contingut.
	- Sobre directoris permet veure el contingut (ls).
- **Write**:
	- Sobre fitxers permet modificar només el seu contingut. No el nom i atributs ni esborrar.
	- Sobre directoris permet crear i esborrar fitxers a dintre i modificar atributs.
- **eXecute**:
	- Sobre fitxers permet fer executable.
	- Sobre directoris permet passar per ells (cd, ls).

---

## 2. POLITIQUES DE CONTRASENYES

Una de les principals mesures de seguretat és aconseguir contrasenyes robustes, ja que constitueixen la majoria de sistemes d'autenticació. **El mal ús de les contrasenyes és una de les deu amenaces més habituals dels sistemes de seguretat.**

**Contrasenya**: Conjunt de caràcters secrets que s'utilitzen en un procés d'autenticació. Pot constar de caràcters alfanumèrics i símbols.

<img src="imgs/contrasenya-facil.jpeg" width="400" align="center" />

Podeu esbrinar la contrasenya?

---

**Política de contrasenyes**: És un document que regula quines són les normes de creació, protecció i freqüència de renovació de les contrasenyes. En una organització determina al menys:
- **Nombre mínim** de caràcters de la contrasenya.
- **Caducitat** de les contrasenyes. Caldrà canviar-la quan caduqui.
- **Condicions de bloqueig del Compte**: Si algú fa un nombre N determinat d'intents seguits fallits per posar la contrasenya, el compte d'usuari es bloca. Aquest nombre d'intents s'ha de fer en un interval de temps x. Per exemple: Fer 3 intents fallits en menys d'un minut.
- **Complexitat** de la contrasenya: Indica quins tipus de caràcters ha de contenir la contrasenya i la seqüència de caràcters. Exemple:
  - cal una barreja de lletres, números i símbols.
  - No utilitzar combinacions de tecles com ara QWERTY, caràcters que estan consecutius al teclat.
  - No usar paraules que puguin estar a un diccionari, etc.
- **Historial de contrasenyes**: No es pot tornar a usar una de les N darreres contrasenyes usades en el passat. El sistema recordarà els hash de les que ja s'han fet servir.

Crear contrasenyes segures i consells per a fer-ho - https://passwordsgenerator.net/es/

---

### CREACIO DE CONTRASENYES SEGURES

Cal crear la contrasenya més forta possible, per superar atacs de força bruta, diccionari o de regles de construcció.

**Contrasenya feble**: Es considera que una contrasenya és feble o fàcilment deduïble si:
- Té menys de 12 caràcters.
- Es una paraula que apareix en un diccionari (en l'idioma que sigui).
- Es el nom d'un familiar, amic, mascota, personatge famós, etc. i es pot relacionar amb informació de l'usuari.
- Està relacionada amb dades personals, com ara data de naixement, adreça, etc.
- Segueix algun patró numèric, alfanumèric, d'ordre de tecles, etc. (12345, qwerty, aaabbb, etc.).
- No barregen majúscules i minúscules ni inclouen números i símbols.

---

![](imgs/3.seg-logica-d6d69063.png)

Temps necessari per a obtenir contrasenyes per força bruta segons la complexitat https://tech.co/password-managers/how-long-hacker-crack-password (2023)

---

Seguretat dels passwords: https://heimdalsecurity.com/blog/password-security-guide/

<img src="imgs/6.seg-logica-c799a468.png" width="700" align="center" />

---

Estimar la fortalesa de les contrasenyes amb "zxcvbn" - https://github.com/dropbox/zxcvbn

Contrasenyes fortes - https://securityinabox.org/es/passwords/passwords-and-2fa/#password-math

Generar frases de pas amb DICEWARE per múltiples idiomes
- https://theworld.com/~reinhold/diceware.html#languages
- Implementació web - https://diceware.dmuth.org/

---

### MESURES DE PROTECCIO DE CONTRASENYES (USUARIS)

Cal educar els usuaris i animar-los a seguir les següents directives respecte les contrasenyes:
- No escriure mai contrasenyes en correus-e.
- No dir mai la contrasenya a ningú (per telèfon, a companys ni que siguin superiors, familiars, etc.)
- No parlar de contrasenyes en llocs públics.
- No posar pistes per fer-la més fàcil de recordar i esbrinar, tot i que per exemple Windows obliga a fer-ho.
- No escriure-les mai en formularis (ni de seguretat)
- No escriure-la en papers ni post-it, ni enganxar-ho al monitor o sota el teclat.
- Canviar la contrasenya mínim cada 6 mesos.
- No escriure la contrasenya si algú observa (atac shoulder surfing).
- No fer servir la mateixa contrasenya a diferents serveis.
- Fer servir gestors de contrasenyes (exemple KeePass).
- Utilitzar autenticació en 2 passos (2FA=2 Factor Authentication).


Més informació sobre usabilitat de passwords: https://www.baekdal.com/thoughts/password-security-usability/

---

### MESURES DE PROTECCIO DE CONTRASENYES (ADMINISTRADORS)

Els administradors de sistemes també han de seguir unes certes directives respecte les contrasenyes:
- **No permetre accedir a ningú** al magatzem de contrasenyes, que pot ser un fitxer com ara /etc/passwd o una base e dades. Això s'implementa amb Control d'accés i Drets.

- Cal **guardar un hash** de les contrasenyes al magatzem de passwords. MAI es podran guardar en clar.

- **Codificar contrasenyes iguals de formes diferents** (en Linux, usar SALT en fitxer /etc/shadow).

- **Comprovar activitats sospitoses** dels usuaris. Això s'implementa analitzant els logs del sistema, per ex. moltes fallades de login en poc temps.

- **Blocar temporalment comptes** d'usuari en cas de detectar activitats sospitoses. No blocar de forma definitiva per que permetrà un atac DoS contra els comptes d'usuari.

- **Blocar equips remots** que fan activitats sospitoses en serveis d'autenticació, per exemple per la seva adreça IP.

- **Comprovar fortalesa de contrasenyes** quan es canviï la contrasenya. El sistema comprova la fortalesa de la nova en el moment de creació, per exemle pam_cracklib en Linux.

---

### ATACS A LES CONTRASENYES

- **Enginyeria social**: Bàsicament, enganyar els usuaris per que te la diguin.
- **Força bruta**: generar totes les possibles combinacions de n caràcters. Ajuda al procès saber la longitud i quins caràcters pots usar.
- **Diccionari**: confeccionar una llista de paraules probables i anar provant una per una.
- **Diccionari temàtic**: Estudiar a l'usuari i esbrinar si li agrada algun tema en concret, per exemple, El senyor dels anells i crear una llista de paraules relacionades amb l'obra.
- **Espiar**
	- Mirar directament quan es posa.
	- Capturar paquets de xarxa amb sniffers i analitzar-ho.
	- Usar Keyloggers, etc.

#### EINES PER INTERCEPTAR CONTRASENYES EN XARXA

Interceptar contrasenyes en xarxa:
- wireshark
- net-creds
- dsnif
- cain (win)
- ettercap


#### EINES PER ATACAR CONTRASENYES OFF-LINE

Atacar el magatzem de contrasenyes (off-line):
- john the ripper (multi)
- hashcat (multi)
- loph crack (win)
- sam inside (win)
- cain (win)

#### EINES PER ATACAR CONTRASENYES ON-LINE

- Atacar per xarxa (on-line)
	- THC Hydra
	- ncrack
	- medusa
	- patator

---

## 3. REGISTRES D'USUARIS, INCIDENCIES I ALARMES (LOGS)

Moltes aplicacions guarden informació sobre el funcionament de l'aplicació per tenir-ne en cas de fallada. Són els fitxers o BD de «LOGS» o «bitàcoles».

Els més importants són:
- Registres del **Sistema Operatiu (SOP)**.
- Registres del **programari de seguretat**.

### REGISTRES DEL SISTEMA OPERATIU

Guarden registres sobre els esdeveniments que succeeixen al sistema. Tenim de dos tipus:
- **Esdeveniments del sistema**: Produïts pels components del propi sistema. Ex: Apagar, iniciar servei, fallada de disc, etc.

- **Esdeveniments d'usuari**: Produïts per accions dels usuaris, com ara esborrar fitxer, iniciar sessió al sistema, equivocar-se en posar la contrasenya...

Els **esdeveniments que es recullen** al registre de successos són molt valuosos en cas d'atac o de fallada.
- **Rendiment** del sistema.
- **Intents d'accés** al sistema (inici de sessió fallit o no)
- **Identitat dels usuaris** que accedeixen al sistema i data/hora.
- **Bloqueig de comptes** d'usuari per inicis fallits.
- **Ús d'eines d'administració**.
- **Dispositius accedits**.
- Peticions de **canvis en arxius de configuració**.

---

### REGISTRES DE PROGRAMARI DE SEG.

### REGISTRES DE PROGRAMARI DE SEG.

El següent programari de seguretat genera els seus propis logs:

- **Antivirus**: Registre del programari maliciós detectat i/o desinfectat així com els fitxers afectats. També actualitzacions i escanejos del sistema realitzats.
- **Encaminadors (routers)**: Estadístiques de funcionament, bloqueig de paquets, etc.
- **Tallafocs**: Registre de la seva activitat i bloquejos de paquets.
- **Proxy's (servidors intermediaris)**: Registre de totes les peticions que arriben i el resultat.
- **Programari d'accés remot**: Registre dels intents d'accés.


---

<img src="imgs/3.seg-logica-24f49851.png" width="600" align="center" />

Windows Event Viewer (visor de succesos)

<img src="imgs/3.seg-logica-3ef06c34.png" width="600" align="center" />

Visor de logs "Registres" (Ubuntu)

---

### GESTIO DE REGISTRES

Per a la gestió dels registres al sistema, cal tenir en compte els següents aspectes:
- **Evitar tenir massa fonts de registres**, ja que la gestió es dificulta. Si pot ser, tot organitzat en un **únic lloc**.

- **Evitar la inconsistència de dades**: Cal recollir **totes les dades rellevants**. Ex: recollir IP però no nom d'usuari, per exemple.


- **Assegurar Consistència temporal**: Tots els registres han de portar **data i hora**. Cal sincronitzar els sistemes informàtics per que no hi hagi cap discrepància en les marques de temps. La sincronització es sol fer per xarxa usant un servei d'hora NTP=Network Time Protocol.

- **Assegurar Consistència de formats**: Mantenir el mateix format en fitxer de registre (no barrejar per ex. XML, valors separats per comes, BD, etc).

- **Registrar Informació sensible**: Com a mínim cal **registrar l'activitat del root** o admin i de comptes sensibles i importants. Aquests són comptes d'usuari molt desitjables pels atacants.

- **Usar eines per analitzar els registres**, buscar, fer estadístiques, gràfiques, etc.

- **Gestionar la rotació i mida dels logs**: Per evitar desbordament dels fitxers de logs o informació no útil per obsolescència. Per exemple, a Linux existeix el servei "logrotate" que s'ocupa d'anar guardant en fitxers comprimits els de dies anteriors fins a un cert límit que configura l'administrador. A windows també es pot configurar la rotació de logs.

- **Gestionar el nivell de detall**: Segons el cas, necessitem més o menys informació. Per exemple, en un servei web, fent proves o després d'un atac necessitem tot el detall possible. En producció potser només errors crítics. Si ens descuidem i deixem un nivell informatiu de logs molt alt, el disc es pot omplir molt ràpid.

---

### PROTECCIO DELS REGISTRES

Si es produeix un atac, **l'atacant intentarà esborrar els registres que puguin incriminar-lo**. Sense ells, no ens adonarem de l'atac. Per evitar-ho:
- Protegir els registres per que **només certs usuaris administradors i personal de seguretat hi accedeixin** amb ACL's de sistema d'arxius o registre.
- **Assegurar la integritat** amb mètodes criptogràfics (hash) per detectar si algú fa canvis no autoritzats en els logs.
- Podem **assegurar la confidencialitat amb criptografia**, xifrant la informació de logs per que no sigui llegible.
- Es poden fer **còpies de seguretat** dels registres per evitar la pèrdua accidental.
- Es poden **guardar els registres en un altre equip** a través de la xarxa, de manera que sigui més difícil accedir-hi.

---

## MES INFORMACIO

- Patator - https://github.com/lanjelot/patator

## ACL’s al sistema d’arxius NTFS de Windows:

- Informació completa sobre NTFS: http://www.pcguide.com/ref/hdd/file/ntfs/index.htm
- https://espace.cern.ch/winservices-help/NICESecurityAndAntivirus/NICESecurityHowTo/Pages/ManagingACLSettingPermssion.aspx
- http://www.ntfs.com/ntfs-permissions-acl-use.htm
- https://technet.microsoft.com/en-us/library/cc781134(v=ws.10).aspx#w2k3tr_ntfs_how_dhao
