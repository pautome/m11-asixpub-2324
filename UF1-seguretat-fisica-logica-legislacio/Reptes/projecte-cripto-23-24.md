# REPTE: Projectes amb criptografia

**ATENCIÓ: La informació proporcionada té un caràcter educatiu i en cap cas se'n pot fer un ús delictiu o maliciós. Cal fer un ús responsable i ètic dels coneixements adquirits. MAI ho heu de fer amb un equip de producció (màquina física o de la feina). En cas contrari, es pot incòrrrer en delictes tipificats al codi Penal amb penes que poden suposar multes i fins i tot presó.**

**ATENCIÓ**: Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, etc. han de ser el vostre nom+cognom per a que a les captures es pugui veure.

Llegiu primer tot el document abans de començar la pràctica.

---

## Objectius

- Volem que l'equip dels usuaris sigui més segur aplicant criptografia. Així que implementarem mesures de seguretat que usen criptografia.
- Escolliu un dels següents projectes i documenteu el procés per lliurar-ho al Moodle.
- Cal ensenyar a classe que el projecte està implementat i funciona.

---

**És molt important que tot el que feu sigui a un disc secundari de la MV per a poder moure'l a una altra màquina virtual i fer el procés d'accés als arxius, carpetes, particions i discos xifrats. En el cas del disc principal, cal fer la prova per veure com es pot protegir el disc d'arrencada i en el cas que el maquinari falli, es pugui fer tot en un altre ordinador.**

---

## 1. Xifrat de discos, particions, carpetes o fitxers a Linux.

### Repte

- Estudia i documenta com es pot fer per a xifrar a nivell de disc o partició sencera (per exemple, posar /home en una partició i xifrar-la).

- Estudia i documenta com es pot fer per a xifrar un directori qualsevol en Linux, de manera que es pugui fer servir els arxius que contingui interactivament i de forma transparent.

- Estudia i documenta com es pot fer per a xifrar un sol fitxer en Linux (per exemple, un fitxer que contingui la formula de la Coca Cola). Indica si estàs fent servir criptografia simètrica (de clau compartida) o asimètrica (de clau pública).

- Estudia i documenta com crear un volum xifrat (fitxer de disc virtual contenidor de dades xifrat) en Linux.

- Indica en tots els casos quin mecanisme es fa servir per a guardar les claus en cas d'emergència.

Demostració a classe:
- Implementa en una màquina virtual **TOTES** les solucions i documenta el procés.
- **Mou el disc, contenidor xifrat o arxiu a una altra màquina virtual i desxifra allà els arxius.** TOTS els fitxers i carpetes xifrats s'han de poder desxifrar i **comprovar el seu contingut**.

---

## 2. Xifrat de discos, particions, carpetes o fitxers a Windows.

### Repte

- Estudia i documenta com es pot fer per a xifrar a nivell de disc o partició sencera. Utilitza les eines de Windows (Bitlocker i EFS)

- Estudia i documenta com es pot fer per a xifrar un directori en Windows, de manera que es pugui fer servir els arxius que contingui interactivament i de forma transparent.

- Estudia i documenta com es pot fer per a xifrar un sol fitxer en Windows (per exemple, un fitxer que contingui la formula de la Coca Cola).

- Estudia i documenta com crear un volum xifrat (fitxer de disc virtual contenidor de dades xifrat) en Windows.

- Indica en tots els casos quin mecanisme es fa servir per a guardar les claus en cas d'emergència.


Demostració a classe:
- Implementa en una màquina virtual **TOTES** les solucions i documenta el procés.
- **Mou el disc, contenidor xifrat o arxiu a una altra màquina virtual i desxifra allà els arxius.** TOTS els fitxers i carpetes xifrats s'han de poder desxifrar i **comprovar el seu contingut**.

---

### Referències

#### Linux

- Disk encryption - https://en.wikipedia.org/wiki/Disk_encryption
- Filesystem-level encryption - https://en.wikipedia.org/wiki/Filesystem-level_encryption

- (2023-24) Ecryptfs (Enterprise Cryptographic Filesystem). Xifrat de directoris.
  - https://keepcoding.io/blog/que-es-ecryptfs/
  - https://wiki.archlinux.org/title/ECryptfs
  - https://ubunlog.com/ecryptfs-encripta-carpeta-usuario/

- Veracrypt, volums xifrats
  - https://www.veracrypt.fr/code/VeraCrypt/
  - https://securityinabox.org/es/guide/veracrypt/linux/

- LUKS (Linux Unified Key Setup), que ja ve integrat en Ubuntu i d’altres. (En la instal·lació es pot crear la carpeta personal xifrada). Permet xifrar carpetes i/o volums. https://gitlab.com/cryptsetup/cryptsetup/wikis/home
- GPG (GnuPG) que permet xifrat simètric i asimètric de fitxers.
  - https://ciberseguridad.com/guias/prevencion-proteccion/criptografia/cifrado-pgp/
  - https://edoceo.com/sys/gpg
  - https://www.madboa.com/geek/gpg-quickstart/

---

#### Windows

- EFS=Encryption File System). Permet xifrar carpetes i fitxers. https://support.microsoft.com/es-pa/help/324897
- BitLocker, no funciona en versions “home”. Permet xifrar particions i discos complets. Ajuda: https://technet.microsoft.com/es-es/library/dd835565(v=ws.10).aspx i https://docs.microsoft.com/es-es/windows/device-security/bitlocker/bitlocker-overview.
- Veracrypt, volums xifrats.
  - https://www.veracrypt.fr/code/VeraCrypt/
  - https://securityinabox.org/es/guide/veracrypt/windows/
- GPG (GnuPG) que permet xifrat simètric i asimètric de fitxers.
  - manual - https://www.gnupg.org/documentation/manuals/gnupg.pdf
  - quickstart - https://www.madboa.com/geek/gpg-quickstart/
  - https://ciberseguridad.com/guias/prevencion-proteccion/criptografia/cifrado-pgp/
  - https://edoceo.com/sys/gpg
