# Virtualització

**INS Carles Vallbona**

**Pau Tomé**

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Virtualització](#virtualitzaci)
	- [Què és la virtualització?](#qu-s-la-virtualitzaci)
	- [Quins avantatges te la virtualizació?](#quins-avantatges-te-la-virtualizaci)
	- [Quins inconvenients te la virtualizació?](#quins-inconvenients-te-la-virtualizaci)
	- [Tipus de virtualizació:](#tipus-de-virtualizaci)
		- [1. Emulació](#1-emulaci)
		- [2. Virtualització total](#2-virtualitzaci-total)
		- [3. Paravirtualització](#3-paravirtualitzaci)
		- [4. Virtualització a nivell de sistema operatiu (Contenidors)](#4-virtualitzaci-a-nivell-de-sistema-operatiu-contenidors)
	- [Concepte d'Hipervisor](#concepte-dhipervisor)
		- [Hipervisor Tipus I (BAREMETAL)](#hipervisor-tipus-i-baremetal)
		- [Hipervisor Tipus II (HOSTED)](#hipervisor-tipus-ii-hosted)
	- [Fonts d'informació](#fonts-dinformaci)

<!-- /TOC -->

---

## Què és la virtualització?

És una capa de maquinari i programari que amaga els elements reals per a presentar a l'usuari i/o gestor uns recursos i informació, anomenats **sistemes virtuals**, sense necessitat d'accedir directament als elements discrets (disc, memòria, CPU, xarxa...).”

- Permet realitzar una **abstracció dels recursos d'un sistema creant una capa entre el hardware de l'equip físic i el sistema operatiu** de la màquina virtual.

- Aquesta capa gestiona els recursos principals de l'equip amfitrió com són la **CPU, memòria, xarxa i emmagatzematge**.

![](imgs/20180413-082940.png)

- Una de les aplicacions més comuns de la virtualització es **poder independitzar l'administració de servidors sota una mateixa màquina física**.

---

## Quins avantatges te la virtualizació?

- **Estalvi de costos:** Podem adquirir un únic servidor, més potent, i no haver de comprar més servidors solament anar creant-los en el gestor de màquines virtuals. També permet estalvi en el cost de manteniment i en el de personal a més d'estalviar espai i energia.

- **Creixement més flexible:** Instal·lar un nou servidor és molt més senzill i ràpid davant de fer-ho amb un de físic.

- **Administració simplificada:** En la consola del gestor de màquines virtuals podem:
  - augmentar o reduir els recursos per a una determinada màquina
  - reiniciar-la
  - instal·lar actualitzacions
  - esborrar-la.

- **Aprofitament d'aplicacions antigues:** Conservar aplicacions que funcionen en sistemes antics en una màquina virtual i modernitzar la infraestructura informàtica.

- **Disminució dels temps de parada:** pot reduir el temps de parada per exemple amb clonacions. Mentre el clon dona servei a l'altra es fa un manteniment d'actualitzacions. La restauració d’un equip virtual és ràpida i independent del maquinari real.

- **Centralització de tasques de manteniment:** Podem realitzar còpies de seguretat en una única vegada de totes les màquines, programar actualitzacions i altres activitats utilitzant el gestor de màquines virtuals.

- **Millor gestió de recursos:** Es pot augmentar la memòria o emmagatzematge de la màquina física per fer-ho també de totes les màquines virtuals a la vegada. Això fa que s'aprofitin millor les inversions en hardware.

- **Balanceig de recursos:** Es pot assignar un grup de servidors físics per a proporcionar recursos a les màquines virtuals i assignar una aplicació que faci un balanceig dels recursos, atorgant més memòria, recursos de processador, emmagatzematge o ampla de banda de la xarxa a la màquina virtual que ho necessiti.

- **Continuïtat de negoci i recuperació davant desastres:** En cas de fallada d’un sistema físic, els sistemes lògics continguts poden distribuir-se dinàmicament a d’altres sistemes.

- **Virtual appliance:** màquines virtuals preconfigurades, "carregar i funcionar". Màquines "paquetitzades" i preconfigurades per una funció determinada (servidors de correu, bases de dades, centralites VoIP, aplicacions tancades).

---

## Quins inconvenients te la virtualització?

- Necessita hardware d’altes prestacions.
- Molts sistemes depenen d’un sol equip físic (si no tenim sistemes HA).
- Limitacions en el hardware de les màquines virtuals (hi ha poca diversitat de dispositius suportats).
- Existeixen problemes d’emulació de certs controladors.
- El rendiment és inferior al d'una màquina real.

---

## Tipus de virtualizació:

Podem trobar els següents tipus.
- **Emulació**
- **Virtualització total**
- **Paravirtualització**
- **Virtualització a nivell del sistema operatiu (Contenidors)**


![](imgs/20180416-112211.png)

Tipus de Virtualització

---

### 1. Emulació

**Emulació Hardware:** una capa software simula la funcionalitat de components hardware (o software).

- El maquinari subjacent normalment no coincideix en molts casos amb el que s'emula. Realment, el sistema i/o programes són traduïts i no s'executen directament sobre la màquina subjacent real.
- Normalment usat per a executar programes o sistemes operatius antics.
- Implica penalització en el rendiment ja que **el maquinari es simula** per programari.

- Ex: Bochs, DOSBox, emuladors d'Android de codi ARM a x86, emuladors d'arcade, Z80, etc.

![](imgs/20180416-113023.png)

---

### 2. Virtualització total

Emula una quantitat suficient de hardware per a que moltes instàncies d'un SO, modificat o no, funcionin concurrentment.

- El sistema i programes **s'executen realment** en el maquinari subjacent. La capa de programari fa d'intermediari entre les màquines virtuals i el maquinari real.

- Exemple: Vmware, Virtual PC, VirtualBox, QEMU.

![](imgs/20180416-113053.png)

---

### 3. Paravirtualització

La màquina virtual no necessàriament emula el hardware, si no que en el seu lloc (o a més) ofereix una sèrie d'APIs a un SO **modificat** per utilizar-les.

- Aquests sistemes ofereixen millor rendiment (s'apropen a un rendiment en màquina física).

- Ex: Xen, Sun xVM, User Mode Linux (UML)...


**Inconvenient**: Requereixen modificar el Sistema operatiu virtualitzat, i els que són propietaris no faciliten el codi font.

![](imgs/20180416-113400.png)

---

### 4. Virtualització a nivell de sistema operatiu (Contenidors)

El propi nucli del sistema operatiu permet crear múltiples instancies aïllades del sistema operatiu natiu en lloc d'una sola.

- Aquestes instàncies (**contenidors**) tenen entitat pròpia i són vistes com un servidor independent.

- Exemples: Linux-VServer, OpenVZ, Docker.

![](imgs/20180416-114136.png)

---

## Concepte d'Hipervisor

**Hipervisor o virtual machine monitor (VMM):** Capa de programari/maquinari que permet fer veure que tenim vàries màquines virtuals, de manera que es poden executar a sobre d'aquestes diferents sistemes operatius.

---

### Hipervisor Tipus I (BAREMETAL)

**Tipus I:** També anomenat natiu, "unhosted" o "bare metal" (sobre el metall nu), és programari que s'executa directament sobre el maquinari sense sistema operatiu, per oferir la funcionalitat de màquines virtuals.

**Característiques:** Millor rendiment i control, però tenen la limitació de hardware suportat reduït. Només tenen drivers per a uns determinats equips físics.

Ex: VMware ESXi (propietari), Xen (lliure), Citrix XenServer (gratis limitat a 4 maquines virtuals), Microsoft Hyper-V Server, Oracle VM (gratis).

![](imgs/20180416-115045.png)

---

### Hipervisor Tipus II (HOSTED)

**Tipus II:** També anomenat "hosted", és programari  que s'executa sobre un sistema operatiu per oferir la funcionalitat de màquines virtuals.

**Característiques:** Menor rendiment que el Baremetal, però tenen suport per a més hardware ja que corren sobre el sistema operatiu.

![](imgs/20180416-115421.png)

---

## Fonts d'informació

- https://www.welivesecurity.com/la-es/2014/07/28/virtualizacion-o-emulacion-esa-es-la-cuestion/
- https://en.wikipedia.org/wiki/Virtualization
- https://es.wikipedia.org/wiki/Hipervisor
- https://es.wikipedia.org/wiki/Paravirtualizaci%C3%B3n
- https://es.slideshare.net/pakus/virtualizacion-3246213
- https://www.vmware.com/techpapers/2007/understanding-full-virtualization-paravirtualizat-1008.html
- Appliances: https://www.infoworld.com/article/2626259/data-center/server-virtualization-eight-great-virtual-appliances-for-vmware-free-for-the-downloading.html

---

Per a l'hipervisor es fa servir:

- **Traducció binària:** S'emula un conjunt d'instruccions (ex: plataforma x86 de 32 bits) que permet no haver de modificar el sistema operatiu convidat (guest). Ex: VMWare, VirtualBox.

- **Suport de virtualizació nativa:** La plataforma x86 te alguns problemes (el sistema operatiu assumeix que té control absolut de la màquina) que fan que sigui difícil la virtualització. Així que les tecnologíes Intel VT y AMD-V permeten la virtualització sense modificar el sistema operatiu. Ex: KVM, VMware, VirtualBox, etc.
