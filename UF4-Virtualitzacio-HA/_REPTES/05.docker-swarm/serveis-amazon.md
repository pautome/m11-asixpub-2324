# Serveis disponibles a AWS

Aquí hi ha un recull de les descripcions d'alguns dels serveis més destacats que ofereix AWS (https://aws.amazon.com/es)

## 1. Computació

### EC2 (Amazon Elastic Compute Cloud)

- https://aws.amazon.com/es/ec2/features/

Consisteix en **instàncies de màquines virtuals**:
- De diferents mides i preus.
- Es pot optimitzar la CPU, memòria, emmagatzemament i xarxa.
- Utilitza processadors de Intel, AMD, NVIDIA y AWS i es pot optimitzar per rendiment i cost.
- Es pot utilitzar emmagatzemament local i opcions de xarxa millorades i diferents tipus d'instàncies per millorar el rendiment de càrrega de treball vinculades a l'E/S del disc o xarxa.
- També hi ha l'opció d'instàncies "bare metal" de manera que les aplicacions poden accedir directament al processador i la memòria del servidor subjacent per executar-les en entorns no virtualitzats o per aplicacions on es vulgui fer servir un hipervisor propi.

Es disposa també de l'eina **AWS Compute Optimizer** per obtenir orientacions sobre quins recursos AWS calen en el nostre cas segons la càrrega de treball i així reduir costos i millorar el rendiment.

Es pot consultar els tipus d'instàncies a: https://aws.amazon.com/es/ec2/instance-types/

---

### Lambda

- https://aws.amazon.com/es/lambda/features/

És un servei informàtic **sense servidor que executa codi com a resposta a esdeveniments i administra automàticament els recursos informàtics subjacents**.
- Els esdeveniments poden ser canvis d'estat o una actualització com ara que un usuari posi un article al carret de la compra en un lloc web de comerç electrònic.

- Es pot fer servir Lambda per a ampliar altres serveis AWS amb lògica personalitzada o crear els serveis propis backend que funcionin a escala, rendiment i seguetat de AWS. L'única cosa que cal fer és proporcionar el codi.


Alguns esdeveniments que poden fer que Lambda executi codi són entre d'altres:
- sol·licituts HTTP a través d'Amazon API Gateway
- les modificacions d'objetes en els buckets de Amazon S3
- les actualitzacions de taules en Amazon DynamoDB
- les transicions d'estat en AWS Step Functions.

Lambda disposa d'una infraestructura d'alta disponibilitat i gestiona automàticament l' administració dels recursos informàtics, com ara:
- manteniment del servidor i del sistema operatiu.
- de l'aprovisionament de capacitat i l'escalat automàtic.
- de la implementació de codi i de pegats de seguretat.
- del monitoratge i el registre del codi.

---

## 2. Emmagatzemament

- Comparativa EBS-EFS-S3 - https://www.nubersia.com/es/blog/amazon-ebs-amazon-efs-o-amazon-s3-cual-es-la-mejor-opcion-de-almacenamiento/

### EBS (Amazon Elastic Block Store)

- https://aws.amazon.com/es/ebs/features/

EBS Permet crear volums d'emmagatzemament virtual que es poden adjuntar lliurement a les diferents instàncies de EC2.
- EBS és l'emmagatzemament per defecte de les instàncies EC2.
- Es pot treballar sobre el volum creant sistemes d'arxius, executar BD o qualsevol ús que normalment es faria amb els discos.
- Aquests volums estan disponibles en una zona determinada i es repliquen automàticament per evitar els possibles errors dels components individuals (discos).
- Permeten instantànies (snapshots)
- Donen una disponibilitat del 99,999 %.

Generalment EBS s'utilitza per proporcionar recolzament i recuperació de desastres o emmagatzemament més tradicional per a càrrega de treball al núvol com ara bases de dades relacionals i altres "Business Applications".

---

### EFS (Amazon Elastic File System)

Sistema d'axius elàstic simple, sense servidor, "listo para usar" que permet compartir dades d'arxius sense aprovisionar ni administrar l'emmagatzemament.

Es pot usar amb serveis d' AWS i recursos locals, i està dissenyat per a escalar a petabytes sota demanda sense interrompre les aplicacions.

Amazon EFS està ben adaptat per a admetre un ampli espectre de casos d'ús, des de directoris particulars fins a aplicacions fonamentals per a l'empresa. Els casos d'ús inclouen l'emmagatzematge per a aplicacions en contenidors i sense servidor, l'anàlisi de big data, el servei web i l'administració de continguts, el desenvolupament i les proves d'aplicacions, els fluxos de treball de mitjans i entreteniment i les còpies de seguretat de bases de dades.

Aquest servei ofereix emmagatzematge simple, escalable i elàstic basat en discos d'estat sòlid (SSD) sota demanda per a instàncies EC2 i escala en funció de les necessitats d'emmagatzematge, ofereix una interfície simple que et permet crear i configurar sistemes d'arxius de manera ràpida i senzilla.

EFS és molt recomanable per a tasques a curt termini que poden ser sensibles al rendiment, com per exemple els entorns de desenvolupament. Aquest servei es pot utilitzar també en tasques a llarg termini com a repositori de dades per a servidors de mitjans per exemple.

---

### S3 (Amazon Simple Storage Service)

- https://aws.amazon.com/es/s3/features/

S3 permet crear **recursos d'emmagatzematge anomenats "buckets"** on s'hi poden guardar **dades en forma d'objectes**. Aquests objectes poden tenir fins a 5 TB de mida i s'hi pot:
- Annexar etiquetes de metadades a cada objecte.
- Moure i guardar dades entre les diferents classes d'emmagatzemament de S3 (canvien de classe segons la freqüència d'accés a les dades i es van migrant a classes més barates però més lentes en accés).
- Configurar i aplicar controls d'accés a les dades i protegir-les contra usuaris no autoritzats.
- Executar anàlisis de Big Data
- Monitorar les dades a nivell d'objecte i bucket
- Revisar l'ús de l'emmagatzemament i les tendències d'activitat.
- Accedir als objectes mitjançant punts d'accés S3 o directament usant el nom d'allotjament del bucket.       

Enfocat al **contingut estàtic** de les webs, aplicacions, hosting, etc.

S3 ofereix la possibilitat de configurar el cicle de vida dels objectes per administrar-los de manera econòmica durant tot el seu cicle de vida: es pot definir el moment en que un objecte passi d'una classe de emmagatzemament a una altra més barata (ja que s'accedeix menys).

---

### Conclusió

- S3 és **el servei més barat**, configurable i **accessible des de qualsevol lloc**.
- EBS **només és accessible des d'una instancia** Amazon EC2. Pot generar problemes per alguns tipus d'aplicacions.
- EFS és **molt més car** que EBS, però EFS es més potent en sistemes d'arxius compartits i escalables.

Cal considerar que avui dia hi ha aplicacions que poden usar les tres opcions segons la modularitat que tinguin.

Cal aconseguir el millor equilibri entre las necessitats, cost i rendiment desitjat per poder escollir l'opció més correcta.

---

## 3. Bases de dades

### RDS (Relational Database Service)

- https://aws.amazon.com/es/rds/features/

Sistemes de BD relacionals amb sis gestors de BD com són:
- Amazon Aurora
- MySQL
- MariaDB
- Oracle
- Microsoft SQL Server
- PostgreSQL

Així les aplicacions desenvolupades ja funcionaran a AWS. RDS gestiona
- l'aprovisionament de les BD
- revisions
- còpies de seguretat
- recuperació de còpies
- detecció d'errors
- reparació de les BD

---

### Aurora

- https://aws.amazon.com/es/rds/aurora/features/

Amazon Aurora és un servei de base de dades relacional que combina la velocitat i la disponibilitat de les bases de dades comercials d'alta gamma amb la simplicitat i la rendibilitat de les bases de dades de codi obert.
- És **compatible amb MySQL i PostgreSQL**, de manera que les eines i les aplicacions de MySQL existents puguin executar-se sense necessitat de modificar res.

---

### Amazon DynamoDB

- https://aws.amazon.com/es/dynamodb/features/

Amazon DynamoDB és una **base de dades NoSQL** que admet models de dades de documents i clau valor. Els desenvolupadors poden utilitzar DynamoDB per a crear aplicacions modernes i "serverless" que poden començar a petita escala i arribar a una escala global per a admetre petabytes de dades i desenes de milions de sol·licituds de lectura i escriptura per segon.
- DynamoDB s'ha dissenyat per a executar aplicacions d'alt rendiment a escala d'Internet que sobrecarregarien les bases de dades relacionals tradicionals.

---

## 4. Xarxes

### VPC (Amazon Virtual Private Cloud)

- https://aws.amazon.com/es/vpc/features/

Amazon Virtual Private Cloud (VPC) és un servei que permet llançar recursos de AWS en una **xarxa virtual aïllada** de manera lògica definida per l'usuari.

Pot controlar tots els aspectes de l'entorn de xarxa virtual:
- selecció del seu **propi rang d'adreces IP**
- creació de **subxarxes**
- configuració de **taules d'encaminament i gateways** de xarxa...

Pot utilitzar tant IPv4 com IPv6 per a la majoria dels recursos de la VPC, la qual cosa ajuda a garantir l'accés segur i fàcil als recursos i les aplicacions.

Com un dels serveis essencials de AWS, Amazon VPC facilita la personalització de la configuració de xarxa de la VPC. Pot crear:
- una subxarxa amb accés públic per als servidors web que tinguin accés a Internet.
- col·locar els sistemes backend, com els servidors d'aplicacions o les bases de dades, en una subxarxa amb accés privat sense accés a Internet.

Amazon VPC li permet utilitzar diverses capes de seguretat, inclosos els grups de seguretat i les llistes de control d'accés a la xarxa, per a ajudar a controlar l'accés a les instàncies de Amazon Elastic Computing Cloud (Amazon EC2) en cada subxarxa.
