# Xarxa Overlay

Crear Docker Swarm

~~~
[node1] (local) root@192.168.0.12 ~ $ docker swarm init --advertise-addr 192.168.0.12
Swarm initialized: current node (nlvzthsrt2hbdi5dbxtomui8h) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-68cu1ljeuvm814q9dt6v5tqy9crfshde2js693kfvn1luzoijb-aoa19gnq7assq3ci9tjbkgua7 192.168.0.12:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
~~~

Afegir un node Worker
~~~
[node2] (local) root@192.168.0.13 ~
$ docker swarm join --token SWMTKN-1-68cu1ljeuvm814q9dt6v5tqy9crfshde2js693kfvn1luzoijb-aoa19gnq7assq3ci9tjbkgua7 192.168.0.12:2377
This node joined a swarm as a worker.
~~~

Comprovar nodes del Swarm
~~~
[node1] (local) root@192.168.0.12 ~
$ docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY  MANAGER STATUS      ENGINE VERSION
nlvzthsrt2hbdi5dbxtomui8h *   node1               Ready               Active  Leader              19.03.4
ue8us0y0eoimbkfmm5tqyky9b     node2               Ready               Active                      19.03.4
~~~

Crear xarxa overlay
~~~
[node1] (local) root@192.168.0.12 ~
$ docker network create -d overlay overnet
9vzjazqqk05g78zvunad9vei7

[node1] (local) root@192.168.0.12 ~
$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
84b7eddc346e        bridge              bridge              local
736c0f11338e        docker_gwbridge     bridge              local
674f677d9276        host                host                local
xigl8kd2d576        ingress             overlay             swarm
ec2835be9a73        none                null                local
9vzjazqqk05g        overnet             overlay             swarm
~~~

Veure les xarxes
~~~
[node2] (local) root@192.168.0.13 ~
$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
1d1d78ecaf7b        bridge              bridge              local
e51aa4f80c87        docker_gwbridge     bridge              local
b432773150a5        host                host                local
xigl8kd2d576        ingress             overlay             swarm
d40879c27674        none                null                local
~~~

Comprovar paràmetres de xarxa
~~~
[node1] (local) root@192.168.0.12 ~
$ docker network inspect overnet
[
    {
        "Name": "overnet",
        "Id": "9vzjazqqk05g78zvunad9vei7",
        "Created": "2020-02-21T11:55:29.1484476Z",
        "Scope": "swarm",
        "Driver": "overlay",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "10.0.0.0/24",
                    "Gateway": "10.0.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": null,
        "Options": {
            "com.docker.network.driver.overlay.vxlanid_list": "4097"
        },
        "Labels": null
    }
]
~~~

Crear servei connectat a la xarxa:
~~~
[node1] (local) root@192.168.0.12 ~ $ docker service create --name myservice \
> --network overnet \
> --replicas 2 \
> ubuntu sleep infinity
fv3c3qwx8hz4gvupmq18aw2cw
overall progress: 2 out of 2 tasks
1/2: running
2/2: running
verify: Service converged

[node1] (local) root@192.168.0.12 ~
$ docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE            PORTS
fv3c3qwx8hz4        myservice           replicated          2/2                 ubuntu:latest
~~~

- Veurem que hi ha una rèplica a cada node:
~~~
[node1] (local) root@192.168.0.12 ~ $ docker service ps myservice
ID                  NAME                IMAGE               NODE                DESIREDSTATE       CURRENT STATE                ERROR               PORTS
iapolytht7ul        myservice.1         ubuntu:latest       node1               Running            Running about a minute ago
xznqixmv7xl2        myservice.2         ubuntu:latest       node2               Running            Running about a minute ago
~~~

- I ara tenim dos containers que són les rèpliques del servei (10.0.0.3 i 10.0.0.4) i dos nodes (peers) a la xarxa overnet:
~~~
[node1] (local) root@192.168.0.12 ~
$ docker network inspect overnet
[
    {
        "Name": "overnet",
        "Id": "9vzjazqqk05g78zvunad9vei7",
        "Created": "2020-02-21T11:57:24.426217732Z",
        "Scope": "swarm",
        "Driver": "overlay",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "10.0.0.0/24",
                    "Gateway": "10.0.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "cd1f4c16a8e7455e607967e52ccebb9384e6a008f9f96c35ffedb756df6d00a1": {
                "Name": "myservice.1.iapolytht7ul0ucfuijm5ze1y",
                "EndpointID": "e31809879f19dde33a585ef92e4f3bf1ae07118a8326be64a9c973820f31ee8c",
                "MacAddress": "02:42:0a:00:00:03",
                "IPv4Address": "10.0.0.3/24",
                "IPv6Address": ""
            },
            "lb-overnet": {
                "Name": "overnet-endpoint",
                "EndpointID": "255ad941115efdb30c1cebd72f8a369d62698c5a8d4b821bb2c0fb1eb1548dc0",
                "MacAddress": "02:42:0a:00:00:06",
                "IPv4Address": "10.0.0.6/24",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.driver.overlay.vxlanid_list": "4097"
        },
        "Labels": {},
        "Peers": [
            {
                "Name": "d47bfe234ab2",
                "IP": "192.168.0.12"
            },
            {
                "Name": "ede0682cba50",
                "IP": "192.168.0.13"
            }
        ]
    }
]
~~~

~~~
[node2] (local) root@192.168.0.13 ~
$ docker network inspect overnet
[
    {
        "Name": "overnet",
        "Id": "9vzjazqqk05g78zvunad9vei7",
        "Created": "2020-02-21T11:57:24.425540296Z",
        "Scope": "swarm",
        "Driver": "overlay",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "10.0.0.0/24",
                    "Gateway": "10.0.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "379ea7447b1d7aeb05d18f281d077de69ea3548fb5cc8237cad8c807f50ca909": {
                "Name": "myservice.2.xznqixmv7xl2vt7qprtlk0oe6",
                "EndpointID": "1df111aee49a308264c3b54ce8121a2fe2cebb8fae68df7c5c265fa4249a908e",
                "MacAddress": "02:42:0a:00:00:04",
                "IPv4Address": "10.0.0.4/24",
                "IPv6Address": ""
            },
            "lb-overnet": {
                "Name": "overnet-endpoint",
                "EndpointID": "b0927a28db57d43d2612123dc704b927ddc4b793536ce29cb6ad138a84fdd246",
                "MacAddress": "02:42:0a:00:00:05",
                "IPv4Address": "10.0.0.5/24",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.driver.overlay.vxlanid_list": "4097"
        },
        "Labels": {},
        "Peers": [
            {
                "Name": "ede0682cba50",
                "IP": "192.168.0.13"
            },
            {
                "Name": "d47bfe234ab2",
                "IP": "192.168.0.12"
            }
        ]
    }
]
~~~

- Podem veure que des d'un contenidor del node 2 podem fer ping a un contenidor del node 1.

~~~
[node2] (local) root@192.168.0.13 ~
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED  STATUS              PORTS               NAMES
379ea7447b1d        ubuntu:latest       "sleep infinity"    18 minutes ago  Up 18 minutes                           myservice.2.xznqixmv7xl2vt7qprtlk0oe6

[node2] (local) root@192.168.0.13 ~
$ docker exec -it myservice.2.xznqixmv7xl2vt7qprtlk0oe6 /bin/bash
root@379ea7447b1d:/# apt-get update && apt-get install -y iputils-ping
...
root@379ea7447b1d:/# ping 10.0.0.3
PING 10.0.0.3 (10.0.0.3) 56(84) bytes of data.
64 bytes from 10.0.0.3: icmp_seq=1 ttl=64 time=0.249 ms
~~~

- Podem veure que el fitxer /etc/resolv.conf del contenidor apunta a un DNS del swarm
~~~
root@379ea7447b1d:/# cat /etc/resolv.conf
search 51ur3jppi0eupdptvsj42kdvgc.bx.internal.cloudapp.net
nameserver 127.0.0.11
options ndots:0
~~~

- Així podem fer ping al servei que hem posat en marxa
~~~
root@379ea7447b1d:/# ping myservice
PING myservice (10.0.0.2) 56(84) bytes of data.
64 bytes from 10.0.0.2 (10.0.0.2): icmp_seq=1 ttl=64 time=0.170 ms
~~~

- Podem veure al node que la IP és la del servei (VirtualIPs):
~~~
[node1] (local) root@192.168.0.12 ~
$ docker service inspect myservice
[
    {
        "ID": "fv3c3qwx8hz4gvupmq18aw2cw",
        "Version": {
            "Index": 20
        },
        "CreatedAt": "2020-02-21T11:57:24.263892985Z",
        "UpdatedAt": "2020-02-21T11:57:24.266143472Z",
        "Spec": {
            "Name": "myservice",
            "Labels": {},
            "TaskTemplate": {
                "ContainerSpec": {
                    "Image": "ubuntu:latest@sha256:8d31dad0c58f552e890d68bbfb735588b6b820a46e459672d96e585871acc110",
                    "Args": [
                        "sleep",
                        "infinity"
                    ],
                    "Init": false,
                    "StopGracePeriod": 10000000000,
                    "DNSConfig": {},
                    "Isolation": "default"
                },
                "Resources": {
                    "Limits": {},
                    "Reservations": {}
                },
                "RestartPolicy": {
                    "Condition": "any",
                    "Delay": 5000000000,
                    "MaxAttempts": 0
                },
                "Placement": {
                    "Platforms": [
                        {
                            "Architecture": "amd64",
                            "OS": "linux"
                        },
                        {
                            "OS": "linux"
                        },
                        {
                            "Architecture": "arm64",
                            "OS": "linux"
                        },
                        {
                            "Architecture": "386",
                            "OS": "linux"
                        },
                        {
                            "Architecture": "ppc64le",
                            "OS": "linux"
                        },
                        {
                            "Architecture": "s390x",
                            "OS": "linux"
                        }
                    ]
                },
                "Networks": [
                    {
                        "Target": "9vzjazqqk05g78zvunad9vei7"
                    }
                ],
                "ForceUpdate": 0,
                "Runtime": "container"
            },
            "Mode": {
                "Replicated": {
                    "Replicas": 2
                }
            },
            "UpdateConfig": {
                "Parallelism": 1,
                "FailureAction": "pause",
                "Monitor": 5000000000,
                "MaxFailureRatio": 0,
                "Order": "stop-first"
            },
            "RollbackConfig": {
                "Parallelism": 1,
                "FailureAction": "pause",
                "Monitor": 5000000000,
                "MaxFailureRatio": 0,
                "Order": "stop-first"
            },
            "EndpointSpec": {
                "Mode": "vip"
            }
        },
        "Endpoint": {
            "Spec": {
                "Mode": "vip"
            },
            "VirtualIPs": [
                {
                    "NetworkID": "9vzjazqqk05g78zvunad9vei7",
                    "Addr": "10.0.0.2/24"
                }
            ]
        }
    }
]
~~~

- Per destruir el serveis

~~~
[node1] (local) root@192.168.0.12 ~
$ docker service rm myservice
myservice
[node1] (local) root@192.168.0.12 ~
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED  STATUS              PORTS               NAMES
~~~

- Per destruir el Swarm
~~~
[node1] (local) root@192.168.0.12 ~
$ docker swarm leave --force
Node left the swarm.

[node2] (local) root@192.168.0.13 ~
$ docker swarm leave
Node left the swarm.
~~~
