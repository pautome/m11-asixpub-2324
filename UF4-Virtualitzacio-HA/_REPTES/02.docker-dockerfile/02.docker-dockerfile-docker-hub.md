# Pràctica: Dockerfile i repositori Docker (Docker registry)

> ATENCIÓ: Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, etc. han de ser el vostre per a que a les captures es pugui veure.

> Teniu l'exercici a fer al final d'aquest enunciat. Llegiu primer tot el document abans de començar la pràctica.

---

## Referències i ajuda
- Ajuda de docker: Escriure "docker" o "docker COMMAND --help"
- Documentació de Docker - https://docs.docker.com/
- Tutorial de Docker - https://www.tutorialspoint.com/docker/index.htm

---

## Objectius

- L'objectiu d'aquesta pràctica és introduir l'automatització de la construcció d'imatges amb script (dockerfile) i el repository (registry) de Docker.
---

## Introducció: com es fa?

### Creació del Docker File

1. Crear un directori al disc local (per exemple: projecte1) i entrar dintre (cd projecte1).
1. Crear un fitxer de nom “Dockerfile” (nom per defecte) i escriure-hi a dins (les comandes mínimes són FROM, RUN, CMD). Exemple:

~~~
# FROM: indica quina és la imatge d'origen
# RUN: Executar comandes en el moment de construir la imatge (docker build) però NO quan inicia el contenidor. Són les comandes que havíem executat al contenidor per a confiurar-ho
# CMD: Comanda que ha d'executar el contenidor en iniciar. Només una comanda (si volem més d'una, cal fer un script
# o usar operador &&, tot i que la filosofia de Docker implica que només tindrem un servei per contenidor)

FROM docker/whalesay:latest
RUN apt-get -y update && apt-get install -y fortunes
CMD /usr/games/fortune -a | cowsay
~~~

**NOTA:** Teniu un fitxer Dockerfile de mostra al directori dels exercicis.

1. Un cop fet, executar la comanda docker build (hi ha moltes opcions al build). Es crearà la imatge utilitzant el fitxer Dockerfile. Amb la comanda d'exemple, crearem localment la imatge i li posarem un nom (tag). Normalment, es fa des del directori del projecte, per això es posa "." com a ruta al fitxer Dockerfile:
~~~
# docker build -t nom_imatge ruta_Dockerfile
docker build -t balena .
~~~

1. Ara podem iniciar un contenidor partint de la nova imatge. Fixeu-vos que el contenidor inicia el programa fortune de forma automàtica (no cal passar-li res com a paràmetre).
~~~
docker run --name lamevabalena balena
~~~


El procediment serà:
- Cada cop que necessitem un canvi en la imatge, canviem el Dockerfile
- Executem build per crear una imatge nova
- Finalment creem un contenidor nou a partir de la imatge (l'anterior contenidor ja l'haurem matat per que no serveix i és "ramat").

---

### El Repositori (registry) Docker Hub

1. Podem crear un compte a DockerHub per guardar les nostres imatges.

1. Per poder pujar la imatge, primer iniciem sessió al DockerHub
~~~
$ docker login -u pautome
Password:
Login Succeeded
~~~

1. Ara, si tenim un contenidor preparat (mirem amb docker ps) amb els canvis que volíem guardar a la imatge, fem commit i després push:

~~~
$ docker commit -a "Nom usuari <email@elquesigui.cat>" nom_contenidor repositori/nomimatge:versió

Exemple:
$ docker commit -a "Pau Tomé <pau.tome@iescarlesvallbona.cat>" elated_shannon pautome/miapache:1
sha256:5a29593e662601aecb7258d5da96024531832013e97aaed83b05c937d544a22e

$ docker push pautome/miapache:1
The push refers to repository [docker.io/pautome/miapache]
6fe37c743b70: Pushing [============================>                      ]  84.48MB/146.7MB
~~~

També podríem haver creat la imatge amb un Dockerfile i fet build:
~~~
# docker build -t repositori/nom_imatge:versió ruta_Dockerfile
docker build -t pautome/miapache:1 .
~~~

També podríem haver canviat el tag d'una imatge creada prèviament (amb commit o build):
~~~
$ docker tag IMAGE_ID repositori/nom_imatge:versió
~~~

---

### Buscar als repositoris

Podem comprovar que hem pujat imatges via comandes o a la web de dockerhub.

~~~
$ sudo docker search pautome
[sudo] contrasenya per a pau:
NAME                DESCRIPTION         STARS               OFFICIAL            AUTOMATED
pautome/editor1                         0                                       
pautome/miapache                        0                                       
pautome/repo        Test Repository     0     
~~~

O buscar altres imatges al repositori

~~~
$ docker search apache
NAME                               DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
httpd                              The Apache HTTP Server Project                  3235                [OK]                
tomcat                             Apache Tomcat is an open source implementati…   2872                [OK]                
cassandra                          Apache Cassandra is an open-source distribut…   1207                [OK]                
...
~~~

---

### Exercici DockerFile i DockerHub

1. Crea la imatge de l'exercici del reproductor de música afegint a un Dockerfile totes les passes fetes "manualment". Afegeix la comanda CMD per a que en llançar el contenidor es cridi automàticament a la descàrrega i execució de la cançó. Pots crear un script que executi les dues comandes.

2. Crea un contenidor a partir d'aquesta imatge (docker build). Prova que funciona amb "docker run" a partir d'aquesta imatge creada.

3. Puja la imatge al DockerHub (captura pantalla del teu compte dockerhub on es vegi imatge penjada). Comprova des de consola de comandes que la imatge existeix al repositori de Docker.

4. Dockeritza (crea un Dockerfile) una aplicació que et doni el missatge d'una galeta de la Fortuna:
- Instal·lar fortune, lolcat i cowsay.
- Arrencar contenidor que permeti escollir mostrar o bé una galeta de la fortuna llarga en colors, o bé que mostri una vaca explicant una galeta de la fortuna llarga.

5. Dockeritza una aplicació que permeti veure una simulació de hacking a l'estil de Hollywood.

6. Afegeix les imatges creades al teu repositori de Docker 
