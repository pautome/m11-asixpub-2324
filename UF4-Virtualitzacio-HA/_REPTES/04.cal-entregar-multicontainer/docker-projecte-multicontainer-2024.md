# REPTE: Projectes Docker Multicontenidor

**ATENCIÓ:** Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, etc. han de ser el vostre per a que a les captures es pugui veure.

Teniu l'exercici a fer al final d'aquest enunciat. Llegiu primer tot el document abans de començar la pràctica.

---

## Referències i ajuda
- Ajuda de docker: Escriure "docker" o "docker COMMAND --help"
- Documentació de Docker - https://docs.docker.com/
- Tutorial de Docker - https://www.tutorialspoint.com/docker/index.htm
- Començar amb docker compose - https://docs.docker.com/compose/gettingstarted/
- Referència de Docker compose - https://docs.docker.com/compose/compose-file/
- Imatges de Wordpress - https://hub.docker.com/_/wordpress
- Imatges de MySQL - https://hub.docker.com/_/mariadb

---

## Objectius

L'objectiu d'aquesta pràctica és començar a treballar amb múltiples contenidors interconnectats amb serveis estàndard.

---

## Introducció

Podem iniciar múltiples contenidors amb imatges ja preparades al DockerHub. Per exemple

~~~
sudo docker search apache
sudo docker search wordpress
sudo docker search mysql
~~~

Podem muntar un contenidor amb un servidor Web i una aplicació Web (WordPress) i un altre contenidor amb un servidor de BD (MariaDB).

Abans de fer el projecte demanat, feu aquest de mostra.

### Multicontainer

![](imgs/01.docker-volums-xarxes-d629cfb7.png)

- c_control: Client
- c_web: Wordpress
- c_bd: MariaDB

Aquí el servidor web serà el contenidor amb Wordpress. El servidor amb la base de dades serà el contenidor amb MariaDB. L'únic contenidor que exporta els ports és el Wordpress, ja que al servidor de BD només hi ha d'accedir el propi servidor Web però no s'hi ha de poder accedir des de l'exterior.

Seguint les instruccions de la pàgina d'ajuda de la imatge de Wordpress (https://hub.docker.com/_/wordpress) i de MariaDB (https://hub.docker.com/_/mariadb), caldrà iniciar primer el contenidor de la BD i després el del WP (en aquest ordre ja que l'aplicació Wordpress necessita que la BD estigui engegada abans):

~~~
$ docker network create xarxa_dades

$ docker run --name mi-sql --network xarxa_dades -d -e MYSQL_DATABASE=wp-db -e MYSQL_USER=wp-user -e MYSQL_PASSWORD=pass -e MYSQL_RANDOM_ROOT_PASSWORD='1' -v wp-db:/var/lib/mysql mariadb:latest

$ docker run --name mi-wordpress --network xarxa_dades -d -e WORDPRESS_DB_HOST=mi-sql -v web:/var/www/html -e WORDPRESS_DB_NAME=wp-db -e WORDPRESS_DB_USER=wp-user -e WORDPRESS_DB_PASSWORD=pass -p 8091:80 wordpress:latest
~~~

Feu molta atenció als paràmetres de cada crida a "docker run" i assegureu-vos que enteneu què volen dir.

- --name: Posar nom al contenidor
- --network: Connectar contenidor a aquesta xarxa
- -d: Dimoni, el contenidor queda en segon pla
- -e: Passar valor de variable d'entorn al contenidor. Aquest ja la te definida amb un valor per defecte.
- -v: Volum on guardem dades per persistència
- -p: port a exportar, o sigui, es veu des d'altres equips de la xarxa, com ara els clients.

Observa que hem posat **volums per als directoris dels contenidors servidors on es guarden dades que es modifiquen** quan funcionen els serveis
- La **Base de dades de WordPress**, que MariaDB guarda al directori del contenidor "mi-sql" /var/lib/mysql es guardarà al volum "wp-db".
- El **directori amb contingut web estàtic i dinàmic**, potser **directoris de "upload"** on els usuaris pugen dades, etc. Es troba al directori /var/www/html del contenidor "mi-wordpress". És el directori per defecte del servidor Apache i es guardarà al volum "web".
- Podríem també guardar en un altre volum els **directoris i fitxers de configuració del contenidor que necessitem que persisteixin després de la mort dels contenidors**.

---

### Depurar erors

En cas d'un error com ara:
~~~
docker: Error response from daemon: driver failed programming external connectivity on endpoint mi-wordpress (819ca4e5808515d35d4af561333db76c0d3332296af9cd48de840ecc0779b050): Error starting userland proxy: listen tcp4 0.0.0.0:8091: bind: address already in use.
~~~

Comproveu que el port no s'està ja utilitzant per un altre container o procés del sistema. Mirem quins processos han obert aquest port i els aturem (l'opció més expeditiva és "kill -9 PID").
~~~
sudo ss -atunp | grep 8091
tcp   LISTEN     0      4096                    0.0.0.0:8091         0.0.0.0:*     users:(("docker-proxy",pid=34443,fd=4))  
tcp   LISTEN     0      4096                       [::]:8091            [::]:*     users:(("docker-proxy",pid=34450,fd=4))  

~~~

Mirem la columna Local "Address:port" per veure quins processos estan escoltant. A la dreta veurem el PID i nom del procés que ha obert el port.

---

Per accedir al Wordpress creat, com ja sabeu apuntem al host local (físic) posant al navegador http://localhost:8091. Ens apareixerà la pàgina inicial del Wordpress per a començar a configurar.

![](imgs/docker-projecte-multicontainer-2023-c4a5111c.png)

**ATENCIÓ**: Quan a la configuració indiqueu el host on es troba la BD, no poseu localhost ja que es troba a un altre contenidor. Tampoc mireu la IP del contenidor MariaDB ja que aquesta pot canviar en cada creació del contenidor. Sempre s'ha de fer servir el nom del contenidor de la BD.

Per depurar errors per si alguna cosa falla, podem veure els logs dels contenidors, fent:
~~~
sudo docker container logs -f mi-sql
sudo docker container logs -f mi-wordpress
~~~

Podem accedir a la BD des dels propis contenidors (des del contenidor del WP o localment al contenidor de la BD) executant un client de mysql amb la comanda exec de Docker:

~~~
// des del contenidor que te Wordpress (cal accés remot i per això especifiquem
// el host a la comanda mycli amb -h)
sudo docker exec -ti mi-wordpress bash
# apt update
# apt install mycli
# mycli -hmi-sql --user=exampleuser --password exampledb

// o bé des del contenidor que té la base de dades
sudo docker exec -ti mi-sql mysql --user=exampleuser --password exampledb
~~~

---

### Orquestració

Podem crear un **fitxer docker-compose** per a engegar els serveis automàticament i en seqüència ordenada. D'això se'n diu "orquestrar". Cal instal·lar primer el docker-compose https://docs.docker.com/compose/install/

El fitxer docker compose el podeu crear a partir de:
- O bé de https://hub.docker.com/_/wordpress/ buscant example stack.yml)
- O bé a partir de l'exemple https://docs.docker.com/samples/wordpress/
- O bé a la web composerize.com a partir dels docker run

Es guardarà en un fitxer compose.yaml o bé docker-compose.yml (en format yaml). Observeu que cada paràmetre del docker run té les seves directives equivalents:

~~~
version: '3.3'
services:
    mariadb:
        image: 'mariadb:latest'
        container_name: mi-sql
        restart: always
        environment:
            - MYSQL_DATABASE=wp-db
            - MYSQL_USER=wp-user
            - MYSQL_PASSWORD=pass
            - MYSQL_RANDOM_ROOT_PASSWORD=1
        volumes:
            - 'wp-db:/var/lib/mysql'

    wordpress:
        depends_on:
            - mariadb
        image: 'wordpress:latest'
        container_name: mi-wordpress
        restart: always
        environment:
            - WORDPRESS_DB_HOST=mi-sql
            - WORDPRESS_DB_NAME=wp-db
            - WORDPRESS_DB_USER=wp-user
            - WORDPRESS_DB_PASSWORD=pass
        volumes:
            - 'web:/var/www/html'
        ports:
            - '8091:80'

volumes:
   wp-db:
   web:
~~~

Al directori on s'ha creat el fitxer docker-compose.yml executem "docker compose up". Quan volem aturar-ho piquem CTRL+C.

Un cop arrencada la aplicació multicontainer amb docker compose, ja podrem accedir al WordPress a hhtp://localhost:8091

---

## Repte: Projecte multicontenidor Mediawiki.

**ATENCIÓ: Vigileu per que no és el mateix arrencar el contenidor per a fer la instal·lació que arrencar el contenidor quan ja hi ha dades prèvies que s'han d'utilitzar. Assegureu-vos bé que quan torneu a engegar contenidors, s'enllacen a les dades que hi havia abans. Si no, cada vegada que inicieu contenidors, s'iniciaria la instal·lació de mediawiki.**

---

### Mediawiki

**NOTA:** Recordeu que heu de posar el vostre nom a tots els objectes que creeu i configureu a les ordres docker run i el docker-compose. També cal que es vegi el vostre nom en el Mediawiki i Joomla que creeu, així com plantilles i fotografies per personalitzar la web.

---

**ATENCIÓ:** Cal respectar la seguretat i els usuaris externs no han de poder veure el contenidor amb la BD. Crea les xarxes de manera que s'acompleixi, com vas fer a l'exercici de xarxes docker.

---

1. Arrenca dos contenidors per poder tenir un lloc mediawiki (busca a DockerHub), de manera que:
- La BD es guardi en un volum. També cal guardar el fitxer de configuració en un volum.
- També cal guardar en un volum els directoris on mediawiki guarda les dades que els usuaris pugen al servidor.
- Mira com i on guarda mediawiki els plugins per ficar-los en el volum.
- Configura amb el teu nom i pàgina d'inici el **Mediawiki** (Que es vegi clarament que és el teu lloc particular, posa alguna imatge pròpia).

2. Anem a comprovar que la persistència és correcta:
- Mata (kill) i destrueix (rm) els dos contenidors amb els que heu configurat el mediawiki.
- Mostra que no estan.
- Crea dos contenidors nous de manera que s'aprofitin les dades que hi ha a la BD de dades creada anteriorment.
- Ha de veure's el que ja havíeu personalitzat.

3. Crea un fitxer docker-compose per a iniciar i aturar els serveis d'una sola tacada.
- Agafa la informació de les pàgines d'ajuda dels contenidors de MariaDB i Mediawiki.

---

## OPCIONAL

Munta els següents entorns de Hacking Web usant Docker.
- Crea volums per no perdre els canvis o progressos que es guarden a les bases de dades i configuracions.
- Per trobar la informació, pots anar a la pàgina del projecte o bé a Dockerhub, per mirar de trobar els fitxers Dockerfile i fitxers de codi font.
- Alguns projectes tenen un docker-compose per arrencar els serveis. Busca'l.

1. Damn Vulnerable Web Application (DVWA) en el port 8080, port 80 del contenidor.
- https://www.dragonjar.org/dvwa-damn-vulnerable-web-app.xhtml
- https://github.com/digininja/DVWA
- https://hub.docker.com/r/vulnerables/web-dvwa

2. Juice Shop en el port 3000, port 3000 del contenidor.
- https://owasp.org/www-project-juice-shop/
- https://hub.docker.com/r/bkimminich/juice-shop  https://github.com/juice-shop/juice-shop

3. WebGoat en el puerto 8081, port 8080 del contenidor.
- https://owasp.org/www-project-webgoat/
- https://github.com/WebGoat/WebGoat
- https://hub.docker.com/r/webgoat/webgoat

4. Mutillidae en el port 81:
- https://owasp.org/www-project-mutillidae-ii/
- https://github.com/webpwnized/mutillidae-docker
- https://github.com/webpwnized/mutillidae
- https://github.com/webpwnized/mutillidae-dockerhub
