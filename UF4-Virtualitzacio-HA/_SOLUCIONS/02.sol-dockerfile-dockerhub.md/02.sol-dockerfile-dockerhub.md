# Solució Dockerfile i DockerHub

Si mplayer dóna l'error "Could not open/inicialize audio device -> no sound" perquè no troba el driver, es pot arreglar modificant el fitxer de configuració del mplayer, que es troba al directori /etc/mplayer. En aquest fitxer cal afegir sota de "other settings" el paràmetre lirc=no. I en l'apartat "audio settings" s'ha de canviar el paràmetre ao=pulse,alsa,sld:aalib per ao=pulseaudio,alsa,sld:aalib.

#### 1. Crea la imatge de l'exercici del reproductor afegint a un Dockerfile totes les passes fetes "manualment".

Per editar Dockerfile des d'Atom, podem afegir un nou paquet anomenat "language-docker" que mostra en colors la sintaxi dels Dockerfile (si el fitxer te aquest nom).

Primer crearem el Dockerfile sense optimitzar (així cada vegada que hi ha canvis, no es torna a fer tota la instal·lació. Com que fem moltes proves, això fa que no perdem tant de temps construint la imatge).

~~~
FROM ubuntu:20.04

# No es pot interactuar amb el contenidor. Si no es posa, aborta l'actualització
ARG DEBIAN_FRONTEND=noninteractive

#  preparar el contenidor instal·lant les eines i dependències necessàries. Això es quedarà al contenidor i és la part estàtica que quedarà a la imatge

RUN apt-get update
RUN apt-get -y install python3
RUN apt-get -y install ffmpeg
RUN apt-get -y install pip
RUN apt-get -y install mplayer
RUN python3 -m pip install -U yt-dlp
RUN mkdir /mp3

# Crear script per a poder executar 2 comandes
RUN echo "yt-dlp -x --audio-format mp3 --audio-quality 0 https://www.youtube.com/watch?v=h2a20YL5mvg -o /mp3/musica.mp3 \n mplayer /mp3/musica.mp3" >musica.sh

RUN chmod a+rx musica.sh

# comandes per a fer la feina demanada. Això es farà quan executi el contenidor. Cridem l'script amb les comandes anteriors, ja que hi ha més d'una.

CMD bash /musica.sh
~~~

Ara optimitzem i posem variables d'entorn per poder canviar la cançó en temps d'execució:

~~~
FROM ubuntu:20.04

# No es pot interaccionar amb el contenidor. Si no es posa, avorta l'actualització
ARG DEBIAN_FRONTEND=noninteractive

# Variables d'entorn per escollir URL i nom del fitxer de sortida (Valors per defecte)
ENV RUTA_MUSICA=https://www.youtube.com/watch?v=h2a20YL5mvg
ENV CANCION=Walk_of_Life-DireStraits

# Instal·lar paquets
# Per concatenar dues ordres, fem servir &&
# Per que es continui la comanda en la següent línia fem servir \

RUN apt-get update && \
    apt-get install -y python3 \
    ffmpeg \
    pip \
    mplayer

# Instal·lar youtube-dl
RUN python3 -m pip install -U yt-dlp

# Crear script per a poder executar 2 comandes
RUN echo "Reproduint música..." >musica.sh
RUN echo "yt-dlp -x --audio-format mp3 --audio-quality 0 $RUTA_MUSICA -o /mp3/$CANCION.mp3 \n mplayer /mp3/$CANCION.mp3" >>musica.sh

# Executar script
CMD bash /musica.sh
~~~

Per  construir la imatge, anar al directori on es troba el Dockerfile (on està el projecte):

~~~
sudo docker build -t reproductor .

# Podriem posar al tag el repositori i la versió.
sudo docker build -t pautome:reproductor:01 .

# si hem fet el build sense posar tag, mirem id de la imatge recent creada amb docker image ls
sudo docker image tag f829beb4a6de player24
~~~

Caldrà executar un container a partir d'aquesta imatge per assegurar-nos que la imatge és viable.

~~~
sudo docker run -it reproductor
~~~

#### 2. Crea un contenidor a partir d'aquesta imatge.

Per executar un contenidor a partir de la imatge (farà servir les variables d'entorn definides amb les directives ENV) https://vsupalov.com/docker-arg-env-variable-guide/ que s'inicialitzen amb un valor en la comanda docker run amb el paràmetre -e:

~~~
sudo docker run --device /dev/snd:/dev/snd -it --name implayer reproductor

# Si volem passar un volum una carpeta del host i la url de Youtube i el nom del fitxer (sobreescriure la definició feta al Dockerfile)
sudo docker run -v /tmp:/mp3 -e "RUTA_MUSICA=https://www.youtube.com/watch?v=btPJPFnesV4" -e "CANCION=eye_of_tiger-Survivor" --device /dev/snd:/dev/snd -it --name implayer repro env

# O creant un volum abans
sudo docker volume create musica
sudo docker run -v musica:/mp3 -e "RUTA_MUSICA=https://www.youtube.com/watch?v=btPJPFnesV4" -e "CANCION=eye_of_tiger-Survivor" --device /dev/snd:/dev/snd -it --name implayer repro env
~~~

#### 3. Puja la imatge al DockerHub.

~~~
# Primer li posem nom a la imatge si no ho vam fer en crear-la (davant el repositori, pautome, i darrera la versió, 01)
sudo docker image tag reproductor pautome:reproductor:01
docker login
docker push pautome:reproductor:01
~~~
