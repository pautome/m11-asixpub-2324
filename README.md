# Mòdul 11 Seguretat i alta disponibilitat 23-24

- Aquí trobareu els apunts, diapositives i enunciats dels exercicis del mòdul.
- Si teniu algun dubte, pregunteu al professor a classe o mitjançant el correu electrònic.  

**ATENCIÓ: La informació proporcionada té un caràcter educatiu i en cap cas se'n pot fer un ús delictiu o maliciós. Cal fer un ús responsable i ètic dels coneixements adquirits. MAI ho heu de fer amb un equip de producció (màquina física o de la feina). En cas contrari, es pot incòrrer en delictes tipificats al codi Penal amb penes que poden suposar multes i fins i tot presó.**

## Imatges Windows per les pràctiques

- Get a Windows 11 development environment

Start building Windows applications quickly by using a virtual machine with the latest versions of Windows, the developer tools, SDKs, and samples ready to go. - https://developer.microsoft.com/en-us/windows/downloads/virtual-machines/

- En Azure, disc ISO, Disc dur virtual VHD de Windows Server 2012
  - (descàrrega directa) https://go.microsoft.com/fwlink/p/?linkid=2195172&clcid=0x409&culture=en-us&country=us
  - https://www.microsoft.com/es-es/evalcenter/evaluate-windows-server-2012-r2

- Versions d'avaluació de Microsoft - https://www.microsoft.com/es-es/evalcenter (obrir menu de dalt)

- Versions anteriors de Microsoft (ja no disponibles a Microsoft) - https://files.rg-adguard.net/

## Referències

- Config serveis i servidors - https://www.server-world.info/en/
