# SERVIDOR D' ACCES REMOT I VPN'S

**INS Carles Vallbona**

**Pau Tomé** (Act nov 2023)

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [SERVIDOR D' ACCES REMOT I VPN'S](#servidor-d-acces-remot-i-vpns)
	- [DEFINICIÓ SERVIDOR D’ACCES REMOT (NETWORK ACCESS SERVER)](#definici-servidor-dacces-remot-network-access-server)
		- [FUNCIONAMENT GENÈRIC DEL CONJUNT NAS I RADIUS (AAA)](#funcionament-genric-del-conjunt-nas-i-radius-aaa)
	- [RADIUS (REMOTE AUTHENTICATION DIAL-IN USER SERVER)](#radius-remote-authentication-dial-in-user-server)
		- [EXEMPLES D'APLICACIONS DE RADIUS](#exemples-daplicacions-de-radius)
			- [XARXES DE GRANS DIMENSIONS](#xarxes-de-grans-dimensions)
			- [ACCÉS A XARXES SENSE FILS](#accs-a-xarxes-sense-fils)
		- [AVANTAGES DE RADIUS](#avantages-de-radius)
	- [AAA](#aaa)
		- [LA PRIMERA “A” AUTENTICACIÓ](#la-primera-a-autenticaci)
			- [SISTEMES D’AUTENTICACIÓ SUPORTATS PER RADIUS](#sistemes-dautenticaci-suportats-per-radius)
		- [LA SEGONA “A” AUTORITZACIÓ](#la-segona-a-autoritzaci)
			- [AUTENTICACIÓ VS AUTORITZACIÓ](#autenticaci-vs-autoritzaci)
		- [LA 3ERA “A”: ACCOUNTING (COMPTABILITAT)](#la-3era-a-accounting-comptabilitat)
	- [VPN](#vpn)
		- [Avantatges de les VPN](#avantatges-de-les-vpn)
		- [Inconvenients de les VPN](#inconvenients-de-les-vpn)
	- [Aplicacions REALS de les VPN](#aplicacions-reals-de-les-vpn)
	- [TIPUS DE CONNEXIONS VPN](#tipus-de-connexions-vpn)
		- [MAQUINARI-PROGRAMARI](#maquinari-programari)
		- [TIPUS DE CONNEXIÓ VPN](#tipus-de-connexi-vpn)
			- [1. CONNEXIO D’ACCES REMOT](#1-connexio-dacces-remot)
			- [2. CONNEXIO PUNT A PUNT (SITE2SITE)](#2-connexio-punt-a-punt-site2site)
			- [3. TUNNELING](#3-tunneling)
	- [4. VPN OVER LAN](#4-vpn-over-lan)
	- [PROTOCOLS VPN: SEGONS ELS NIVELLS TCP/IP O OSI](#protocols-vpn-segons-els-nivells-tcpip-o-osi)
		- [1. PROTOCOLS VPN DE CAPA D'ENLLAÇ](#1-protocols-vpn-de-capa-denlla)
			- [EXEMPLE](#exemple)
		- [2. PROTOCOLS VPN DE CAPA DE XARXA](#2-protocols-vpn-de-capa-de-xarxa)
			- [EXEMPLE](#exemple)
		- [3. PROTOCOLS VPN DE CAPA D'APLICACIO.](#3-protocols-vpn-de-capa-daplicacio)
			- [EXEMPLES](#exemples)
			- [SSL/TLS (Secure Socket Layer/Transport Layer Security)](#ssltls-secure-socket-layertransport-layer-security)
				- [SSL/TLS: FASES DE LA COMUNICACIO](#ssltls-fases-de-la-comunicacio)
		- [SSH (Secure Shell)](#ssh-secure-shell)
		- [Diferents túnels SSH](#diferents-tnels-ssh)
	- [PROTOCOLS VPN PROPIETARIS](#protocols-vpn-propietaris)
- [Referències:](#referncies)
	- [Ports necessaris en una VPN i descripció VPN](#ports-necessaris-en-una-vpn-i-descripci-vpn)
	- [Internet_Protocol_Security](#internetprotocolsecurity)
	- [VPN amb autenticació RADIUS](#vpn-amb-autenticaci-radius)
	- [Serveis VPN gratuïts "fiables"](#serveis-vpn-gratuts-fiables)
	- [Túnel SSH](#tnel-ssh)

<!-- /TOC -->
---

## DEFINICIÓ SERVIDOR D’ACCES REMOT (NETWORK ACCESS SERVER)

**Servidors d'Accès Remot (NAS=Network Access Server)**:  són una combinació de maquinari i programari que permet l’accés remot a eines o informació (fitxers, serveis, impressores) que resideix en una xarxa local. Per exemple, als treballadors teletreballant.

- **ATENCIÓ: No confondre amb Network Attached Storage (NAS)**, que és un disc de xarxa.

També són anomenats per Microsoft com **Terminal Server** o **Remote Access Server/Service (RAS)**.
- A Windows server 2012 el servei es diu RRAS (Routing and Remote Access).

Per fer servir l’accés remot, es requereix **un client en l’equip remot** que:
- negocia l’enllaç amb el servidor
- s’autentica
- s’assigna adreça IP al client
- s’accedeix als serveis remots.

Passes per accedir als serveis remots:

1. Quan un client vol fer us d’un servei remot protegit (serveis de carpetes, impressores, intranet, accés a Internet, etc.) es connecta al NAS, fent servir algun tipus de protocol de connexió remota (de VPN o PPP per exemple).

2. El NAS es comunica amb un servidor de **AAA (típicament RADIUS o TACACS o ACACS)** i li pregunta si les credencials donades pel client són vàlides. Segons la resposta, el NAS li permetrà accedir o no al recurs protegit.

<p align="center">
<img src="imgs/1.servidors-acces-remot-c215dee8.png" width="500" />
</p>


HWTACACS i TACACS - https://support.huawei.com/enterprise/en/doc/EDOC1100092137

---

Actualment les **VPN** es fan servir molt com a sistema d'accés remot utilitzant el servidor VPN com a NAS (Network Access Service).


<p align="center">
<img src="imgs/servidors-acces-remot-941a8104.png" width="500" />
</p>
<p align="center">
https://howdoesinternetwork.com/2013/radius-infrastructure
</p>

---

### FUNCIONAMENT GENÈRIC DEL CONJUNT NAS I RADIUS (AAA)

Exemple de la connexió amb un ISP mitjançant mòdem, DSL, cablemòdem, Ethernet o Wi-Fi:
- S'envia una informació que generalment és un **nom d'usuari i una contrasenya**.
- Aquesta informació és transferida a un **dispositiu NAS** (Servidor d'Accés a la Xarxa o Network Access Server (NAS)) sobre el protocol PPP.
- El **NAS** reenvia la petició a un servidor RADIUS sobre el protocol RADIUS.
- El **servidor RADIUS (AAA)** comprova que la informació és correcta fent servir esquemes d'autenticació com PAP, CHAP o EAP.
- Si és acceptat, **el servidor autoritzarà l'accés** al sistema de l'ISP i li assigna els recursos de xarxa com una adreça IP, i altres paràmetres como L2TP, etc.

---

## RADIUS (REMOTE AUTHENTICATION DIAL-IN USER SERVER)

RADIUS és un protocol que permet gestionar l’**autenticació, autorització i registre** (AAA) d’usuaris remots sobre un determinat recurs.

- **AAA= Authentication, Authorization, and Accounting** (autenticació, autorització i registre o comptabilitat)

- Utilitza el **port estándar 1813 UDP** per establir les seves connexions.

- RADIUS és el **protocol per a AAA més comú** actualment.

- **RADIUS és extensible** i per tant  la majoria de fabricants de software i hardware RADIUS implementen els seus propis dialectes Estàndards.

**DIAMETER**: És un protocol AAA que estén i millora RADIUS. També proporciona gestió d’errors i comunicació entre dominis. https://en.wikipedia.org/wiki/Diameter_(protocol)
- El seu nom és un joc de paraules (diàmetre és el doble del radi).

---

### EXEMPLES D'APLICACIONS DE RADIUS

#### XARXES DE GRANS DIMENSIONS

Quan tenim xarxes de grans dimensions sobre les que volem oferir un servei d’accés centralitzat (el seu funcionament es pot jerarquitzar amb diversos servidors RADIUS).

Un dels principals usos de RADIUS és en empreses que donen **accés a Internet** o **grans xarxes corporatives**, amb diverses tecnologies de xarxa (mòdems, xDSL, VPNs i xarxes sense fils) no només per gestionar l’accés a la pròpia xarxa, sinó també per a serveis propis d’Internet (com e-mail, Web o inclús el procés de senyalització SIP en VoIP).

#### ACCÉS A XARXES SENSE FILS

Es pot fer servir **RADIUS per l’autenticació en xarxes sense fils** (Wi-Fi), substituint mètodes més simples de clau compartida (pre-shared key, PSK), molt limitats en gestionar una xarxa quan es fa massa gran.

- En aquest cas, el **NAS serà el punt d’accés Wi-Fi** que serà client de RADIUS i haurà de suportar la comunicació amb el servidor RADIUS per poder autenticar, autoritzar i registrar (si cal).

- El client Wi-Fi, a cada dispositiu que es vol connectar al punt d'accés, se l'anomena **“Suplicant”** (NO client).

<p align="center">
<img src="imgs/vpn-45f90587.png" width="500" />
</p>
<p align="center">
Esquema Eduroam (https://www.eduroam.hr/images/eduroam_auth.png)
</p>

---

### AVANTAGES DE RADIUS

- Els **clients de RADIUS** (servidors NAS normalment) **només han d’implementar el protocol de comunicació amb RADIUS**. Així, no cal que implementin totes les possibilitats de AAA existents (PAP, CHAP, LDAP, kerberos, mySQL, etc.).
- Per un punt d’accés, **només cal una solució NAS** (servidor que enten el protocol RADIUS) que faci les consultes a RADIUS.
- En la comunicació entre RADIUS i NAS mai s’envien les contrasenyes en clar per la xarxa ni tan sols en usar PAP, sinó que **usa algorismes per ocultar les contrasenyes com MD5** (tot i que MD5 ja no és massa segur).

Es **recomana usar** altres sistemes addicionals per **xifrar el tràfic de RADIUS**, com poden ser túnels de IPsec (VPN entre NAS i RADIUS).

---
## AAA

**AAA= Authentication, Authorization, and Accounting** (autenticació, autorització i registre o comptabilitat)

### LA PRIMERA “A” AUTENTICACIÓ

**Autenticació:** Fa referència al procés pel que es determina si un usuari te permís per accedir a un determinat servei de xarxa del que vol fer ús. Es fa presentant una identitat i unes credencials (contrasenya secreta o certificat) per part de l’usuari que demana accés.

#### SISTEMES D’AUTENTICACIÓ SUPORTATS PER RADIUS

- **Sistema Unix** (/etc/passwd)

- **Protocols PAP, CHAP, EAP** usats per ISP’s i accessibles via PPP.

- **LDAP (Lightweight Directory Access Protocol)** protocol de servei de directori ordenat, molt usat com a base de dades de noms d’usuaris i contrasenyes.

- **Kerberos:** mètode d’autenticació dissenyat pel MIT.

- **EAP(Extensible Authentication Protocol)**, entorn universal d’autenticació usat en xarxes wi-fi i connexions punt a punt.

- **Fitxers locals** de configuració del servidor RADIUS.

---

### LA SEGONA “A” AUTORITZACIÓ

**Autorització:** Concedir serveis específics (també “negació de servei”) a un determinat usuari, en base a l’autenticació, els serveis que està sol·licitant, i l’estat actual del sistema.

Es pot configurar restriccions a l’autorització de serveis en funció de, per exemple:
- l’hora del dia
- la localització de l’usuari
- la impossibilitat de fer múltiples “logins” d’un mateix usuari.

L’autorització determina la natura del servei que es concedeix a l’usuari, com:
- Adreça IP assignada
- El tipus de qualitat de servei (QoS) que rebrà
- L’ús d’encriptació
- L’ús obligatori de túnels per a determinades connexions...

Els mètodes per gestionar l'autorització suportats per un servidor de RADIUS inclouen:
- bases de dades LDAP
- bases de dades SQL (com Oracle, MySQL i PostgreSQL)
- Fitxers de configuració locals al servidor.

---

#### AUTENTICACIÓ VS AUTORITZACIÓ

No s'ha de confondre el termes autenticació amb autorització.
- **L’autenticació** és el procés de verificar la identitat d’un individu (persona o inclús ordenador)
- **L’autorització** és el procés de verificar que un individu ja autenticat te l’autoritat per efectuar una determinada operació (ACL's).

---

### LA 3ERA “A”: ACCOUNTING (COMPTABILITAT)

Es refereix a realitzar un registre del consum de recursos que fan els usuaris.
- El registre sol incloure aspectes com la **identitat de l’usuari**, la **natura del servei prestat**, i quan va **començar i acabar l’ús del servei**.

---

## VPN

**VPN (Virtual Private Network, Xarxa privada virtual)**: És una xarxa privada que s'estén a diferents punts remots mitjançant infraestructures públiques de transport.
- Normalment es fa servir Internet per la connexió remota, però no és l'únic mitjà. També es pot fer amb xarxes dedicades de proveïdors de telecomunicacions (Movistar per ex.) però cal pagar un lloguer de les línies.
- Es fa un procés d’encapsulació i xifrat dels paquets de dades.

<p align="center">
<img src="imgs/vpn-04eb1aa9.png" width="500" />
</p>
<p align="center">
Tipus de VPN
</p>

---

### Avantatges de les VPN

- **Seguretat**: proveeix d’encriptació i encapsulació de dades a través d’un túnel. Les dades viatgen encriptades.

- **Cost baix**: Estalvien molts diners en línies dedicades o enllaços físics.

- **Millor administració**: cada usuari que es connecta pot tenir una adreça IP fixa assignada per l’administrador. Això facilita per exemple imprimir remotament. També  es pot assignar IP dinàmiques.

- **Fàcil per usuaris amb poca experiència** per connectar-se a grans xarxes corporatives de forma segura.

- **Permet simular un canvi en la ubicació geogràfica del host**: La IP que veurà el servei de destí serà la del servidor VPN. Utilitzat per accedir a serveis que no es poden accedir des d'altres països, com ara Netflix de Turquia des d'Espanya per a contractar-ho més barat.

- **Saltar la censura o restriccions**: El tràfic cap a un servidor VPN va xifrat i per tant no es pot monitorar.
  - Serviria per saltar restriccions d'accés de la xarxa d'una empresa cap a alguns llocs web prohibits.
  - O per exemple, Xina té prohibides les VPN per que es pot saltar les restriccions de la Gran Muralla, un firewall de nivell nacional que monitoritza tot el que fan els ciutadans https://en.wikipedia.org/wiki/Great_Firewall.


### Inconvenients de les VPN


- **Cal un alt nivell de coneixements per configurar**: La implementació requereix un alt nivell de coneixements i la comprensió de factors com la seguretat de la xarxa pública.
- **Cal una planificació acurada**: Per evitar problemes de seguretat.

- **Disponibilitat i rendiment difícils de controlar**: En ser tràfic xifrat, implica latència en el procés de xifrat i desxifrat. La disponibilitat i rendiment depenen molt del nombre d'usuaris connectats al servidor VPN.

- **Velocitat de transmissió de dades baixa**: molt més lentes que una connexió tradicional, ja que la VPN de seguretat requereix autenticació i xifrat de dades. Les adreces de xarxa també poden ser xifrades per més seguretat.

- La **navegació no és anònima**. Sempre et monitora el proveïdor de la VPN (si es fa amb un proveïdor no controlat per nosaltres i hem de confiar en ell). El tràfic és segur des del client al servidor VPN, però aquest últim pot desxifrar-ho.

---

## Aplicacions REALS de les VPN

Algunes aplicacions reals de les VPN:
- **Teletreball**: Permet treballar a la xarxa local de l'empresa des de qualsevol lloc usant les adreces privades com si estiguéssim dintre la xarxa local.
- **Unir seus geogràficament llunyanes**: Amb un enllaç barat ja que fa servir una xarxa pública i de forma segura per que hi ha xifrat.
- **Evitar censura**: Es pot fer una connexió a un servidor VPN que es troba en un altre país i permet accedir a llocs web prohibits.
- **Evitar blocat geogràfic**: El servidor VPN es pot trobar a un altre país i és aquest servidor des d'on s'inicien connexions a serveis no accessibles des del país origen (ex. Netflix, webs prohibides des de Xina)
- **Afegeix una capa de seguretat extra**, per exemple en xarxes Wifi públiques.
- Permet organitzar **partides de joc en xarxa local** entre jugadors separats geogràficament (ex. Hamachi)

---

## TIPUS DE CONNEXIONS VPN

Es poden classificar atenent a diferents criteris:
- Maquinari-programari
- Tipus de connexió

### MAQUINARI-PROGRAMARI

Es pot implementar amb solucions basades en Maquinari o Programari. El més important és el protocol que es faci servir.

**Maquinari**: bàsicament equips dedicats com ara routers. Són segurs i fàcils d’usar.
- Avantatges: gran rendiment ja que són dispositius dedicats
- Inconvenients: cost elevat i moltes vegades, protocols propietaris.

**Programari**: En un sistema operatiu es configura el servei VPN. També hi ha solucions dedicades a serveis de xarxa com PFSense que ho implementen.
- Avantatges: Menys cost, aprofitament d’equips, ús de protocols estàndard i propietaris.
- Inconvenients: Més baix rendiment ja que s’utilitzen molts recursos del processador per altres serveis.

---

### TIPUS DE CONNEXIÓ VPN

#### 1. CONNEXIO D’ACCES REMOT

<p align="center">
<img src="imgs/vpn-970a9c12.png" width="500" />
</p>
<p align="center">
Connexió d'accés remot
</p>

Es realitza per un client o un usuari d’un host que es connecta des de llocs remots utilitzant normalment Internet.
- Els paquets són originats al client de accés remot, i aquest s’autentifica al servidor d’accés remot (també router, firewall), que també s’autentifica amb el client.
- Una vegada autenticats, els serveis són similars als que tindria a la LAN.

---

#### 2. CONNEXIO PUNT A PUNT (SITE2SITE)

<p align="center">
<img src="imgs/vpn-6ad83a03.png" width="500" />
</p>
<p align="center">
Connexió site2site
</p>

S’utilitza per unir seus remotes amb la seu principal d’una organització.

- Els extrems de la VPN són dos routers o firewalls. El router que fa la connexió remota s’autentifica amb el router de la seu principal que també s’autentica (autenticació mútua).

- Els routers no generen els paquets, si no que reenvien els paquets generats d’una seu a l’altra.

- Aquest model evita haver de fer servir connexions privades entre seus, molt més cares.

---

#### 3. TUNNELING

- S’encapsula un protocol de xarxa sobre un altre (protocol de xarxa encapsulador) creant un túnel dintre una xarxa. Així, s’encaminen els paquets de dades sobre nodes intermitjos que no poden veure en clar el contingut dels  paquets.
- El túnel queda definit pels punts extrems i el protocol de comunicació usat, entre altres, SSH.
- Es fa servir entre d’altres coses, per convertir protocols insegurs en segurs.

<p align="center">
<img src="imgs/vpn-0defb14b.png" width="500" />
</p>

<p align="center">
<img src="imgs/vpn-16937ecc.png" width="500" />
</p>
<p align="center">
Exemples: Túnel SSH
</p>

Més info: Túnel SSH (interface tun): https://help.ubuntu.com/community/SSH_VPN

---

## 4. VPN OVER LAN

És una variant del tipus "accés remot" que en lloc de fer servir Internet, usa la LAN de l’empresa.

Serveix **per aïllar zones i serveis de la xarxa interna**. Per exemple per millorar prestacions de seguretat de les xarxes sense fils (Wi-Fi).

- **Exemple1**: un servidor sensible, com les nòmines, darrera un servidor VPN, que dona autenticació addicional i xifrat. Només el personal de RRHH pot accedir-hi.

- **Exemple2**: conexió a xarxes Wi-Fi amb túnels xifrats IPSec o SSL (a més dels sistemes Wi-Fi WEP, WPA, etc.) que afegeixen credencials de seguretat del túnel VPN.

---

## PROTOCOLS VPN: SEGONS ELS NIVELLS TCP/IP O OSI

Existeixen protocols VPN en els diferents nivells dintre l’arquitectura TCP/IP o OSI:
- **A nivell de capa d’enllaç (nivell 2)**
- **A nivell de capa de xarxa (nivell 3)**:
- **A nivell de capa d’aplicació (nivell 7)**

---

### 1. PROTOCOLS VPN DE CAPA D'ENLLAÇ

**A nivell de capa d’enllaç (nivell 2)**: Encapsulen tot el que hi ha per sobre de la capa 2. Normalment per enllaços punt a punt (cable sèrie o connexió amb "trucada").

#### EXEMPLE

- **PPTP (Protocol de túnel punt a punt)** desenvolupat per Microsoft, 3Com, Ascend, US Robotics y ECI Telemàtics. Actualment és un protocol insegur i es recomana NO fer-ho servir.
- **L2TP (Protocol de túnel de capa 2)**, treball de l’IETF (RFC 2661). És un protocol de capa 2 basat en PPP. Aprofita els protocols IPSec per a protegir els paquets.

---

### 2. PROTOCOLS VPN DE CAPA DE XARXA

- **A nivell de capa de xarxa (nivell 3)**: Encapsulen tot el que hi ha per sobre de la capa 3 de manera que no caldria fer xifrat a nivell 2. Les aplicacions no han de fer res especial per tenir seguretat, fins i tot poden usar protocols insegurs, ja que el nivell 3 ho xifrarà.

#### EXEMPLE

**IPsec (Internet Protocol Security**
Desenvolupat per Internet Engineering Task Force (IETF) per IPv6.
- Molt usat amb IPv4 i L2TP (Layer 2 Tunneling Protocol).
- Aconsegueix els objectius de seguretat:
  - Autenticació (amb capçalera AH auth header)
  - Integritat (amb hash)
  - Confidencialitat (amb xifrat).

IPsec usa xifrat, encapsulant un paquet IP dintre un paquet IPsec.

Es desencapsula al final del túnel, on el paquet IP original és desencriptat i reenviat a la seva destinació.

---

### 3. PROTOCOLS VPN DE CAPA D'APLICACIO.

**A nivell de capa d’aplicació (nivell 7)**: Encapsulen en l’aplicació d’origen de manera que podria o no haver seguretat en les capes inferiors. De fet, és el més segur per que no es refia de si als nivells inferiors hi ha o no xifrat.

Afegeix un nivell més de seguretat, i assegura confidencialitat entre hosts extrems.

#### EXEMPLES

- **SSL (Secure Socket Layer)**
- **TLS (Transport Layer Security)**
- **SSH (Secure Shell)**

#### SSL/TLS (Secure Socket Layer/Transport Layer Security)

Tunnelitza tot el tràfic de xarxa (com fa OpenVPN i SoftEther VPN) o assegura una connexió individual.
  - Funciona sobre qualsevol protocol confiable com ara TCP.
  - Es fa servir una xarxa VPN amb SSL quan IPsec te problemes amb NAT (Network Address Translation) i les regles del firewall.
  - TLS és una evolució de SSL, que afegeix noves opcions i millora la seguretat.

##### SSL/TLS: FASES DE LA COMUNICACIO

SSL/TLS implica una sèrie de fases:


<p align="center">
<img src="imgs/vpn-41b7af61.png" width="400" />
</p>
<p align="center">
SSL/TLS: Fases del protocol SSL/TLS
</p>


1. **Negociar entre les parts l’algorisme de xifrat** que s’usará en la comunicació (ClientHello-ServerHello).
2. **Intercanvi de claus públiques i autenticació basada en certificats digitals**. El certificat del client és opcional, ja que requeriria una CA en el costat del client.
3. **Intercanvi de claus privades** (intercanviades per Criptografia asimètrica)
4. **Xifrat del tràfic basat en xifrat simètric**.

---

### SSH (Secure Shell)

<p align="center">
<img src="imgs/vpn-583c7f58.png" width="500" />
</p>
<p align="center">
SSH
</p>


Secure Shell (SSH) VPN: OpenSSH connecta remotament a una xarxa o permet enllaços punt a punt.
- Connexió limitada de túnels concurrents. No suporta autenticació personal.
- Hi ha un tram insegur: La comunicació entre el client remot i el servidor SSH són segures, però no entre servidor SSH i servei tunnelitzat.


<p align="center">
<img src="imgs/vpn-7c253a85.png" width="600" />
</p>

- Per poder veure el servidor web ubicat a 192.168.0.5
~~~
$ ssh -p 2222 -N -L 8080:192.168.0.5:80 usuari@155.10.10.1
~~~

- Per poder veure el servidor web ubicat al propi firewall, no accessible directament
~~~
$ ssh -p 2222 -N -L 8080:localhost:443 usuari@155.10.10.1
~~~

- Un cop iniciat el túnel, només cal obrir el navegador web indicant http://localhost:8080. Podem fer-ho amb el client que sigui, per a qualsevol protocol si el client permet canviar el port de destí de la petició.

---

### Diferents túnels SSH


<p align="center">
<img src="imgs/NAS-VPN-d0274b66.png" width="700" />
</p>
<p align="center">
https://iximiuz.com/en/posts/ssh-tunnels/
</p>

---

## PROTOCOLS VPN PROPIETARIS

Existeix una gran varietat de protocols propietaris. A mode d'exemple:
- **Datagram Transport Layer Security (DTLS)**: Cisco AnyConnect VPN and in OpenConnect VPN per solucionar els problemes de SSL/TLS amb túnel sobre UDP.
- **Microsoft Point-to-Point Encryption (MPPE)**: funciona amb PPTP (Point-to-Point Tunneling Protocol) and in several compatible implementations on other platforms.
- **Microsoft Secure Socket Tunneling Protocol (SSTP)**: túnels Point-to-Point Protocol (PPP) o tràfic L2TP sobre un canal SSL 3.0 channel.
- **Multi Path Virtual Private Network (MPVPN)**: de Ragula Systems Development (propietari).

---

# Referències:

## Ports necessaris en una VPN i descripció VPN

- https://blog.mdcloud.es/puerto-vpn-cuales-abrir-para-utilizar-una-red-privada-virtual/

## Internet_Protocol_Security

- IPSec wikipedia - https://en.wikipedia.org/wiki/Internet_Protocol_Security

- IPSec tècnic - https://howdoesinternetwork.com/2014/ipsec-tunnel-transport-mode

## VPN amb autenticació RADIUS

https://prezi.com/2rrdykfk13pj/instalacion-y-configuracion-vpn-con-radius-en-windows-2012-server/

## Serveis VPN gratuïts "fiables"

- ProtonVPN - https://protonvpn.com
- NordVPN - https://nordvpn.com

## Túnel SSH

- Tots els túnels SSH (molt bo): https://iximiuz.com/en/posts/ssh-tunnels/
- Túnel SSH (interface tun): https://help.ubuntu.com/community/SSH_VPN
