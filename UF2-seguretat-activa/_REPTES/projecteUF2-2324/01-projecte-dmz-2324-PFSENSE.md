# PROJECTE UF2: Configuració de xarxa amb seguretat perimètrica  
# Primera part: DMZ de 3 "potes"

**_MOLT IPORTANT:_ CAL LLEGIR AMB ATENCIÓ A CADA ENUNCIAT**

- **ATENCIÓ**: Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, carpetes, etc. han de ser el vostre nom+cognom. **A TOTES les captures s'ha de veure el vostre nom i cognoms**. Si no es fa així, caldrà tornar a repetir TOTA la pràctica amb les captures adients.

- **Responeu a totes les preguntes**, si cal cercant informació als apunts del mòdul, als manuals de les aplicacions, fitxers d'ajuda i "man" i si no es troba la solució, fins i tot a Internet com a últim recurs.

- A les captures heu de **MARCAR amb cercles o fletxes la part rellevant**. Aquestes captures han de tenir un context, o sigui, saber d'on surten. No feu mini-captures ja que no sabré d'on surt la informació. Podeu fer servir "FlameShot" per capturar i afegir fletxes, etc.

- Abans de començar **podeu fer un "snapshot" o instantània de la maquina virtual**. Això farà que l'estat actual de la màquina virtual es guardi i pugueu fer tots els canvis que vulgueu sense por de fer malbé alguna cosa. Poseu-li de nom "Abans de (l'acció que aneu a fer)...". En acabar, haureu de revertir aquesta instantània per tornar a l'estat anterior.

- En general, heu de **comprovar SEMPRE que els canvis de configuració que feu produeixen l'efecte desitjat** amb el conjunt de proves necessari.

---

**ATENCIÓ: La informació proporcionada té un caràcter educatiu i en cap cas se'n pot fer un ús delictiu o maliciós. Cal fer un ús responsable i ètic dels coneixements adquirits. MAI ho heu de fer amb un equip de producció (màquina física o de la feina). En cas contrari, es pot incórrer en delictes tipificats al codi Penal amb penes que poden suposar multes i fins i tot presó.**

**Es declina tota responsabilitat en el cas que aquesta informació sigui utilitzada amb finalitats il·lícites o delictives. També es declina tota responsabilitat en cas de destrucció total o parcial de dades. Cal fer abans còpies de seguretat per no perdre cap dada.**

---

## Referències i ajuda

- Descàrrega PFSense - https://www.pfsense.org/
- Documentació PFSense - https://docs.netgate.com/pfsense/en/latest/
- Vídeo explicació - https://www.youtube.com/watch?v=wuReb-XEPyw&list=PLZISSe2iUa2Cm-H5-KJhVg9k1vZYRoNwj
- Problemes amb paquets Pfsense - https://docs.netgate.com/pfsense/en/latest/troubleshooting/upgrades.html

---

## Objectius

- L'objectiu d'aquesta projecte és crear una xarxa més segura, familiaritzar-se amb el concepte de DMZ i explorar les possibilitats de la distribució PFSense.

## Introducció

En aquesta primera part del projecte haurem de fer un muntatge d'una xarxa DMZ amb tres potes:

![](imgs/exercici-dmz-78aeab00.png)

DMZ 1 router, 3 potes

---

PFSense és una distribució FreeBSD (64bit) que proporciona diferents serveis de xarxa i funciona com a Firewall.
- Es pot configurar des d'una interfície web accessible des de la xarxa LAN.
- Quan configurem el firewall, hem de pensar que les regles s'apliquen a les peticions d'inici de connexió. Les respostes es permeten automàticament si hi ha una connexió iniciada (per que s'ha permés el paquet d'inici).

- Recordeu de comprovar el hash del fitxer baixat abans de començar.

**NOTA:** Per a poder veure des de l'equip amfitrió les màquines internes cal afegir a la taula de rutes del host amfitrió la xarxa DMZ (192.168.200.0) i si cal la LAN (192.168.100.0). El seu gateway serà la IP del PFSense a la xarxa WAN (mireu quina us assigna). Per afegir una ruta a la taula de rutes feu servir la comanda, tenint en compte que és temporal fins apagar l'equip:
~~~
ip route add 192.168.200.0/24 via ip-externa-PFSense
ip route add 192.168.100.0/24 via ip-externa-PFSense
// Per veure la taula de rutes i comprovar que s'han afegit
ip route
~~~

Per a fer-ho de forma permanent, consultar la configuració d'interfície per afegir rutes estàtiques: https://netplan.readthedocs.io/en/stable/examples/#how-to-configure-source-routing. En l'exemple suposem que la IP del PFSense és 192.168.5.20. El fitxer de configuració es troba a /etc/netplan/

~~~
network:
  version: 2
  ethernets:
    ens3:
      addresses:
        - 192.168.5.30/24
      dhcp4: no
      routes:
        - to: default
          via: 192.168.5.1
        - to: 192.168.100.0/24
          via: 192.168.5.20
        - to: 192.168.200.0/24
          via: 192.168.5.20
~~~

Un cop configurada la xarxa, cal aplicar els canvis amb:

~~~
sudo netplan apply
~~~

Amb CentOS consulteu https://www.networkinghowtos.com/howto/adding-persistent-static-routes-on-centos/

---

## Requisits del projecte

**(2 punts)** Mapa lògic i configuració bàsica de PFSense:

El nostre firewall serà un PFSense amb 3 interfícies:
- Una externa connectada via **xarxa pont** que serà 172.16.26.0/24 a l'aula o bé potser 192.168.0.0/24 a casa i que serà automàtica (DHCP).
- Una altra interna per la xarxa LAN (192.168.100.0/24).
- Una altra interna per la DMZ (192.168.200.0/24).

1.  Escolliu un programa per a fer mapes lògics de xarxa. Feu el mapa lògic del vostre muntatge. Feu-hi constar:
- Les adreces IP de totes les interfícies.
- Els serveis que s'ha posat en marxa a cada equip.

2. L'ordinador de la xarxa LAN ha de poder navegar per Internet.

3. El servei SSH i el servei d'administració web del Firewall han d'estar permesos a la interfície WAN. El password de l'administrador ha de ser segur. Un cop fet, podreu administrar el Firewall des de l'equip físic si ho preferiu. **NOTA: Si no feu aquest pas, no es podrà comprovar remotament amb l'script de comprovació del professor.**

---

NOTA: El PFSense **no permet per defecte la configuració web des de la interfície WAN** i per tant s'hauria de configurar per una de les interfícies LAN (o sigui, per exemple des de la màquina virtual Ubuntu Desktop).

Un cop instal·lat PFSense es pot assignar la configuració de les interfícies i determinar l'adreça IP. Des d'un equip de la xarxa LAN s'ha d'obrir un navegador web i obrir l'aplicació web d'administració de PFSense.

---

**(4 punts)** Configuració de la DMZ

- Posarem un equip servidor a la DMZ, semblant a com es veu a la imatge del començament:
  - 1. **Ubuntu Server** amb l'adreça 10 de la xarxa DMZ, configurat de forma estàtica NO per DHCP. Tindrà un servei ssh amb banner personalitzat i un servidor web segur i no segur amb una pàgina d'inici personalitzada.
  - 2. **Un equip client** amb l'adreça 50 de la xarxa LAN, que pot ser un Ubuntu Desktop. Tindrà el servei ssh configurat amb un banner personalitzat.

**NOTA: Personalitzat vol dir que apareix el vostre nom i cognoms.**

1. Actualitzeu el mapa lògic.

2. Configurem les opcions de Firewall->Rules:

  - El firewall ha de **permetre el tràfic iniciat des de la xarxa interna cap a la DMZ**, només pels serveis que tingui el server de la DMZ. Cal que arribi la resposta (això últim és automàtic a PFSense).

  - També ha de **permetre que els servidors de la DMZ accedeixin tant a Internet com a la xarxa externa** que és del rang 172.16.101.0/24, i els arribi la resposta.

  - S'ha de **permetre accedir als servidors de la DMZ des de la xarxa externa, només pels serveis que tingui el server de la DMZ**. NO utilitzeu NAT a la interfície WAN, si no que heu d'obrir els ports necessaris ja que la IP de destí serà la del servidor a la DMZ.

  - **No s’ha de permetre**:
    - l’accés directe dels equips de la xarxa interna cap a la xarxa externa (ni internet), per a cap protocol.
    - l’accés directe dels equips de la xarxa externa (ni internet) a la xarxa interna, per a cap protocol.

  - **No s'ha de permetre iniciar connexió des de la xarxa DMZ cap a la xarxa interna**.

---

L’objectiu de la pràctica és poder comprovar els següents punts (més endavant configurarem serveis i més limitacions):

**(4 punts)** Fer comprovacions que demostrin els punts que segueixen. **A la captura, sense talls, s'ha de veure els paràmetres IP (l'adreça, màscara, taula de rutes) de la màquina des de la que s'està fent la prova**. :

1. Fer comprovacions que demostrin **des d’un equip de la xarxa interna**:
  - Poder fer **ping, tràfic ssh i accés a servidors web** cap al servidor de la DMZ i la resposta corresponent.
  - Fer un mtr cap a 8.8.8.8 per veure per on passen els paquets. Provar a navegar per internet. S'ha de poder?
  - Comprovar si es pot **resoldre noms de domini** amb nslookup www.microsoft.com.
  - Fer **nmap cap al servidor de la xarxa DMZ** per veure quins serveis us mostra (només haurien de ser els habilitats al firewall).

2. Fer comprovacions que demostrin **des d’un equip de la xarxa externa** (equip amfitrió):
  - No poder fer ping (ni tràfic) des d’un equip de la xarxa externa (amfitrió) cap als equips de la xarxa interna. Recordeu que primer heu de configurar la taula de rutes a l'amfitrió per que sàpiga com arribar a la xarxa interna i la DMZ.
  - Poder fer ping (i tràfic ssh i web) cap a la IP del servidor de la DMZ.
  - Fer nmap cap al servidor de la xarxa DMZ per veure quins serveis es mostren.

3. Fer comprovacions que demostrin, **des d'un equip de la DMZ**
  - No poder accedir cap a l'equip de la xarxa interna. Es pot provar intentant connectar via ssh al Desktop intern i fer ping.
  - Fer un mtr cap a 8.8.8.8 per veure per on passen els paquets. Provar a navegar per internet.
  - Comprovar si es pot resoldre noms de domini amb nslookup www.microsoft.com.
  - Provar a navegar per Internet amb un navegador en mode text com ara lynx. Es pot?
  - Fer nmap cap a l'equip de la xarxa interna per veure quins serveis us mostra.
