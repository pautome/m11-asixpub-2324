# PROJECTE UF2: Configuració de xarxa amb seguretat perimètrica  

## Part 2: Configuració de VPN (OpenVPN amb PfSense)

**_MOLT IPORTANT:_ CAL LLEGIR AMB ATENCIÓ A CADA ENUNCIAT**

- **ATENCIÓ**: Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, carpetes, etc. han de ser el vostre nom+cognom. **A TOTES les captures s'ha de veure el vostre nom i cognoms**. Si no es fa així, caldrà tornar a repetir TOTA la pràctica amb les captures adients.

- **Responeu a totes les preguntes**, si cal cercant informació als apunts del mòdul, als manuals de les aplicacions, fitxers d'ajuda i "man" i si no es troba la solució, fins i tot a Internet com a últim recurs.

- A les captures heu de **MARCAR amb cercles o fletxes la part rellevant**. Aquestes captures han de tenir un context, o sigui, saber d'on surten. No feu mini-captures ja que no sabré d'on surt la informació. Podeu fer servir "FlameShot" per capturar i afegir fletxes, etc.

- Abans de començar **podeu fer un "snapshot" o instantània de la maquina virtual**. Això farà que l'estat actual de la màquina virtual es guardi i pugueu fer tots els canvis que vulgueu sense por de fer malbé alguna cosa. Poseu-li de nom "Abans de (l'acció que aneu a fer)...". En acabar, haureu de revertir aquesta instantània per tornar a l'estat anterior.

- En general, heu de **comprovar SEMPRE que els canvis de configuració que feu produeixen l'efecte desitjat** amb el conjunt de proves necessari.

---

**ATENCIÓ: La informació proporcionada té un caràcter educatiu i en cap cas se'n pot fer un ús delictiu o maliciós. Cal fer un ús responsable i ètic dels coneixements adquirits. MAI ho heu de fer amb un equip de producció (màquina física o de la feina). En cas contrari, es pot incórrer en delictes tipificats al codi Penal amb penes que poden suposar multes i fins i tot presó.**

**Es declina tota responsabilitat en el cas que aquesta informació sigui utilitzada amb finalitats il·lícites o delictives. També es declina tota responsabilitat en cas de destrucció total o parcial de dades. Cal fer abans còpies de seguretat per no perdre cap dada.**

---

## Referències i ajuda

- Configurar VPN a Pfsense. Vídeo Pau Tomé - https://www.youtube.com/watch?v=K26Pir7xu5s
- Descàrrega Pfsense - https://www.pfsense.org/download/
- Problemes amb paquets Pfsense - https://docs.netgate.com/pfsense/en/latest/troubleshooting/upgrades.html
- Documentació Pfsense - https://docs.netgate.com/pfsense/en/latest/index.html
- Configurar VPN amb Pfsense - http://www.3ops.com/implementacion-de-vpn-cliente-servidor-con-openvpn-y-pfsense/


## Objectius

- L'objectiu d'aquesta part del projecte és familiaritzar-se amb el concepte de VPN i la implementació de Pfsense. Crear un mètode d'accés remot als recursos de la xarxa local.

---

## Introducció

### Les VPN

![](imgs/openvpn-pfsense-5a523c6f.png)

Observeu que tenim al menys 3 xarxes en total:

1. La **xarxa WAN** que permet connectar-se des del client remot a la interfície WAN del Pfsense. En realitat, el client remot i el Pfsense no tenen normalment IP's de la mateixa xarxa, són de xarxes diferents. Però tot i això han de poder ser accessibles entre ells.

2. La **xarxa LAN interna** que aquí és la 192.168.100.0/24 i que no és accessible des de l'exterior, però si des del Pfsense.

3. La **xarxa del túnel** que aquí és la 10.8.0.0/24 i que seràn les adreces IP que s'assignarà a les interfícies virtuals del client remot i el Pfsense en el moment que s'estableixi el túnel. **És molt important que no coincideixi ni amb la xarxa del client remot ni amb cap de les LAN a les que volem accedir per la VPN**.

Per a **crear un servidor OpenVPN** i poder connectar a ell es necessita:
- El propi servidor OpenVPN, que serà el Pfsense. La interfície amb IP pública per connectar-se serà la WAN (que en la simulació en realitat serà una IP privada de l'aula).
- **Al menys un client** que es connecta via la interfície WAN del servidor.
- **Un certificat per al servidor VPN** (amb les seves corresponents claus privada i pública).
- **Una CA** (Certificate Authority, recordeu la infrastructura PKI) en que puguin confiar els clients i el servidor.
- **Un certificat per a cada client** (amb les seves corresponents claus privada i pública).
- **Configurar fitxer de OpenVPN del servidor i el client** amb els mateixos paràmetres.

Llavors seguirem les següents passes per a configurar la VPN amb PFSense:
1. Creació de Certificate Authority (CA) i Certificat Arrel de la CA.
2. Creació del Certificat del Servidor.
3. Configuració del Servidor OpenVPN.
4. Activar una regla per obrir el port VPN al Firewall (a la interfície WAN).
5. Creació dels usuaris + Certificats dels usuaris.
6. Exportació del fitxer de configuració del client i instal·lació del Client.

**L'objectiu serà poder treballar a la xarxa interna de l'empresa (darrera el firewall Pfsense) des de fora però com si estiguéssim a la mateixa xarxa local.**

---

### Configuració de servidor OpenVPN amb Wizard de Pfsense

Podem fer servir el Wizard per a crear un nou servidor OpenVPN o fer les passes manualment com hem descrit abans.

- Anem a VPN->OpenVPN->Wizard

En el cas d'executar amb Wizard, instal·lem un servidor d'accès remot. Podem escollir desprès entre Remote Access (servidor-client, per treballadors remots) o Peer to peer (entre servidors si volem connectar dues seus):

1. **Type of Server**: Escollim on resideixen els usuaris (base de dades local, LDAP o RADIUS). Per no haver de muntar un altre servei, escollim local.
2. **Escollir la CA**. Si no l'hem creat abans, podem fer-ne una de nova ara. Poseu el vostre nom-cognom per identificar-la.
3. Crear el **certificat del servidor VPN**. Si no l'hem creat abans, podem fer-ne un de nou ara.

4. Ara cal configurar els **paràmetres del servidor OpenVPN**

- **Interfície** (WAN per donar accés a equips remots, altres per a accès local)
- **Protocol** (UDP per defecte, per tenir velocitat en xarxes fiables. TCP per tenir fiabilitat a costa de velocitat)
- **Port**, 1194 per defecte en OpenVPN.
- **Descripció**: Les nostres dades.

---

- **Paràmetres criptogràfics**:
1. Autenticació TLS i creació de nova clau TLS.
2. Paràmetres de la clau Diffie Hellman (DH), usats per establir una comunicació segura.
3. Algorisme de xifrat entre els dos extrems (server i clients l'han de configurar igual). Pot ser accelerat per hardware si les màquines disposen de xips criptogràfics (AES per defecte).
4. Algorisme hash d'autenticació (per defecte SHA256)
5. Hardware d'acceleració criptogràfica si n'hi ha.

---

- **Paràmetres del tunnel**:
1. **Xarxa a fer servir al tunel** (posarem una xarxa privada que no es faci servir enlloc més, per exemple 10.0.8.0/24. El servidor tindrà al seu extrem del tunel l'adreça 10.0.8.1 i els clients aniran agafant adreces succesives).
2. **Redirect Gateway**: Força a tots els equips a passar TOT el seu tràfic pel tunel.
3. **Local Network**: Xarxes locals que hi ha darrera el servidor VPN. S'hi podrà accedir des dels nodes remots a les que s'indica aquí.
4. **Connexions concurrents màximes** (nombre d'equips que poden iniciar sessió)
5. **Compressió de les dades** (carrega els equips ja que consumeix CPU però aprofita millor l'ample de banda de la connexió de xarxa)
6. Type-of-Service
7. **Inter-Client Communication**, els clients es veuran o no.
8. **Allow multiple concurrent connections**: permetre obrir més d'una connexió VPN amb el mateix nom (no recomanat). Permet mateix certificat per diferents hosts.

---

- **Opcions de client**:

1. IP dinàmica
2. Topology
3. DNS default Domain, DNS Server 1, 2, 3, 4, NTP Server,
4. NetBIOS Options, NetBIOS Node Type, NetBIOS Scope ID
5. WINS Server 1, 2

---

- **Regles de firewall**
1. Permetre accedir al port 1194 des de internet
2. Permetre passar pel tunel el tràfic VPN.

---

### Revisió dels paràmetres de la VPN

1. Podem revisar les **configuracions del servidor** **OpenVPN a VPN->OpenVPN**.  
2. Podem revisar la **configuració de la CA i els certificats** a **System->Cert Manager** i a la **pestanya CA o Certificates**.
3. Podem **crear usuaris LOCALS** per utilitzar a la VPN a **System->User Manager**. A la pestanya "Authentication Servers" podríem enllaçar amb un servidor extern d'autenticació LDAP o RADIUS.
4. Podem revisar les regles del Firewall. D'entrada observeu que tenim una interfície virtual nova (de túnel) anomenada per defecte OpenVPN. Veurem les regles que s'hi ha afegit.

---

### Crear usuari i certificat: System->User Manager

Pestanya Users:
1. Crear nou usuari que es connectarà via client VPN (a la base de dades interna del Pfsense).
2. Crear el certificat de client (vigilar que el tipus de certificat no sigui servidor si no de client).

---

### Exportar el fitxer  de configuració del client:

Configurar el client VPN (són els equips que volen iniciar sessió a la VPN des d'internet, **ESTAN FORA!**):

A l’equip que vol iniciar sessió VPN, que per a provar pot ser l’equip físic **que s'ha de trobar a la xarxa EXTERNA** o un altre equip virtual amb interfície en xarxa pont:

1. **Exportar el fitxer de configuració OpenVPN** del Pfsense (VPN->OpenVPN->Client Export). Si no hi és l'opció, instal·leu el paquet de Pfsense que dona la funcionalitat a System->Package Manager.

  **ATENCIÓ:**
  Si hi ha problemes amb paquets Pfsense consulteu - https://docs.netgate.com/pfsense/en/latest/troubleshooting/upgrades.html
  - Comproveu que el gateway per defecte del Pfsense és la interfície WAN a System->Routing
  - Comproveu que el DNS està ben configurat.
  - Podeu instal·lar a ma obrint una consola: Pkg install -y pfsense-pkg-openvpn-client-export


2. Vigileu de marcar l’opció Legacy client, en el cas que el client openvpn de l’equip client sigui anterior a la versió 2.4 (comproveu-ho). En cas contrari no cal.

3. **Baixeu la configuració adequada** per al tipus d’equip client. Aquí baixem "Inline Configurations", "Most clients". Obtindrem un fitxer amb extensió ovpn. Aquest fitxer és la configuració per un client OpenVPN. Veureu que hi ha dues parts:
 - La primera conté les directives de configuració del client OpenVPN.
 - La segona conté els certificats i claus.

- El fitxer que baixeu conté la configuració del client VPN semblant a aquesta:

~~~
dev tun
persist-tun
persist-key
data-ciphers-fallback AES-128-CBC
auth SHA256
tls-client
client
resolv-retry infinite
remote 192.168.0.28 1194 udp4
lport 0
verify-x509-name "Server-Pau-Tome" name
auth-user-pass
remote-cert-tls server

<ca>
-----BEGIN CERTIFICATE-----
MIIEGDCCAwCgAwIBAgIBADANBgkqhkiG9w0BAQsFADBmMRQwEgYDVQQDFAtDQS1Q
...(segueix)
RmXHYdCktz9DKsZ1Kl5uALTflAq7dJOc05n5I2PMgUzkCbAxY7wJDLJmC4U=
-----END CERTIFICATE-----
</ca>
<cert>
-----BEGIN CERTIFICATE-----
MIIEhTCCA22gAwIBAgIBAjANBgkqhkiG9w0BAQsFADBmMRQwEgYDVQQDFAtDQS1Q
...(segueix)
EmoIGpr2SdXw
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDIpkfOZCsySeMz
...(segueix)
JWAhmPnd7OQjnf53uXb3NIU=
-----END PRIVATE KEY-----
</key>
key-direction 1
<tls-auth>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
e40585fd2e64bbdc7663e0cb7c9c0a19
...(segueix)
901317bf1527b9e9bf3d8c60e794e99a
-----END OpenVPN Static key V1-----
</tls-auth>
~~~

Observeu que hi ha uns blocs marcats:

~~~
<cert> ... </cert>
<key> ... </key>
<ca> ... </ca>
<tls-auth>...</tls-auth>
~~~

Aquests blocs contindran respectivament:
- el certificat de l’usuari amb la seva clau pública (cert)
- la clau privada de l’usuari (key)
- el certificat de la CA (ca) que havíem creat.
- La clau TLS per a identificar clients i servidors (és opcional però aporta un nivell més de seguretat).

En principi, podríem guardar-ho en fitxers independents i incloure'ls al fitxer ovpn (no farem res d'això ara).

---

#### Client en mode consola (Linux)

Si executeu directament a consola el client VPN hauria de funcionar la connexió (instal·leu el client OpenVPN si no hi és):
~~~
sudo openvpn --config config.ovpn
~~~

---

#### Client en mode gràfic (Linux i Windows)

#### Linux

**Recordeu que el client s'ha de trobar a la XARXA EXTERNA.**

1. Instal·leu al client el paquet network-manager-openvpn-gnome si és necessari.
2. A la icona de Network Manager->Edita les connexions->Afegeix->VPN->OpenVPN.
3. La pasarel·la és el servidor OpenVPN. Si piqueu Avançat, podeu configurar les opcions que al fitxer de configuració es troben al principi.
4. Per provar la connexió aneu al Network Manager->Conexions VPN i escolliu la connexió creada.
5. Comproveu que s’ha creat una interfície tun amb la ip de la xarxa virtual assignada (normalment 10.0.8.2).

6. Feu una **captura dels logs**, tant si funciona bé com si no:
- **Al client**: sudo tail -f /var/log/syslog | grep vpn
- **Al servidor**: VPN->OpenVPN->icona “Related logs entries” a la part superior dreta de la pantalla. O bé, Status->OpenVPN.

7. Si tot va bé, haureu de poder fer ping a un equip de la xarxa local que hi ha darrera del servidor VPN (usant la seva adreça privada) i també al servidor VPN en la seva adreça per defecte, 10.0.8.1 (si heu fet servir la xarxa 10.0.8.0 per les adreces virtuals de la VPN).

8. Obriu el navegador per accedir al servidor web del client de la xarxa interna, amb la seva adreça privada. Connecteu també via ssh al servidor intern.

9. Vigileu que el firewall permeti passar els protocols que feu servir per a les proves (Firewall->Rules->OpenVPN).

10. Comproveu amb traceroute des del client remot per quins gateways es passa per arribar a Internet.

---

### Depuració del funcionament de la VPN

Si alguna cosa va malament podem fer servir diferents eines:

1. **Al servidor**
- Posar un panell de visualització a la pantalla principal (Dashboard, picar +, afegir "OpenVPN").
- Logs del Firewall (Firewall->Rules->icones a dalt a la dreta - status i logs)
- Logs de VPN (VPN->OpenVPN->icones a dalt a la dreta - status i logs)

2. **Als clients**
- Logs de client OpenVPN Gnome:
~~~
tail -f /var/log/syslog | grep vpn
~~~

- Logs de client Windows via l'app del client.

---

## Enunciat repte

1. **3 punts**: Farem les passes per configurar el servidor VPN al Pfsense.

2. Tenim les xarxes LAN i DMZ de la part anterior configurades. Si no, caldrà fer la primera part abans.

3. Ja tenim **Blocat tot el tràfic de la LAN cap a fora** (cal treure les regles permissives) **i de la WAN cap a la LAN**, per assegurar-nos que només s'hi pot accedir pel tunel VPN. Només es permetrà a la interfície WAN del Firewall el port de la VPN, així com els ports que teníem oberts per accedir a la DMZ.

4. **0,5 punts**: Permeteu al firewall, a la interfície VPN, només l'accés als serveis de la LAN (ssh de l'equip client) i als serveis de la DMZ web i ssh. Haureu de publicar la xarxa DMZ també en la configuració de la VPN.

5. **1 punts**: Instal·leu i configureu el client openVPN del client Linux.
  - Feu servir el seu fitxer de configuració per iniciar sessió a la VPN (usuari Linux-nom-cognom).

6. **1 punts**: Instal·leu el client openVPN a un client Windows
  - Feu servir el seu fitxer de configuració per iniciar sessió a la VPN (usuari Win-nom-cognom).

7. **1 punts**: Inicieu sessió des del client Linux i feu captures de les evidències que tot funciona (logs dels clients i logs del Pfsense on indiqui que s'ha establert la connexió).

8. **1 punts**: Captureu el resultat:
  - Mostreu la nova interfície creada a l'equip client.
  - Mostreu també els logs on indiquin que l'equip s'ha connectat.

9. **1 punts**: Inicieu sessió des del client Windows.Feu captures de les evidències que tot funciona.
  - logs dels clients
  - logs del Pfsense on indiqui que s'ha establert la connexió).

10. **0,5 punts**: Accedirem des dels clients:
  - Als serveis de la xarxa LAN utilitzant les IP's privades de la LAN (192.168.100.x) amb un client ssh i a la DMZ amb un client web i ssh.
  - Haurem de veure el banner ssh i la pàgina web preparades a l'equip de la xarxa LAN i DMZ.

11. **0,5 punts**: Des dels clients VPN:
  - Executeu traceroute 8.8.8.8 i traceroute 1.1.1.1 per observar quina ruta segueixen els paquets cap a Internet.
  - Permeteu l'accés a Internet dels equips que es connectin via la VPN.

12. **3 punts** Necessitem una altra seu que simularem amb un altre Pfsense secundari amb només una xarxa LAN, on caldrà tenir un equip de proves.
- Connectarem les seus de manera que el segon pfsense serà considerat un client del primer, tot i que farà les funcions d'enrutament cap a la seva xarxa local o cap a la xarxa local del primer Pfsense.  
- Caldrà crear un servidor OpenVPN en el segon PfSense, de tipus Site2site.
- Caldrà crear a la configuració del Pfsense secundari un nou equip client openVPN, per poder crear les credencials de connexió.
- Caldrà provar a accedir al servidor web i equips clients de les xarxes del servidor Pfsense principal des de un equip a la LAN del Pfsense secundari.

NOTA: Vigileu de no repetir els identificadors de xarxa LAN ni DMZ en els dos servidors Pfsense.
