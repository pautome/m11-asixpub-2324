# REPTE: Treball amb VirtualBox i les seves xarxes

> ATENCIÓ: Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, etc. han de ser el vostre per a que es pugui veure quan es corregeix o es fan captures.

> Teniu a sota dels reptes les referències per a fer-los

---

## Objectius

- L'objectiu d'aquesta pràctica és repassar i provar algunes característiques de VirtualBox i configurar les màquines per les següents pràctiques. Els exercicis a fer estan al final del document.

## Conceptes previs

- **Equip amfitrió**: La màquina física que fa córrer el VirtualBox.
- **Equip convidat**: La màquina virtual que corre sobre l'amfitrió.

## Reptes

### Configuració inicial

1. Configura el directori on es guardaran les màquines virtuals per defecte. Fes que sigui a una partició on hi hagi molta capacitat de dades. En el cas de SMX, feu servir la partició NTFS i fiqueu-les a una carpeta "MV".
2. Configura el VirtualBox amb Extension Pack.

### Crear màquines virtuals a partir de ISO

1. Descarrega les imatges ISO de la web d'Ubuntu (22.04 Desktop, 22.04 Server). Comprova els hash de les imatges ISO abans de començar.
2. Crea les màquines virtuals plantilla de Ubuntu (UDesktop 22.04, UServer 22.04).
3. Agrupa les màquines plantilla en un grup anomenat "PLANTILLES".

### Preparar Guest Additions i clonar

1. Configura Guest Additions en la plantilla Ubuntu Desktop.

2. Crea un clon enllaçat de l'Ubuntu Desktop. Posa-li de nom PC1. Recorda reiniciar l'adreça MAC del nou equip.

### Configurar màquina clonada

1. Activa la xarxa "NAT" al clon "PC1". Posa'l en marxa. Els paràmetres IP es configuren automàticament.

2. Mira quina adreça té l'equip convidat i quina adreça s'ha assignat a l'equip amfitrió.

3. Fes ping des de la màquina amfitriona a la màquina convidada i des de la convidada a la amfitriona. Comprova que no es pot.

4. Fes ping a www.google.es des de la màquina convidada i comprova que obtens resposta. Hauries de poder accedir a Internet.

5. Instal·la ssh a la màquina convidada. Configura el reenviament NAT de la màquina convidada: posa 2222 al port amfitrió i 22 al port convidat. No cal posar IP ni de la convidada ni de l'amfitriona. Connecta't via ssh a la màquina convidada des de la consola de l'amfitriona ( ssh -p <portamfitrió> localhost ). Comprova que es pot.

6. Crea una carpeta compartida de manera que la màquina convidada pugui accedir a la carpeta de descàrregues de l'amfitriona.

### Canviar de xarxa (a Només amfitrió)

1. Obre les propietats de la màquina virtual i vés a xarxes. O bé, pica amb el botó dret la icona de xarxa que apareix a sota la màquina virtual iniciada i vés a propietats de xarxa. Canviarem la xarxa NAT per xarxa Només Amfitrió VBoxNet0 **sense apagar la màquina virtual**.

2. Activa i desactiva la targeta de xarxa del sistema operatiu Ubuntu per que s'actualitzi l'adreça IP.

3. Mira quina adreça té l'equip convidat i quina adreça s'ha assignat a l'equip amfitrió a la targeta VBoxNet0.

4. Fes ping des de la màquina amfitriona a la màquina convidada i des de la convidada a la amfitriona. S'ha de poder.

5. Fes ping a www.google.es des de la màquina convidada i comprova que NO obtens resposta. No es pot accedir a Internet.

6. Connecta't via ssh a la màquina convidada des de l'amfitriona. Comprova que es pot.

### Xarxa interna

1. Torna a canviar la xarxa del PC1 però ara escull "Xarxa interna". Posa-li un nom com ara "XInterna"
2. Fes un altre clon de la màquina plantilla "UDesktop 22.04" i anomena-la "PC2". Configura-li a propietats de xarxa la xarxa interna "XInterna".
3. Configura de forma estàtica els paràmetres IP dels dos clons, ja que no hi ha DHCP a les xarxes internes.
4. Fes ping d'un equip a l'altre. S'hauria d'obtenir resposta.
5. Fes ping a www.google.es des de la màquina convidada i comprova que NO obtens resposta. No es pot accedir a Internet.
6. Connecta per ssh de PC2 a PC1 i comprova que es pot.
7. Fes ping des de la màquina amfitriona i comprova que no es pot.


### Posar dues targetes de xarxa a un host

1. Atura el PC2 i afegeix una nova targeta de xarxa configurada com a NAT. Torna a engegar PC2.
2. Des de PC2, comprova que pots accedir a Internet i també fer ping a PC1.
3. Obre una consola a PC2 i escriu (per convertir PC2 en un router amb NAT):
~~~
sudo sysctl net.ipv4.ip_forward=1
sudo iptables -t nat -A POSTROUTING -s 192.168.100.0/24 -o enp0s8 -j MASQUERADE
~~~

  (Vigila que la xarxa que poses al paràmetre -s sigui la de la interfície amb xarxa interna. Es suposa que enp0s8 és la interfície on has posat "NAT" al VBox).

4. Edita els paràmetres de xarxa de PC1.
   - Posa-li com a gateway l'adreça IP de la xarxa interna de PC2.
   - Posa-li com a servidor DNS l'adreça 1.1.1.1
5. Comprova que ara PC1 tindrà accés a Internet a través del PC2, que farà de router.

**NOTA**: Si no funciona, pot ser que el firewall intern estigui activat al PC2. Desactiva'l: "sudo ufw disable"

![](imgs/virtualbox-d4ba4af1.png)

---

## Referències i ajuda

- Xarxes VirtualBox - https://www.pinguytaz.net/index.php/2016/11/20/virtualbox-configurando-la-red/

- Tutorial per comprovar imatge ISO d'Ubuntu (29-09-2021) - https://ubuntu.com/tutorials/how-to-verify-ubuntu#1-overview

- Avaluació de productes de Microsoft (29-09-2021) - https://www.microsoft.com/en-us/evalcenter/#products

---

## Instal·lació

Assegureu-vos d'instal·lar el VirtualBox 7.0 seguint les instruccions de la pàgina https://www.virtualbox.org/

## Configuració

A continuació teniu les passes que seguim normalment per a crear màquines virtuals amb VirtualBox.

0. Configurar Directori on es guarden les màquines virtuals
1. Instal·lar Extension pack a VirtualBox
2. Baixar imatges ISO i comprovar la integritat
3. Crear màquines virtuals base o plantilla
4. Clonar màquines (clonació enllaçada)
5. Configurar xarxa de la màquina virtual

---

### 0. Configurar Directori on es guarden les màquines virtuals

Aneu a Fitxer->preferències:

![](imgs/virtualbox-867b523f.png)

Després, a la finestra que s'obre aneu a General i editeu la "carpeta de la màquina per defecte".

![](imgs/virtualbox-4226d5a5.png)

---

### 1. Instal·lar Extension pack a VirtualBox

Cal instal·lar **Extension Pack** al programa VirtualBox per tenir algunes funcionalitats que no venen per defecte a VirtualBox (suport de dispositius USB 2.0 i USB 3.0, Webcam, VirtualBox RDP, encriptació de disc, NVMe i PXE ROM per a targetes de xarxa Intel).
  - El podem descarregar a la web de VirtualBox https://www.virtualbox.org/wiki/Downloads i fer doble click a sobre per a que s'instal·li o bé anar a Preferències->Extensions i buscar el fitxer.

**NOTA:** Cal instal·lar la mateixa versió de Extension Pack que tenim instal·lada de VirtualBox (veure amb l'opció "Ajuda->Quant a VirtualBox").

![](imgs/virtualbox-058f0b83.png)

---

### 2. Baixar imatges ISO i comprovar la integritat

Recorda **comprovar que s'han baixat correctament** abans de fer servir les imatges ISO .

- Baixa el fitxer que contenen els hash sha256. Els trobaràs a la pàgina d'on descarregues la imatge ISO (per exemple d'Ubuntu).
- Pots fer servir la comanda "sha256sum nom-fitxer" per comprovar el hash de la imatge.
~~~
  $ sha256sum ubuntu-20.04.1-desktop-amd64.iso
 b45165ed3cd437b9ffad02a2aad22a4ddc69162470e2622982889ce5826f6e3d  ubuntu-20.04.1-desktop-amd64.iso
~~~

- També pots fer servir el programa gtkhash.

    ![](imgs/virtualbox-05bbc88c.png)

- El hash SHA256 ha de coincidir amb el que has recuperat de la web on vas baixar la imatge ISO. Compara el hash obtingut amb el que hi havia al fitxer de hash.

---

### 3. Crear màquines virtuals base o plantilla

- Necessitem crear màquines virtuals "plantilla". Primer de tot, crearem un grup al VirtualBox (anomenat "Plantilles" o "Base") amb les màquines plantilla:
  - Ubuntu Server 22.04.
  - Ubuntu Desktop 22.04 (**Instal·lat amb les opcions mínimes**).
  - Windows 10

- Més endavant
  - Kali Linux (Vigileu, hi ha moltes versions. Live, màquina virtual, Install...).
  - Windows Server
  - PfSense

---

### 3.2. Instal·lar Guest Additions a les màquines Base (si escau)

Per a que l'equip convidat pugui interaccionar amb VirtualBox, per exemple per tenir portaretalls compartit o carpetes compartides, cal afegir els **Guest Additions** (depenent del sistema, cal instal·lar un paquet o un altre a la màquina convidada). Un cop arrencada la màquina, al seu menú buscar l'opció per inserir el "cd" amb les Guest Additions.

![](imgs/virtualbox-f4cada4a.png)

---

### 4. Clonar màquines

- Cada cop que necessitem una nova màquina virtual del mateix tipus, en lloc de tornar a instal·lar el sistema, farem un **clon ENLLAÇAT**.

- Picarem opcions avançades i escollirem l'opció "**Clon enllaçat**".
- Per a tenir clar quines màquines virtuals fem servir a cada mòdul, crearem **grups** per separar-les.
  - **Creació d'un grup**: Seleccionar màquines que volem agrupar (Mantenint tecla CTRL podem seleccionar vàries). Botó dret per a crear grup.

  ![](imgs/virtualbox-a3c4a416.png)

  - Resultat: Un grup per a M6 i un per a plantilles.

  ![](imgs/virtualbox-2b5ce73c.png)

  - Físicament, al disc dur tindrem les màquines virtuals en carpetes diferents.

  ![](imgs/virtualbox-0bf71a9d.png)

---

### 5. Configurar xarxa de la màquina virtual

- Aneu a la màquina virtual que voleu editar, piqueu botó dret, Paràmetres i aneu a Xarxa.

  ![](imgs/virtualbox-38873dcd.png)

#### 5.1. NAT (no confondre amb "NAT Network")

- Permet que un equip virtual tingui accés a internet de forma fàcil. Es crea un router virtual que implementa NAT i tradueix les adreces dels equips convidats.

![](imgs/virtualbox-ee2e2bea.png)

- Els equips convidats no són accessibles directament des de l'amfitrió, però es pot configurar el reenviament de ports cap a la màquina convidada i així exposar serveis específics de la màquina convidada.

  ![](imgs/virtualbox-924cbcf4.png)

- Si volem reenviar el port 8080 de la màquina amfitriona cap el 80 de la convidada, afegim una regla de reenviament com aquesta (podem afegir regles per a cada servei que volem exposar):

  ![](imgs/virtualbox-d3bed598.png)

- D'aquesta manera, quan obrim el navegador web a la **màquina amfitriona** i posem http://localhost:8080, apareixerà la pàgina web del servidor web que ha d'estar funcionant a la màquina convidada.

- Això funciona per a tots els ports que necessitem i amb els clients que necessitem, per exemple ssh. Reenviar el port 2222 del host amfitrió cap el 22 de la màquina convidada permetrà accedir via ssh a la convidada (sempre que el servei ssh estigui instal·lat) escrivint :
~~~
$ ssh -p 2222 usuari@localhost
~~~

#### 5.2. Adaptador pont

Permet que l'equip convidat tingui accés a la xarxa física directament.
- Es comportarà com si fos una màquina física més i tindrà una IP de la xarxa física (si existeix un servei DHCP).
- Es pot accedir des de la màquina amfitriona i des de qualsevol de la xarxa física.

![](imgs/virtualbox-3241201f.png)

#### 5.3. Xarxa interna

L'equip convidat NO tindrà accés a la xarxa física ni tampoc internet.
- Caldrà configurar un altre convidat per a fer de router, si calgués.
- Tampoc es pot accedir des de l'amfitrió.

![](imgs/virtualbox-d4ba4af1.png)

---

#### 5.4. Només Amfitrió

La xarxa permet que l'equip amfitrió pugui accedir als equips convidats.
- Es crea una nova interfície en l'equip físic que es connecta a la xarxa "només amfitrió". Així podem interactuar amb els convidats des de l'equip físic, però sense donar accés des de la xarxa física.

- Cal anar a les opcions generals de VirtualBox per a crear una nova xarxa Només Amfitrió. Aneu al menú fitxer, gestor de xarxes amfitrió (Network Manager).

  ![](imgs/virtualbox-9fd66e4d.png)

  VirtualBox 6

  ![](imgs/virtualbox-e262fd76.png)

  VirtualBox 7

---

- Cal picar el botó "crea". Ja tindrem la xarxa només amfitrió.

  ![](imgs/virtualbox-4b6d69cd.png)

  VirtualBox 6

  ![](imgs/virtualbox-db057ff2.png)

  VirtualBox 7

---

- A l'equip amfitrió es crea una nova interfície anomenada vboxnet0, que podeu comprovar que de moment no te IP assignada. VirtualBox li assignarà una adreça normalment dintre de la xarxa 192.168.56.0/24 quan el host convidat es posi en marxa:
~~~
$ ip a
...
4: vboxnet0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 0a:00:27:00:00:00 brd ff:ff:ff:ff:ff:ff
~~~

---

#### 5.5. Opcions de xarxa avançades

A la finestra "Xarxa" de la màquina virtual, en desplegar el botó "Avançades" podrem accedir a les opcions Extra:

![](imgs/virtualbox-8f78a3dc.png)

- **Tipus d'adaptador**: Per defecte Intel PRO 1000. Depenent del sistema operatiu convidat, cal escollir una altra si no la suporta.

- **Mode promiscu**: Permet que des de la màquina virtual es pugui veure el tràfic de xarxa que viatja entre terceres màquines. És a dir, és com si es connectés la interfície a un hub en lloc d'un switch. A part, caldrà posar la targeta en mode promiscu al sistema operatiu convidat si volem veure aquest tràfic (per exemple ho fa Wireshark).
  - Hi ha **tres opcions**: Denega, Permet màquines virtuals o Permet-ho tot.

- **Adreça MAC**: Permet veure quina MAC te la targeta i generar una de nova si ho necessitem (amb el botó a la dreta que representa un cicle blau).
