# nftables (netfilter/iptables)

- Nou sistema de firewall.

- Traductor iptables a nftables: https://wiki.nftables.org/wiki-nftables/index.php/Moving_from_iptables_to_nftables


- nftables en Archlinux wiki - https://wiki.archlinux.org/index.php/Nftables_(Espa%C3%B1ol)
- Wiki: https://wiki.nftables.org/wiki-nftables/index.php/Main_Page

- NFTABLES, DISEÑO DE UN FIREWALL MODERNO - https://mytcpip.com/nftables-firewall/

- Guia per iniciar-se en nftables - https://linux-audit.com/nftables-beginners-guide-to-traffic-filtering/
- GUI nftables - https://github.com/caballeroalba/nftables-gui
