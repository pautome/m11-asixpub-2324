# Squid, ejemplos de ACLs. Ejercicios de entrenamiento

- Sabiendo que Squid cuenta, entre otros, con los siguientes elementos de ACL:

  - src: source (client) IP addresses
  - dst: destination (server) IP addresses
  - dstdomain: destination (server) domain name
  - url_regex: URL regular expression pattern matching
  - time: time of day, and day of week

- Explique las siguientes configuraciones:

**¿Quién podrá acceder a la web?**

~~~
acl network172 src 172.16.5.0/24

http_access allow network172
~~~

Todos los hosts. NO hay una regla final "http_access deny all". Si la hubiera podrían acceder solo los que tienen ip de la red 172.16.5.0/24.  

**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl Cooking1 url_regex cooking
acl Recipe1 url_regex recipe
acl myclients src 172.16.5.0/24

http_access deny Cooking1
http_access deny Recipe1
http_access allow myclients
http_access deny all
~~~
Solo deja salir a las máquinas que estén en la red 172.16.5.0/24 siempre que la URL de la petición no contenga las palabras cooking o recipe. Todo lo demás, lo bloquea

**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl Cooking2 dstdomain www.gourmet-chef.com

http_access deny Cooking2
http_access allow all
~~~
Bloquea solo las peticiones al sitio con dominio www.gourmet-chef.com y cualquier otra se permite

**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl ME src 10.0.0.1
acl YOU src 10.0.0.2

http_access allow ME YOU
http_access deny all
~~~
Permite peticiones con IP origen 10.0.0.1 y simultáneamente 10.0.0.2, o sea, que como es imposible, no permite ninguna.


**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl ME src 10.0.0.1
acl YOU src 10.0.0.2

http_access allow ME
http_access allow YOU
http_access deny all
~~~

Permite peticiones con IP origen 10.0.0.1 o 10.0.0.2. Esto es posible.


**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl US src 10.0.0.1 10.0.0.2

http_access allow !US
http_access deny all
~~~
Permite todas las peticiones que NO provengan de US (origenes 10.0.0.1 y 10.0.0.2). Todo lo demás, lo bloquea.


**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl GOOD dst 10.0.0.1

http_access allow GOOD
http_access deny all
~~~
Solo permite las peticiones que tienen destino la IP 10.0.0.1

**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl US src 10.0.0.1 10.0.0.2
acl GOOD dst 10.0.0.5

http_access allow GOOD US
http_access deny all
~~~
Permite todas las peticiones con (destino a 10.0.0.5) Y (con origen en 10.0.0.1 O 10.0.0.2). Los paréntesis están para indicar la precedencia de operaciones.
Todo lo demás, lo bloquea

**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl PornSites url_regex "/usr/local/squid/etc/pornlist"

http_access deny !PornSites
http_access deny all
~~~

Bloquea todas las peticiones a las URLs que NO contengan palabras indicadas en el fichero "/usr/local/squid/etc/pornlist". De todas maneras, con "http_access deny all" al final, el resultado es que NO se permite nada, ya que no hay ningún allow.


**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl USER1 src 192.168.100.0/24
acl USER2 src 192.168.200.0/24 192.168.201.0/24
acl DAY time 06:00-18:00

http_access allow USER1 DAY
http_access deny USER1
http_access allow USER2 !DAY
http_access deny USER2
http_access deny all
~~~

Permite las peticiones con origen de la red 192.168.100.0/24 (USER1) Y entre las 06:00 y las 18:00. Fuera de ese horario deniega la petición en la siguiente línea.
Permite las búsquedas con origen en las redes 192.168.200.0/24 O 192.168.201.0/24 (USER2) si **NO** son entre las 06:00 y las 18:00. En esas horas, bloquea las peticiones de esas redes. La siguiente línea, deniega el resto de peticiones de USER2.
