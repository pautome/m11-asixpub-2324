# Squid, ejemplos de ACLs. Ejercicios de entrenamiento

- Sabiendo que Squid cuenta, entre otros, con los siguientes elementos de ACL:

  - src: source (client) IP addresses
  - dst: destination (server) IP addresses
  - dstdomain: destination (server) domain name
  - url_regex: URL regular expression pattern matching
  - time: time of day, and day of week

- Explique las siguientes configuraciones:

**¿Quién podrá acceder a la web?**

~~~
acl network172 src 172.16.5.0/24

http_access allow network172
~~~

**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl Cooking1 url_regex cooking
acl Recipe1 url_regex recipe
acl myclients src 172.16.5.0/24

http_access deny Cooking1
http_access deny Recipe1olo deja salir a las máquinas que estén en la red 172.16.5.0/24 siempre que la URL de la petición no contenga las palabras cooking o recipe. Todo lo demás, lo bloquea

http_access allow myclients
http_access deny all
~~~

**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl Cooking2 dstdomain www.gourmet-chef.com

http_access deny Cooking2
http_access allow all
~~~

**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl ME src 10.0.0.1
acl YOU src 10.0.0.2

http_access allow ME YOU
http_access deny all
~~~


**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl ME src 10.0.0.1
acl YOU src 10.0.0.2

http_access allow ME
http_access allow YOU
http_access deny all
~~~



**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl US src 10.0.0.1 10.0.0.2

http_access allow !US
http_access deny all
~~~


**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl GOOD dst 10.0.0.1

http_access allow GOOD
http_access deny all
~~~

**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl US src 10.0.0.1 10.0.0.2
acl GOOD dst 10.0.0.5

http_access allow GOOD US
http_access deny all
~~~

**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl PornSites url_regex "/usr/local/squid/etc/pornlist"

http_access deny !PornSites
http_access deny all
~~~


**¿Qué está permitido y qué está denegado en la siguiente configuración?**

~~~
acl USER1 src 192.168.100.0/24
acl USER2 src 192.168.200.0/24 192.168.201.0/24
acl DAY time 06:00-18:00

http_access allow USER1 DAY
http_access deny USER1
http_access allow USER2 !DAY
http_access deny USER2
http_access deny all
~~~
