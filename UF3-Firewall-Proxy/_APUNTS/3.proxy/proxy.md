# Servidors Proxy

<img src="imgs/proxy-a3eeeece.png" width="600" align="center" />


**INS Carles Vallbona**

**Pau Tomé** (Act gener 2024)

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Servidors Proxy](#servidors-proxy)
	- [Que és un proxy?](#que-s-un-proxy)
	- [Proxy com a firewall de nivell d’aplicació](#proxy-com-a-firewall-de-nivell-daplicaci)
		- [Avantatges](#avantatges)
		- [Inconvenients](#inconvenients)
	- [Tipus de servidor proxy](#tipus-de-servidor-proxy)
		- [1. Proxy cache i proxy NAT](#1-proxy-cache-i-proxy-nat)
		- [2. Proxy transparent](#2-proxy-transparent)
		- [3. Proxy anònim](#3-proxy-annim)
		- [4. Proxy invers](#4-proxy-invers)
		- [5. Proxy obert](#5-proxy-obert)
	- [POSSIBLE UBICACIÓ DEL PROXY](#possible-ubicaci-del-proxy)
		- [1. No intercepció](#1-no-intercepci)
		- [2. Intercepció](#2-intercepci)
	- [PROXY AMB SQUID](#proxy-amb-squid)
		- [Característiques de Squid](#caracterstiques-de-squid)
		- [Depuració del servidor](#depuraci-del-servidor)
		- [SECCIONS IMPORTANTS DEL FITXER DE CONFIGURACIÓ](#seccions-importants-del-fitxer-de-configuraci)
		- [ALTRES SECCIONS](#altres-seccions)
		- [Condicions de les regles ACL (Access control list) a SQUID](#condicions-de-les-regles-acl-access-control-list-a-squid)
		- [Regles de control d’accés (accés a http)](#regles-de-control-daccs-accs-a-http)
	- [DEPURAR REGLES I ACL](#depurar-regles-i-acl)
	- [Tipus a les ACL](#tipus-a-les-acl)
		- [1. IP’s origen i destí](#1-ips-origen-i-dest)
		- [2. Dominis d’origen i destí](#2-dominis-dorigen-i-dest)
		- [3. Horaris](#3-horaris)
		- [4. Expressions regulars a la URL i el PATH](#4-expressions-regulars-a-la-url-i-el-path)
		- [5. Autenticació dels usuaris](#5-autenticaci-dels-usuaris)
			- [Autenticació bàsica](#autenticaci-bsica)
		- [6. Altres tipus d’ACL](#6-altres-tipus-dacl)
		- [Exemple 1 d’ús d’acl’s i http_access](#exemple-1-ds-dacls-i-httpaccess)
		- [Exemple 2 d’ús d’acl’s i http_access](#exemple-2-ds-dacls-i-httpaccess)
		- [Exemple complet: cas concret](#exemple-complet-cas-concret)
		- [Subjectes i objectes de l’exemple](#subjectes-i-objectes-de-lexemple)
	- [Enllaços per a consulta](#enllaos-per-a-consulta)

<!-- /TOC -->
---

## Que és un proxy?

És un **agent intermediari** que realitza una acció en representació d'un altre. Intercepta les connexions de xarxa fetes des d’un client a un servidor de destí.
- **Funcionament**: A sol·licita un recurs de C mitjançant B

**Exemple:** Comprar alcohol o tabac de forma fraudulenta per a menors a una botiga. El major d'edat és qui recull en una llista les peticions dels menors, fa les compres, només se'l veu a ell, i en retornar mira la llista per lliurar els productes demanats als menors.

<img src="imgs/proxy-d503d8f3.png" width="500" align="center" />

Servidor proxy https://www.ionos.es/digitalguide/servidores/know-how/que-es-un-servidor-proxy-inverso/.

---

<img src="imgs/proxy-8ff64b2d.png" width="800" align="center" />

Tira de Mafalda: Proxy

---

## Proxy com a firewall de nivell d’aplicació

Els servidors proxy actuen sobre la **capa d’aplicació (nivell 7)** del model OSI.
- Pot entendre certes aplicacions i protocols de nivell 7 (ex: protocol de transferència de fitxers o **FTP**, **DNS** o **HTTP**).
- **Permet detectar si un protocol no desitjat està funcionant per un port no estàndard** o si s’abusa d’un protocol de forma perjudicial, gràcies a que entén els protocols de nivell d'aplicació.
- És molt **més segur i fiable** comparat amb tallafocs de filtrat de paquets, ja que repercuteix sobre les 7 capes del model OSI.
- És similar a un tallafocs de filtrat de paquets, però també permet **filtrar pel contingut del paquet**, per exemple, per trobar un fitxer de malware a una resposta http.

### Avantatges

#### 1. Millora la seguretat:

- Pot **blocar tràfic maliciós**, ja que analitza el contingut del paquet, per exemple, les peticions HTTP, per observar si es fa la descàrrega d'un fitxer executable.

- **Bloca** l'accés a sites no desitjades, per exemple, examinant la URL de destí.

- Pot fer **Filtratge de continguts/Control de continguts** que no volem que els usuaris accedeixin, com per exemple, contingut pornogràfic.

- **Restringeix l'accés a Internet** per només uns determinats ports (la resta restaran tancats).

- **Oculta la identitat** i dona privacitat dels equips de la xarxa interna a nivell de xarxa (només un equip es comunica amb Internet).
  - Pot donar servei a molts usuaris per sol·licitar a traves d'ell continguts web (Ocultació IP / anonimat).
  - La petició del host NO va destinada al servidor final si no que es fa a la IP del proxy. Aquest serà el que farà la petició a Internet.
  - Tampoc hi ha enrutament ni accés directe com en el cas del firewall de nivell 3 i 4.

---

#### 2. Accelera la navegació per Internet:

Els usuaris de la xarxa local naveguen més ràpid, ja que pot guardar en **memòria cau** els recursos que s'han descarregat prèviament d'Internet.
- Utilització més eficient de la connexió a Internet.
- Augment i millora de la velocitat de resposta a peticions.

---

#### 3. Registra l'activitat de la xarxa

Es guarda als logs l'activitat cap a Internet o altres xarxes. Així podem extreure estadístiques d'ús de l'accés a Internet, i prendre decisions basades en dades, per exemple:
- Saber quins llocs són més visitats, així guardar en caché tot el que es pugui, com ara vídeos, imatges ISO grans, etc.
- Saber quins llocs es visiten i no es vol permetre.
- Saber quin és el periode que s'utilitza més l'accés a Internet.
- Saber l'ample de banda usat per cada usuari i si hi ha abús...

---

### Inconvenients

#### 1. Possibles continguts desactualitzats

Hi ha la possibilitat de servir als clients continguts desactualitzats de la memòria cau. Això es millora comprovant data de caducitat dels recursos a la memòria cau.

---

#### 2. Pot impedir la realització d'operacions de xarxa avançades

Alguns protocols no suporten ús de proxy, per exemple el protocol SIP de VoIP.

---

#### 3. Possible violació de la intimitat

El proxy actua com un "man-in-the-middle" dels usuaris de la xarxa local.
- Emmagatzemar les pàgines i objectes que els usuaris sol·liciten pot suposar una **violació de la intimitat** per algunes persones.
- Cal establir a les polítiques de l'empresa que s'està monitorant el tràfic dels usuaris.

---

#### 4. Possible punt de congestió

Com que ha de gestionar molt de trànsit de xarxa cap a l'exterior, això pot fer que en determinants moment sigui un coll d'ampolla.
- Llavors, cal dimensionar bé aquest equip en memòria RAM i disc.
- Utilitzar el balanceig de càrrega, que seria un clúster de servidors proxy repartint-se la feina.

---

## Tipus de servidor proxy

Podem classificar els tipus de proxy segons la seva funció en:

### 1. Proxy cache i proxy NAT

**Proxy cache** web:
- Aplicació específica d'accés a la web, per mirar de millorar-ne el rendiment.
- Mantenen copies locals dels arxius més sol·licitats.

**Proxy NAT**
- Integra els serveis de traducció d’adreces de xarxa privada-pública al Proxy.

El proxy Squid fa les dues funcions.

---

### 2. Proxy transparent

En un proxy normal, és necessari configurar les aplicacions clients, coma ara navegador web, client ftp, client de correu, etc. per indicar que han d'interaccionar amb un servidor proxy (adreça proxy), ja que les peticions sempre van destinades al proxy.

Però el proxy transparent combina un **servidor proxy** amb un **enrutador** o firewall. Per simplicitat es pot fer que s'executin en la mateixa màquina.

---

![](imgs/proxy-e3ede78a.png)

Configuració de proxy transparent amb dos dispositius

---

1. Els clients envien les peticions als servidors de destí originals, per exemple Google, tenint com a gateway l'enrutador-firewall que treballa en conjunt amb el proxy.

2. L'enrutador-firewall reenvia les connexions dels clients destinades al port 80 cap el port del servei proxy (3128 habitualment).

3. El proxy filtra la petició, tal com estigui definit a les seves polítiques, i reenvia la petició si escau cap a l'enrutador-firewall per que la reenvii a Internet.

Això elimina la necessitat de configurar els clients per anar a través de proxy.
- Les aplicacions NO són conscients de tenir un proxy pel mig i per això se'n diu transparent (com un vidre que permet veure Internet a través d'ell).
- Aquest tipus de proxy no permet fer autenticació d'usuaris com fan els proxys convencionals.

Squid també ho permet

### 3. Proxy anònim

És un proxy públic que augmenta la privacitat i l'anonimat dels clients proxy mitjançant l'eliminació de característiques identificatives.
- De fet, com que les peticions reals parteixen del proxy, és la seva adreça IP d'origen la que apareix als paquets de tràfic.

---

### 4. Proxy invers (reverse proxy)

<img src="imgs/proxy-73506b7e.png" width="500" align="center" />

https://www.ionos.es/digitalguide/servidores/know-how/que-es-un-servidor-proxy-inverso/

El proxy invers **es situa a la xarxa de destí**, on es troben els servidors, i no pas a la xarxa origen, on estan els clients, com els proxy-caché.

El proxy invers és un proxy instal·lat en una xarxa amb diferents servidors web (o d'altres tipus), servint d'intermediari a les peticions externes.
- **Dona "servei" a servidors** web, de bases de dades, etc. i **no als clients**.
- Millora la seguretat dels servidors que hi ha darrera, ja que rep totes les peticions i les gestiona.
- Millora el rendiment ja que **fa de servidor de balanceig de càrrega**, repartint les peticions entre els servidors que te darrera.
- També **guarda en memòria cau el contingut estàtic**, per exemple fitxers html dels servidors que te darrera.

Exemples poden ser HA Proxy i NGINX.

---

<img src="imgs/proxy-0e2d7de0.png" width="400" align="center" />

Exemple de configuració de NGINX com a proxy invers

---

### 5. Proxy obert o públic

Aquest proxy accepta peticions de qualsevol ordinador, estigui o no connectat a la seva xarxa. Solen situar-se a Internet com a proxy públic.

És un servidor proxy públic, susceptible d’abús però també pot ser fet servir per espies.

<img src="imgs/proxy-ea27dd33.png" width="500" align="center" />


---

## POSSIBLE UBICACIÓ DEL PROXY

### 1. No intercepció

En aquest cas, el proxy és un equip més de la xarxa local.

![](imgs/proxy-c91c89f7.png)

Mesures addicionals:
- Cal configurar els equips per a que utilitzin el proxy (a l’aplicació client web) però els usuaris amb coneixements es podrien “saltar” el proxy..
- Cal configurar el firewall per a que només accepti tràfic provinent del proxy i no dels clients de la xarxa local.
- Algú podria suplantar la IP del proxy si es troba a la mateixa xarxa.

---

### 2. Intercepció

El proxy és un equip que intercepta físicament les connexions de xarxa (està al mig), i està dotat de dues interfícies de xarxa.

![](imgs/proxy-df441f9c.png)

- En aquest cas, si cau el proxy, quedem sense connexió a internet i hauríem de tenir solucions alternatives.

---

## PROXY AMB SQUID

Squid http://www.squid-cache.org/ és un popular programa lliure que implementa un servidor proxy i memòria cau de pàgines web, amb llicència GPL.
- Permet accelerar un servidor web, guardar a memòria cau peticions repetides a DNS i altres cerques, memòria cau de web i seguretat filtrant el trànsit.
- Està dissenyat per executar-se sota entorns Unix (però funciona amb Windows).
- Se'l considera molt complet i robust.
- Orientat a HTTP i FTP però compatible amb altres protocols.
- Implementa diverses modalitats de xifrat com TLS, SSL, i HTTPS.

---

### Característiques de Squid

Algunes de les seves característiques són:
- Proxy i Memòria cau HTTP, FTP, HTTPS.
- Proxy per SSL
- Jerarquies de memòria cau ICP, HTCP, CARP, memòria cau digests (per a comunicar-se amb altres servidors proxy si tenen el recurs).
- Proxy transparent
- Suporta WCCP (Web Cache Control Protocol), control de múltiples proxys per balancejar càrrega.
- Regles de Control d'accés
- Acceleració de navegació HTTP.
- Memòria cau de resolució DNS
- Suporta SNMP

---

### Depuració del servidor

Quan comencem a desplegar un servidor Proxy Squid, necessitem informació sobre com està funcionant. La trobarem als arxius de log:
- /var/log/squid/access.log
- /var/log/squid/cache.log

Podem consulta les seccions i els nivells de logs a https://wiki.squid-cache.org/KnowledgeBase/DebugSections. Podem activar opcions de log amb la directiva **debug_options**:

~~~
debug_options Section, Level
~~~

~~~
debug_options ALL,9     # es veurà TOT!!! Això pot omplir molt ràpid els logs
~~~

Per comprovar la sintaxi del fitxer de configuració "squid.conf":

~~~
squid -k parse
~~~

---

### SECCIONS IMPORTANTS DEL FITXER DE CONFIGURACIÓ

- **OPTIONS FOR AUTHENTICATION**: Per configurar la validació d'usuaris. Vàries directives "auth...".
- **ACCESS CONTROLS**: Directives "acl" i "http_access".
- **NETWORK OPTIONS**: Directiva "http_port".
- **OPTIONS WHICH AFFECT THE NEIGHBOR SELECTION ALGORITHM**: Directiva "cache_peer"
- **MEMORY CACHE OPTIONS**: "cache_mem",  "maximum_object_size_in_memory",
- **DISK CACHE OPTIONS:** "cache_dir", "minimum_object_size", "maximum_object_size"
- **OPTIONS FOR URL REWRITING**: "url_rewrite_program", "url_rewrite_children", "url_rewrite_access"

---

### ALTRES SECCIONS

- LOGFILE OPTIONS:
- OPTIONS FOR TROUBLESHOOTING:
- OPTIONS FOR FTP GATEWAYING:
- OPTIONS FOR EXTERNAL SUPPORT PROGRAMS:
- HTTPD-ACCELERATOR OPTIONS:
- DELAY POOL PARAMETERS:
- CLIENT DELAY POOL PARAMETERS:
- SSL OPTIONS:
- OPTIONS RELATING TO EXTERNAL SSL_CRTD:
- ...

---

### Condicions de les regles ACL (Access control list) a SQUID

Primer es defineix les condicions a comprovar per cada petició dels clients. Es fa amb la directiva acl.
- acl [nom de la llista] [tipus] [components de la llista]

Les condicions definides es fan servir després a les regles de control com **"http_access"**. L’ordre en que definim les condicions "acl" **no te importància**.

Ex:
~~~
acl localnet src 172.16.100.0/28
acl hosts_permesos src -i "/etc/squid/llistes/permesos"
acl hosts_profes 192.168.0.4 192.168.0.7
acl sites_prohibits url_regex facebook.com twitter.com
~~~

El paràmetre -i permet ignorar majúscules i minúscules.

Es pot fer servir un fitxer per a posar els paràmetres (un per a cada línia). Caldrà posar el nom de fitxer amb la ruta entre cometes.

~~~
acl hosts_permesos src -i "/etc/squid/llistes/permesos"
~~~

Al fitxer permesos hi hauria una llista de ip’s, una per cada línia, amb els hosts permesos:
~~~
cat /etc/squid/llistes/permesos
192.168.0.25
192.168.0.35
192.168.0.70
~~~

---

**ATENCIÓ:** Ens hem d'assegurar que el fitxer existeix a la ruta indicada i que l'usuari "proxy" el pot llegir.

---

### Regles de control d’accés (accés a http)

Després es defineixen les accions (deny/allow) que s'ha d'executar amb la petició del client si la/les condició/ns que posem com a paràmetres es compleixen.

~~~
http_access [deny o allow] [acl implicades]
~~~

L'ordre en que escrivim les HTTP_ACCESS és **MOLT important**. No es poden escriure en qualsevol ordre (similar a les regles iptables). Normalment farem:
- Primer posarem les regles més específiques (afecten a menys hosts)
- Després les menys específiques (afecten a més hosts).

Això no vol dir que no hi hagi casos on s'hagi de fer d'una altra manera.

Ex1: Regla genèrica (afecta a molts hosts). La segona regla s'ha de posar sempre com a política per defecte que afecta a tots els hosts, i canviar l'ordre farà que ningú pugui navegar per Internet.
~~~
http_access allow hosts_permesos
http_access deny all
~~~

Ex2: Regla més específica, té dues condicions definides abans en les "acl". Afecta a les mateixes IP's origen que abans però ara només a les peticions que NO van destinades a hosts prohibits. S'han de complir les dues condicions (AND)
- Totes les acl que apareguin a la mateixa línia http_access s’han de complir (AND). Només que falli una, no s’executa la regla (que en aquest cas és allow).
- Si la regla s’executa, no es mira cap regla més (com a iptables).
- Es pot negar una acl amb el símbol !

~~~
// Si la IP és d'un host permès (AND) la URL NO conté una paraula denegada
http_access allow hosts_permesos !sites_prohibits
http_access deny all
~~~

- Si es vol fer un "OR lògic" (o sigui, una condició o una altra) s’ha de duplicar la regla (en l’exemple, tres acl de tipus src):

~~~
http_access allow xarxa_profes
http_access allow xarxa_alumnes

http_access deny all
~~~

- Si es vol fer AND (les dues acl es compleixen) es posen una al costat de l'altra.

~~~
http_access allow xarxa_alumnes xarxa_profes
~~~

**ATENCIÓ**: Aquesta última regla és impossible que es compleixi mai ja que les dues xarxes són diferents i un paquet només pot tenir ip origen en una de les dues.

---

## DEPURAR REGLES I ACL

IMPORTANT: Per saber quina regla es compleix (s'activa), podeu activar:

~~~
debug_options 28,3
(Activar secció 28=ACL’s, nivell de detall 3)
~~~

Podem veure quina regla es compleix al fitxer cache.log.

---

**ATENCIÓ:** Cal desactivar aquestes opcions de LOGS un cop posem el proxy en producció real, ja que consumeix molts recursos.

---

- Les seccions que podem activar són explicades a :
https://wiki.squid-cache.org/KnowledgeBase/DebugSections

---

## Tipus a les ACL

### 1. IP’s origen i destí

Per mirar adreces IP d’origen o destí (src, dst).

~~~
acl aclname src ip-address/netmask ...  # adreça IP del client (src=source=origen)
acl aclname src addr1-addr2/netmask ... # Igual però amb un rang d'adreces
acl aclname dst ip-address/netmask ...    # Adreça IP obtinguda de la URL de destí (dst=destí)
~~~

Exemples:
~~~
acl pc-jefe src 192.168.0.22
acl pcs-alumnes src 192.168.0.50-192.168.0.60
acl ip-facebook dst 157.240.243.35
acl xarxa-prohibida dst 192.168.10.0/24
~~~


### 2. Dominis d’origen i destí

Per mirar dominis d’origen o destí (srcdomain, dstdomain).

~~~
acl aclname srcdomain .foo.com ...          # Domini origen de la petició
acl aclname dstdomain .foo.com ...          # Domini de destí
~~~

### 3. Horaris

Per comprovar un horari:

~~~
acl aclname time [day-abbrevs] [h1:m1-h2:m2]   # Dies i hora a comprovar
~~~

Abreviatures:

S - Sunday, M - Monday, T - Tuesday, W - Wednesday

H - Thursday, F - Friday, A - Saturday

h1:m1 ha de ser menor que h2:m2

Exemple:
~~~
acl horari_laboral_mati time MTWHF 09:00-14:30
~~~

---

### 4. Expressions regulars a la URL i el PATH

Per buscar una expressió dintre la URL:

~~~
acl aclname url_regex [-i] ^http:// ...   # Expressió regular que es busca dintre la URL completa
acl aclname urlpath_regex [-i] \.gif$ ... # Expressió regular que es busca només dintre el camí de la URL
~~~

Per a més informació de les expressions regulars (RegEx) i prova: https://regexr.com/

Exemples
~~~
// A principi de la URL, buscar la cadena seguida de qualsevol caràcter
url_regex serv_publicidad ^http://adserver.*

// Buscar al final de la URL les lletres mp3
url_regex ficheros_mp3 -i mp3$
~~~

---

### 5. Autenticació dels usuaris

Per comprovar si una petició prové d'un client autenticat fem servir proxy_auth.

**NOTA:** proxy_auth requereix un programa extern per l'autenticació, o sigui, comprovar username/password. Hi ha una directiva prèvia que cal haver configurat correctament: auth_param.

**NOTA:** proxy_auth **no es pot usar en un proxy transparent** ja que el browser necessita ser configurat per funcionar amb proxy.

---

És molt important indicar a Squid quina és la forma en que ha d’autenticar els usuaris. Hi ha múltiples maneres que podem veure a:
https://wiki.squid-cache.org/ConfigExamples#Authentication

Exemple
~~~
// Indicar quin programa s'utilitza per autenticar els usuaris i els seus paràmetres. En aquest cas, autenticació bàsica amb fitxer de passwords
auth_param basic program /usr/lib/squid/basic_ncsa_auth /etc/squid/passwords

// 5 autenticadors simultanis com a màxim
auth_param basic children 5

// Caducitat de l'autenticació
auth_param basic credentialsttl 10 minute
~~~

---

Un cop determinat el mode que s'usa per autenticar, definim les acl d'autenticació

~~~
acl aclname proxy_auth [-i] username ...    #  Per autenticació del client (es pot fer servir un fitxer)
acl aclname proxy_auth_regex [-i] pattern ... # Es posa una llista de noms d'usuari permesos.
~~~

Cal usar REQUIRED (en lloc de nom d’usuari) per acceptar qualsevol usuari del fitxer d'usuaris.

Exemple:

~~~
// Serà TRUE si l'usuari es diu roger o jaume o julian i posem bé la contrasenya
acl profe_autenticat proxy_auth -i roger jaume julian

// Es pot fer amb un fitxer profes.txt que contindrà un llistat de noms
acl profe_autenticat proxy_auth -i "/etc/squid/aut/profes.txt"

// Usuaris que comencin amb el prefix profe_
acl profe_autenticat proxy_auth_regex -i ^profe_*

// Si volem que tots els usuaris que existeixin a la base de dades serveixin
acl usuari_autenticat proxy_auth REQUIRED
~~~

---

#### Opcions d'Autenticació

Tenim vàries opcions per autenticar, amb diferents programes o "helpers" que es connecten al sistema d'autenticació.
- Autenticació bàsica
- Autenticació LDAP
- Autenticació Kerberos
- Autenticació Mysql
- Autenticació NTLM (Windows)
- Autenticació Active Directory (Windows)
...

Veure: https://wiki.squid-cache.org/ConfigExamples#Authentication

---

Si fem servir autenticació bàsica, anem al bloc OPTIONS FOR AUTHENTICATION. Descomentem les línies aut_param:

~~~
auth_param basic children 5
auth_param basic realm Benvingut al Proxy de ACME
auth_param basic credentialsttl 2 hours
auth_param basic program /usr/lib/squid3/basic_ncsa_auth /etc/squid3/usuaris/squidpasswords
~~~

---

**ATENCIÓ:** Ens hem d'assegurar que tant el fitxer de passwords com el programa d'autenticació existeix a la ruta indicada i que l'usuari "proxy" els pot llegir.
---

Crearem el fitxer de password amb:
~~~
sudo apt install apache2-utils
htpasswd -c -nbm /etc/squid/passwords username password
~~~

Per afegir usuaris
~~~
htpasswd -nbm /etc/squid/passwords username password
~~~

Per esborrar usuaris:
~~~
htpasswd -D -nbm /etc/squid/passwords username password
~~~

Després farem servir acl i les regles per usar-les
~~~
acl auth proxy_auth REQUIRED

http_access deny !auth
http_access allow auth
http_access deny all
~~~

Canviarem el programa pel que haguem de fer servir (ens assegurem que hi és). Podem provar a mà a la consola de comandes que el programa extern de validació que hem posat a "auth_param basic program" funciona correctament

Per exemple, el programa "basic_ncsa_auth" es pot executar directament per a veure com funciona. De fet, Squid el crida per a que faci l'autenticació i aquest li torna un valor booleà (TRUE o FALSE).

---

### 6. Altres tipus d’ACL

~~~
acl aclname port 80 70 21 0-1024... # Port de destí de la petició
acl aclname proto HTTP FTP ...      # Protocol de la petició
acl aclname method GET POST ...     # Métode de la petició HTTP (GET, POST, HEADER...)
acl aclname browser [-i] regexp ... # Comprova el nom del navegador client
~~~

---

### Exemple 1 d’ús d’acl’s i http_access

Condicions a complir pel cas
- Permetre accedir si "ets el jefe" O (OR) "vas a webs permeses"
- Si vas a una web fora de les permeses i no ets jefe, et denega (última regla).
- Només que es compleixi una ja es fa un allow:

~~~
http_access allow ip_jefe
http_access allow web_permesa
http_access deny all
~~~

- Pregunta: El jefe podrà accedir a webs no permeses?

### Exemple 2 d’ús d’acl’s i http_access

Condicions a complir pel cas
- Permetre accedir si "ets el jefe" I (AND) "vas a una web permesa"
- Has de ser jefe i anar a web permesa per navegar.
- S'han de complir les dues condicions!

~~~
http_access allow ip_jefe web_permesa   
http_access deny all
~~~

- Pregunta: El jefe podrà accedir a webs no permeses?

---

### Exemple complet: cas concret

Condicions a complir pel cas
- Es vol que el jefe pugui accedir sense restriccions (cap ni una) a internet.
- Hi ha uns quants llocs web permesos per a qualsevol persona i a qualsevol hora.
- No es permet l'accés als empleats a webs prohibides durant l'horari de treball, però si en horari de pausa.

~~~
acl pc_jefe src 192.168.0.10
acl paraules_prohibides url_regex sex porn culo weapon chat

// Paraules excepcions escrites al fitxer amb la ruta que hem indicat
acl paraules_excepcions url_regex "/etc/squid3/acl/paraules_excepcions.acl"  
acl xarxa_local src 192.168.0.10/255.255.255.0     # Identificador de xarxa local
~~~

---
**ATENCIÓ:** No poseu comentaris darrera les acl. Cada comentari ha de posar-se a començament de línia.

---

~~~
// rang de Ips ordinadors dels empleats
acl pcs_empleats src 192.168.0.15-192.168.0.20  
// Horari de la pausa
acl horari_pausa time MTWHF 14:00-15:00

// Permet la petició origen ip del pc del jefe         
http_access allow pc_jefe    

// Permet la petició si la URL conté una paraula excepció i  
// a més la petició prové de la xarxa local
http_access allow paraules_excepcions xarxa_local    

// Denega la petició si la URL conté una paraula prohibida i a més la petició
// prové del pc d'un empleat i NO és horari de pausa
http_access deny paraules_prohibides pcs_empleats !horari_pausa

// Permet la petició si prové d'una ip de xarxa local
http_access allow xarxa_local   

// Aquesta regla sempre està i denega qualsevol petició si s'arriba aquí
http_access deny all  
~~~

### Subjectes i objectes de l’exemple

![](imgs/proxy-1c322bb6.png)

![](imgs/proxy-053006d2.png)

---

## Enllaços per a consulta

- Autenticació HTTP explicada: https://developer.mozilla.org/es/docs/Web/HTTP/Authentication

- Configuració bàsica Squid:
  - http://www.alcancelibre.org/staticpages/index.php/19-0-como-squid-general
  - http://www.alcancelibre.org/staticpages/index.php/manuales-indice (mirar secció squid)

- Exemples de configuració Squid:
- https://wiki.squid-cache.org/ConfigExamples

- Fer peticions per a test: comanda curl
https://curl.haxx.se/docs/httpscripting.html
https://www.computerhope.com/unix/curl.htm

- Usar Squid amb pàgines segures (Man in the middle): https://docs.diladele.com/faq/squid/recommended_settings_no_decryption.html

- Configuració de Squid segons el CNI: https://www.ccn-cert.cni.es/series-ccn-stic/600-guias-de-otros-entornos/454-ccn-stic-660-seguridad-en-proxies/file.html

- Restringir ample de banda - https://www.howtoforge.com/pfsense-squid-squidguard-traffic-shaping-tutorial

- Enrutament - http://www.ite.educacion.es/formacion/materiales/85/cd/linux/m6/enrutamiento_en_linux.html

- Logs amb Squid: https://forums.freebsd.org/threads/tip-debug-squid-acl-matches.5966/
