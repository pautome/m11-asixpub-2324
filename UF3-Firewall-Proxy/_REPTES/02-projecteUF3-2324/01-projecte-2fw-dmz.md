# PROJECTE UF2-UF3: Configuració de xarxa amb seguretat perimètrica  
# Tercera part: DMZ amb 2 firewalls

**ATENCIÓ:** Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, etc. han de ser el vostre per a que a les captures es pugui veure. Cada equip ha de tenir un nom de host diferent (firewall, server-web, etc.). Això facilitarà saber en quin equip estem quan ens connectem amb una consola remota.

---

## Referències i ajuda

- Taula de comandes de IPTables - https://drive.google.com/open?id=1hXl0NAim8VwqsJIFOzmoGnDDv88YTmtQ
- iptables (Espanyol) - https://wiki.archlinux.org/index.php/Iptables_(Espa%C3%B1ol)
- Mode actiu i mode passiu a FTP - https://slacksite.com/other/ftp.html#active
- https://wiki.archlinux.org/index.php/Simple_stateful_firewall_(Espa%C3%B1ol)
- https://www.frozentux.net/iptables-tutorial/iptables-tutorial.html
- https://engineering.purdue.edu/kak/compsec/NewLectures/Lecture18.pdf
- http://es.tldp.org/Manuales-LuCAS/doc-iptables-firewall/doc-iptables-firewall.pdf
- https://www.sans.org/reading-room/whitepapers/firewalls/nftables-second-language-35937

---

## Objectius

L'objectiu d'aquest projecte és crear una xarxa amb DMZ de 2 firewalls per aplicar iptables.

## Introducció

En aquest projecte haurem de fer un muntatge d'una xarxa DMZ amb dos firewalls (canvieu i completeu el mapa lògic):

![](imgs/01-projecte-dmz-1ba55514.png)

DMZ 2 firewalls

---

**NOTA:** Per a poder veure des de l'equip amfitrió les màquines internes cal afegir a la taula de rutes del host amfitrió la xarxa DMZ (192.168.222.0) i si cal la LAN (192.168.111.0). El seu gateway serà la IP del Pfsense a la xarxa WAN (mireu quina us assigna). Per afegir una ruta a la taula de rutes feu servir la comanda:
~~~
ip route add 192.168.222.0/24 via ip-externa-firewall
ip route add 192.168.111.0/24 via ip-externa-firewall
// Per veure la taula de rutes i comprovar que s'han afegit
ip route
~~~

Per a fer-ho de forma permanent, consultar la configuració d'interfície per afegir rutes estàtiques: https://netplan.readthedocs.io/en/stable/examples/#how-to-configure-source-routing. En l'exemple suposem que la IP del PFSense és 192.168.5.20. El fitxer de configuració es troba a /etc/netplan/

~~~
network:
  version: 2
  ethernets:
    ens3:
      addresses:
        - 192.168.5.30/24
      dhcp4: no
      routes:
        - to: default
          via: 192.168.5.1
        - to: 192.168.100.0/24
          via: 192.168.5.20
        - to: 192.168.200.0/24
          via: 192.168.5.20
~~~

Un cop configurada la xarxa, cal aplicar els canvis amb:

~~~
sudo netplan apply
~~~

Amb CentOS consulteu https://www.networkinghowtos.com/howto/adding-persistent-static-routes-on-centos/

---

## Orientacions

Per poder fer les coses pas a pas us recomano que ho feu en aquest ordre. Aquest NO és l'exercici, són indicacions:

1. Feu un mapa lògic on es vegin els equips, les interfícies que es configuren a cada equip, les adreces IP i quins serveis hi ha a cada equip.
2. Configurar el firewall extern amb les seves IP's externa i interna.
3. Configurar les polítiques per defecte de INPUT, OUTPUT, FORWARD a ACCEPT. Després les posarem a DROP per fer el firewall més segur. Cada cop que tingueu problemes, torneu a posar les polítiques a ACCEPT per descartar fallades a les regles que aneu afegint al firewall.
4. Activar l'enrutament en el firewall extern, de forma permanent.
5. Activar la regla d'enmmascarament al firewall (NAT) per a que el servidor pugui accedir a Internet.
6. Configurar els paràmetres de xarxa de les màquines internes.
7. Al Server de la DMZ li poseu la seva ip, màscara de subxarxa, gateway per defecte que és la IP interna del firewall extern i el servidor DNS, que pot ser el de google, 8.8.8.8.
  - Feu una prova de connectivitat ping a 8.8.8.8 per exemple. Hauria d'anar i ja comproveu que l'enrutament funciona.
  - Ara feu una prova de resolució de DNS a www.google.es per exemple, amb nslookup.
  - Si no funciona, teniu algun problema de resolució o accés del protocol DNS (funciona tant amb TCP com UDP). Llavors proveu un altre servidor DNS 208.67.222.222 o bé 1.1.1.1 per exemple.
8. Si tot va bé, proveu a navegar (podeu usar curl).
9. Ara configureu i proveu el server.
10. Configureu el reenviament de ports cap el servidor de la DMZ.

Ara ja podeu provar a posar la política FORWARD i la resta a DROP i anar afegint els protocols que es vol permetre.

Després configureu el firewall intern, seguint les mateixes indicacions.

---

## Requisits del projecte

Tant el **firewall extern com l'intern** seran Ubuntu Server amb dues interfícies i caldrà fer el necessari per a que es correspongui amb el mapa lògic indicat.

---

**ATENCIÓ:** Poseu un nom de host diferent a cada equip per poder diferenciar-los quan iniciem sessió remota.

---

El **firewall EXTERN** serà un Ubuntu server amb 2 interfícies:
- Una interfície per la xarxa pública via **interfície pont**, adreça per DHCP.
- Una altra interfície connectada a la DMZ via **xarxa interna**, adreça 192.168.222.1.

El **firewall INTERN** serà un Ubuntu server amb 2 interfícies:
- Una connectada a la DMZ via **xarxa interna**, adreça 192.168.222.2.
- Una altra interfície via **xarxa interna**, adreça 192.168.111.1 per la xarxa LAN 192.168.111.0/24.

Mantenim els equips anteriors:

- **Ubuntu Server** a la DMZ amb una IP adequada a la xarxa on es troba, configurat de forma estàtica NO per DHCP. Tindrà:
  - un **servei ftp** amb banner personalitzat
  - un **servei ssh** amb banner personalitzat
  - un **servidor web segur** amb una pàgina d'inici personalitzada.
  - un **servidor web no segur** amb una pàgina d'inici personalitzada.
  - Tindrà nmap i curl instal·lats.

- Un **equip client** a la xarxa interna Ubuntu Desktop.
  - Tindrà el servei ssh configurat amb un banner personalitzat.
  - Tindrà nmap i curl instal·lats.

---

**(4 punts)**

**Afegiu a la documentació la configuració completa final que feu al firewall.**

0. **Feu un mapa lògic** on es vegin els equips, les interfícies que es configuren a cada equip, les adreces IP i quins serveis hi ha a cada equip.

**Afegiu a la documentació la configuració completa final que feu al firewall.**

Configurem **iptables del firewall extern**:

1. Recordeu que aquest equip ha de tenir **l'opció de reenviament activada**.

2. S'ha de **permetre que els servidors de la DMZ accedeixin a Internet a qualsevol adreça i a la xarxa externa** que és del rang 172.16.26.0/24, i els arribi la resposta.
   - Per a poder fer-ho, caldrà activar NAT de sortida al firewall extern per a l'equip servidor que hi ha a la DMZ, només pels protocols necessaris.
  - Recordeu que per navegar i accedir a noms de domini s'ha de **permetre les resolucions DNS**.
  - El servidor ha de poder fer **ping i traceroute cap a Internet i la xarxa de l'aula**.
  - Els servidors de la DMZ han de:
    - Contenir a la seva taula de rutes **on es troba la xarxa LAN**, per poder tornar les respostes a peticions fetes des de la LAN.
    - Aquesta **taula de rutes ha de ser persistent**.

3. S'ha de **permetre accedir al servei web de la DMZ des de les xarxes externes**. NO utilitzeu NAT, si no que heu d'obrir els ports necessaris ja que la IP de destí serà la del servidor a la DMZ.

4. S'ha de **permetre accedir al servei ssh del servidor de la DMZ usant la IP externa del firewall extern i el port 2222**. O sigui, que cal configurar el reenviament de ports per NAT cap el servei ssh del servidor de la DMZ.  

4. **No s’ha de permetre l’accés directe dels equips de la xarxa LAN cap a la xarxa externa ni des de la xarxa externa a la xarxa interna**, per a cap protocol.

---

**(3 punts)**

**Afegiu a la documentació la configuració completa final que feu al firewall.**

Configurem **iptables del firewall intern** (en aquest cas és un equip Ubuntu server amb firewall iptables que enruta i filtra):

1. Recordeu que aquest equip ha de tenir **l'opció de reenviament activada**.

2. Les polítiques per defecte de **INPUT, OUTPUT i FORWARD han de ser DROP**.

3. El firewall ha de **permetre el tràfic iniciat des de la xarxa LAN cap a la DMZ**, només pels serveis que tingui el server de la DMZ (ssh, web segura, web no segura, ftp i poder fer-li ping). Cal que arribin els paquets de la resposta. Si cal retocar la configuració del servidor, feu-ho.

  **ATENCIÓ:** Ara hem de configurar regles tant per als paquets de petició com els de resposta.

4. **No s'ha de permetre el tràfic iniciat des de fora de la LAN cap als equips clients** que estiguin a la xarxa LAN (exemple, fer una petició ssh al servidor ssh de l'equip client de la LAN). Tampoc des de la DMZ.

5. Cal que la **configuració iptables sigui persistent** i que quan es reinicii el router segueixi funcionant.

---

**(3 punts)**

Comprovar els següents punts amb tests que evidenciïn el resultat. **A la captura, sense talls, s'ha de veure els paràmetres IP (l'adreça, màscara, taula de rutes) de la màquina des de la que s'està fent la prova**.

1. Fer comprovacions que demostrin, des d’un equip de la **xarxa interna**:
  - Poder fer ping, tràfic ssh, accés a servidors web i usar ftp cap al servidor de la DMZ i la resposta corresponent.
  - Fer un traceroute cap a 8.8.8.8 per veure per on passen els paquets. Provar a navegar per internet.
  - Comprovar si es pot resoldre noms de domini amb nslookup www.microsoft.com.
  - Fer nmap cap al servidor de la xarxa DMZ per veure quins serveis us mostra (només haurien de ser els habilitats al firewall).

2. Fer comprovacions que demostrin des d’un equip de la **xarxa externa** (equip amfitrió):
   - No poder fer ping (ni tràfic) des d’un equip de la xarxa externa (amfitrió) cap als equips de la xarxa interna. Recordeu que primer heu de configurar la taula de rutes a l'amfitrió per que sàpiga com arribar a la xarxa interna o la DMZ.
  - Poder fer ping i tràfic ssh cap a la IP del servidor de la DMZ.
  - Poder fer ssh cap a la IP del firewall extern i port 2222 per accedir a ssh del servidor de la DMZ.
  - Fer nmap cap al servidor de la xarxa DMZ per veure quins serveis es mostren.
  - Fer nmap cap al firewall extern per veure quins serveis es mostren.

3. Fer comprovacions que demostrin, **des d'un equip de la DMZ**
  - No poder accedir cap a l'equip de la xarxa interna. Es pot provar intentant connectar via ssh al Desktop intern i fer ping.
  - Fer nmap cap a l'equip de la xarxa interna per veure quins serveis us mostra.
  - Fer un traceroute cap a 8.8.8.8 per veure per on passen els paquets. Provar a navegar per internet o instal·lar paquets.

---

OPCIONAL:

**(2 punts)**

**Afegiu a la documentació la configuració completa final que feu al firewall.**

Configurem **iptables del servidor de la DMZ** (servidor amb firewall intern):
1. Política per defecte **INPUT i OUTPUT a DROP**.
2. Només es **permet tràfic de peticions als serveis instal·lats al servidor** (web, web segura, ftp i ssh).
3. El servidor ha de poder accedir a Internet a web segura i no segura. Cal activar els protocols necessaris per navegar i poder baixar paquets amb apt per exemple.
4. El servidor ha de poder fer **ping i traceroute cap a Internet i la xarxa de l'aula**.
5. Cal que la **configuració iptables sigui persistent** i que quan es reinicii el servidor segueixi funcionant.

Els servidors de la DMZ han de:
- Contenir a la seva taula de rutes **on es troba la xarxa LAN**, per poder tornar les respostes a peticions fetes des de la LAN.
- Aquesta **taula de rutes ha de ser persistent**.
