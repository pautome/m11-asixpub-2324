# PROJECTE UF2: Configuració de xarxa amb seguretat perimètrica  

# Part 4: Configuració de Proxy Squid

**ATENCIÓ:** Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, etc. han de ser el vostre per a que a les captures es pugui veure.

Teniu l'exercici a fer al final d'aquest enunciat. Llegiu primer tot el document abans de començar la pràctica.

---

## Referències i ajuda

Instal·lació i configuració - http://www.alcancelibre.org/staticpages/index.php/19-0-como-squid-general

Debug Squid ACL's
- https://www.cammckenzie.com/blog/index.php/2015/01/07/how-to-debug-squid-acls/
- https://wiki.squid-cache.org/KnowledgeBase/DebugSections

Manual Squid - http://www.squid-cache.org/Doc/config/

Instal·lació bàsica - http://www.alcancelibre.org/staticpages/index.php/19-0-como-squid-general

---

És possible que us trobeu un error "DNS name lookup tests failed" en iniciar Squid.

Això es deu a que Squid comprova que es pugui resoldre DNS amb uns dominis DNS de prova. En fi, si no pot fer-ho, no arrenca.

Això es pot saltar si posem el paràmetre -D quan cridem al dimoni. Per fer-ho, editeu el fitxer /etc/default/squid i afegiu la línia:

SQUID_ARGS=-D

---

## Objectius

L'objectiu d'aquest projecte és continuar creant una xarxa més segura, incorporant un servidor proxy a la DMZ del projecte anterior.

## Introducció

Farem la instal·lació bàsica i prova del servidor Squid. El muntatge és senzill. Només cal crear:
- La xarxa anterior amb un client a la LAN per a poder fer proves.
- Un **Ubuntu Server** com a servidor Squid ubicat a la DMZ amb 1 interfícies amb xarxa interna (configurada amb la IP corresponent).
- La xarxa DMZ tindrà l’identificador 192.168.200.0/24.
- La xarxa LAN tindrà l’identificador 192.168.100.0/24.

![](imgs/01-projecte-dmz-1ba55514.png)

---

## Reptes

#### NOTA: Si bloquem una pàgina HTTPS amb Squid, no sortirà el missatge de bloqueig de Squid, si no que apareixerà un missatge de que el proxy està denegant les connexions. Més info: https://docs.diladele.com/faq/squid/cannot_connect_to_site_using_https.html

---

### Repte 1: Configuració i prova bàsica (4 punts)

Es recomana començar a configurar i provar el servidor Proxy sense posar-ho a la xarxa completa. Es pot configurar la targeta de xarxa en mode pont de VirtualBox i provar des de la màquina física. Més tard, quan ja funcioni, es posarà a la xarxa completa.

1. Feu que el servidor Proxy pugui accedir a Internet i resoldre DNS configurant el firewall exterior. Assegurar-se que el servidor Proxy pot anar a Internet i pot resoldre noms DNS (provar amb ping a un servidor d’Internet, per exemple 8.8.8.8 i després usar nslookup per a resoldre un nom DNS com ara www.google.es).

2. Instal·leu de forma bàsica el proxy Squid (amb apt-get).

3. Un cop instal·lat:
- Els fitxers de configuració es troben a “/etc/squid” o bé a “/etc/squid3”
- Els fitxers de log es troben a “/var/log/squid”: **són cache.log i access.log**

4. Obriu una consola per editar els fitxers de configuració i una altra consola pels fitxers de log.

5. Comproveu que el proxy funciona i està escoltant al port 3128 des d'un equip client.

~~~
telnet ip 3128
(i escriure qualsevol cosa i enter)
~~~

Apareixerà text que podem veure que identifica al proxy:

6. Feu també la prova amb la comanda curl des del servidor web.

~~~
$ curl -x ip:port www.google.es
~~~

Ara ja sabrem que el proxy atén peticions i funciona.

6. Obrir el fitxer de configuració /etc/squid.conf
- Buscar **bloc “ACCESS CONTROLS”**: Per permetre els equips d’una xarxa local determinada fer servir el proxy (normalment la xarxa LAN interna):
- **tag “acl”**: Buscar la línia "acl localnet src …" i treure el comentari. Editar la xarxa si cal, i posar la xarxa local que es necessiti, la dels hosts clients.
- **tag “http_access”**: Buscar la línia "http_access allow localnet" i treure el comentari.


7. Al **navegador Web del client**, a preferències, buscar l’opció per configurar el servidor proxy o intermediari:
- Posar l’adreça IP del servidor Proxy i el port 3128 (si no s’ha canviat).
- Marcar l’opció de fer servir el mateix servidor proxy per a tots el protocols.
- Al quadre de text “Sense servidor intermediari per” afegirem la IP del propi proxy i les de la xarxa local que volem que es puguin accedir directament sense passar pel proxy (per exemple el propi servidor proxy).

8. Podem veure com ens limita el proxy si afegim una nova acl (al final del bloc d’acl’s) i la seva http_access associada (al bloc on hi ha el comentari “posa les teves regles aquí”, busqueu-ho). Fer una **prova de navegació des del client**.

~~~
acl prohibit url_regex youtube sex
http_access deny prohibit
~~~

L’efecte serà que si anem a youtube o una URL que contingui la paraula “sex”, es blocarà l’accés (sempre que sigui una petició http. Si és https ens tornarà un error del navegador).

Per poder fer consultes al proxy des de consola, podem fer servir la comanda curl (per ex, si el proxy te l'adreça 192.168.56.103):

~~~
$ curl -x 192.168.56.103:3128 www.google.es
~~~

9. Filtrem el fitxer del proxy per que no hi hagi els comentaris (seria interessant guardar una còpia del fitxer original, però filtrar-lo per poder treballar amb ell):

~~~
grep -v "^#" /etc/squid/squid.conf | sed -e '/^$/d' > fitxer.txt
~~~

10. Per saber quina regla es compleix, podeu activar al fitxer squid.conf la directiva debug_options (mireu al manual si els codis següents són correctes):

~~~
debug_options 28,3
~~~

Es veurà **quina regla es compleix, al fitxer de logs cache.log**. Mostreu les evidències que mostren quines regles s'estan executant, en aquest cas, quan es bloca una web prohibida.

**NOTA:** Cal desactivar aquestes opcions de LOGS un cop posem el proxy en producció real, ja que consumeix molts recursos.

11. Configurar els firewalls:
  - a l'intern per a afegir l'accés des de la LAN al port 3128 del servidor proxy. Atenció als paquets de tornada. Evidentment, els clients NO poden accedir directament a Internet saltant el proxy.
  - Actualitzeu si cal la "acl localnet" al proxy.
  - a l'extern, afegiu les regles que permetin al proxy accedir a Internet. Recordeu que s'ha de fer NAT per a que pugui accedir a Internet.    

---

## Repte 2: Configuració 1 (6 punts)

Volem aplicar restriccions de Squid per a una petita empresa, dedicada a la formació.
- De moment, no hi ha autenticació, així que tots els **controls es faran a partir de la IP d’origen de les peticions**.
  - administratius: 192.168.100.11-192.168.100.49
  - director: 192.168.100.10

- L’horari de treball s’estipula en:
  - Horari de pausa: de 13:00 a 15:00
  - Horari de treball: de 8:00 a 13:00 i de 14:00 a 17:00.

- Definiu els fitxers de webs permeses i web prohibides posant alguns dominis o paraules.

- Es vol que des de l’ordinador del director es pugui accedir sense restriccions (cap ni una) a Internet.

- Hi ha uns quants llocs web permesos per a qualsevol persona i a qualsevol hora.

- No es permet als empleats l'accés a webs prohibides durant l'horari de feina, només en horari de pausa.

- En horari de feina es pot accedir a Internet.

---

1. Escriviu la llista de acl's i https_access que necessiteu per **complir TOTS els requisits anteriors**. Poseu el vostre nom a les regles i **mostreu els logs que demostren que s'executen les regles quan toca**.

**Vigileu per que VirtualBox sincronitza l'hora actual d'una màquina virtual amb la de la màquina física si no ho desactiveu (cal buscar com es desactiva la sincronització horària).**

2. Feu les proves necessàries per **demostrar que les regles funcionen** (accedir dintre i fora d'horari amb diferents usuaris i a webs permeses i prohibides). Cal demostrar que les restriccions funcionen i s'apliquen amb els logs associats a les diferents proves (treballadors i director en diferents horaris, accedint a webs permeses o no). Marqueu les evidències.

3. També volem **visualitzar estadístiques d’ús de Squid**, que es confeccionen a partir dels fitxers de logs de l'Squid.
   - Existeixen diferents utilitats per a veure estadístiques d’ús de Squid. Per exemple:
    - SARG: https://sourceforge.net/projects/sarg/
    - LIGHTSQUID: http://lightsquid.sourceforge.net/Docs.html

---

*NOTA:* En el cas de LightSquid, cal modificar script de generació d'informes per que només funciona fins el 2020 https://finisky.github.io/2021/01/03/lightsquidhardcodeyear.en/. Recordar instal·lar "apt-get install libcgi-session-perl" per a que funcioni perl en Apache.

---

**S'ha de demostrar que les estadístiques funcionen i tenen contingut per veure les gràfiques i peticions. Per tant heu de fer proves d'accés des de diferents equips (ip's) i horaris (en els punts anteriors ho heu fet, així que les proves s'haurien de veure reflexades a les estadístiques).**

---

## Repte 2 (AMPLIACIÓ OPCIONAL): Configuració avançada de restriccions (3 punts)

https://stackoverflow.com/questions/3297196/how-to-set-up-a-squid-proxy-with-basic-username-and-password-authentication

Volem configurar restriccions al servidor Proxy de manera que els usuaris tinguin l’accés més controlat a internet:

1. En aquesta petita empresa, també es vol controlar l’accés a Internet d'altres usuaris. Tenim tres grups d’usuaris:
  - administratius
  - professors
  - alumnes

2. Tindrem els blocs d’adreces IP:
  - director: 192.168.100.10
  - administratius: 192.168.100.10-192.168.100.49
  - professors: 192.168.100.50-192.168.100.100
  - alumnes: 192.168.100.101-192.168.100.254

3. Cal autenticar els usuaris, i per tant hi ha d’haver un fitxer amb els comptes d’usuaris. (Restricció addicional opcional: Un usuari no es podrà autenticar des d’una IP que no sigui del seu grup, o sigui, un alumne no es pot autenticar com alumne des d'un pc de professor o administració).

---

**Molt important:** Si fem servir autenticació bàsica (no tenim un servidor d'autenticació extern):
- Caldrà crear un sol fitxer amb tots els noms d'usuari i password del sistema (alumnes, professors i administratius).
- Caldrà crear fitxers de text, un per a cada grup. Cada fitxer contindrà els noms d'usuaris del grup, un per línia. Farem servir aquests fitxers com a criteri per saber si un usuari pertany a un grup o no.

---

4. L’horari de classes s’estipula en:
- 8 a 14:30 h de dilluns a divendres
- 15 a 18:30 h només dimarts i dijous.

5. Els professors:
  - Podran accedir en horari de classe a totes les pàgines web excepte les prohibides al fitxer “webs-prohibides-profes”.
  - Fora d'horari de classes, podran accedir a les pàgines web que necessitin sense restricció.

6. Els alumnes:
  - Només podran accedir a les pàgines web autoritzades que apareixeran a un fitxer “webs-autoritzades-alumnes”.
  - L’horari d’ús per a ells serà el de classes, fora d'aquest no hi podran accedir.

7. Els administratius:
  - Poden usar Internet en l'horari de classes.

---

**Cal demostrar que les restriccions funcionen i s'apliquen amb els logs associats a les diferents proves (alumnes i professors en diferents horaris, autenticats i accedint a webs permeses o no). Marqueu les evidències.**
